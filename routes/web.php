<?php
use Vsch\TranslationManager\Translator;

\Route::group(['middleware' => env('TRANSLATION_MANAGER', null), 'prefix' => 'translations'], function () {
     Translator::routes();
 });

Route::get('sitemap.xml', 'Web\SitemapController@getIndex');
Route::get('sitemap_{lang}.xml',  array('as' => 'lang', 'uses' => 'Web\SitemapController@getLang'));

Route::prefix('admin')->group(function () {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/','AdminController@getIndex')->name('admin.dashboard');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
    Route::post('/password/email','Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset','Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset','Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}','Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');    
    
    Route::get('/projects','AdminController@menuProjects')->name('admin.projects');
    Route::get('/clients','AdminController@menuClients')->name('admin.clients');
    Route::get('/employees','AdminController@menuEmployees')->name('admin.employees');
    Route::get('/admins','AdminController@menuAdmins')->name('admin.admins');
    Route::get('/employeestest','AdminController@menuEmployeesTest')->name('admin.projectstest');

    //Proyectos
    Route::get('project/listall', ['uses' => 'AdminController@listAll', 'as' => 'admin.projects.listall']);
    Route::get('project/create', ['uses' => 'AdminController@createProject', 'as' => 'admin.projects.formupload']);
    Route::post('projects/postUpload', ['uses' => 'AdminController@storeProject', 'as' => 'admin.projects.store']);
    Route::post('projects/postUploadTranslation', ['uses' => 'Web\ProjectsTranslationController@storeTranslation', 'as' => 'admin.projectstranslation.store']);
    Route::post('projects/updateOrder', ['uses' => 'AdminController@updateOrder', 'as' => 'admin.projects.updateOrder']);

    //Order by {field}/{order}
    Route::get('project/order/{field}/{order}', ['uses' => 'AdminController@orderProjects', 'as' => 'admin.projects.order']);
    Route::get('client/order/{field}/{order}', ['uses' => 'AdminController@orderClients', 'as' => 'admin.clients.order']);
    Route::get('employees/order/{field}/{order}', ['uses' => 'AdminController@orderEmployees', 'as' => 'admin.employees.order']);
    Route::get('admins/order/{field}/{order}', ['uses' => 'AdminController@orderAdmins', 'as' => 'admin.admins.order']);
    //Order by end

    Route::get('project/{id}/edit', ['uses' => 'AdminController@editProject', 'as' => 'admin.project.edit']);
    Route::put('projects/{id}', ['uses' => 'AdminController@updateProject', 'as' => 'admin.project.update']);
    Route::delete('project/{id}', ['uses' => 'AdminController@destroyProject', 'as' => 'admin.projects.destroy']);
    Route::get('project/{id}/delete', ['uses' => 'AdminController@delete', 'as' => 'admin.projects.delete']);
    Route::get('project/{id}', ['uses' => 'AdminController@showProject', 'as' => 'admin.projects.show']);
    Route::post('projectsclients/postUpload', ['uses' => 'AdminController@storeProjectsClients', 'as' => 'admin.projectsclients.store']);
    Route::post('projectsclientsifexist/postUpload', ['uses' => 'AdminController@storeProjectsClientsIfExist', 'as' => 'admin.projectsclientsifexists.store']);
    Route::put('project/cambiarVisible/{id}', ['uses' => 'AdminController@cambiarPublicProject', 'as' => 'admin.projects.cambiarVisible']);
    
    //Clients
    Route::get('clients/create', ['uses' => 'AdminController@createClient', 'as' => 'admin.clients.formupload']);
    Route::post('clients/postUpload', ['uses' => 'AdminController@storeClient', 'as' => 'admin.clients.store']);
    Route::get('client/{id}/edit', ['uses' => 'AdminController@editClient', 'as' => 'admin.clients.edit']);
    Route::put('clients/{id}', ['uses' => 'AdminController@updateClient', 'as' => 'admin.client.update']);
    Route::delete('client/{id}', ['uses' => 'AdminController@destroyClient', 'as' => 'admin.clients.destroy']);
    Route::get('client/{id}/delete', ['uses' => 'AdminController@delete', 'as' => 'admin.clients.delete']);
    Route::get('client/{id}', ['uses' => 'AdminController@showClient', 'as' => 'admin.clients.show']);

    //Employees
    Route::get('employees/create', ['uses' => 'AdminController@createEmployee', 'as' => 'admin.employees.formupload']);
    Route::post('employees/postUpload', ['uses' => 'AdminController@storeEmployee', 'as' => 'admin.employees.store']);
    Route::get('employee/{id}/edit', ['uses' => 'AdminController@editEmployee', 'as' => 'admin.employees.edit']);
    Route::put('employees/{id}', ['uses' => 'AdminController@updateEmployee', 'as' => 'admin.employee.update']);
    Route::delete('employee/{id}', ['uses' => 'AdminController@destroyEmployee', 'as' => 'admin.employees.destroy']);
    Route::get('employee/{id}/delete', ['uses' => 'AdminController@delete', 'as' => 'admin.employees.delete']);
    Route::get('employee/{id}', ['uses' => 'AdminController@showEmployee', 'as' => 'admin.employees.show']);
    Route::post('employees/updateOrder', ['uses' => 'AdminController@updateOrderEmployee', 'as' => 'admin.projects.updateOrder']);
    Route::put('employee/cambiarVisible/{id}', ['uses' => 'AdminController@cambiarPublicEmployee', 'as' => 'admin.employees.cambiarVisible']);

    
    //Admins
    Route::get('admins/create', ['uses' => 'AdminController@createAdmin', 'as' => 'admin.admins.formupload']);
    Route::post('admins/postUpload', ['uses' => 'AdminController@storeAdmin', 'as' => 'admin.admins.store']);
    Route::get('admin/{id}/edit', ['uses' => 'AdminController@editAdmin', 'as' => 'admin.admins.edit']);
    Route::put('admins/{id}', ['uses' => 'AdminController@updateAdmin', 'as' => 'admin.admin.update']);
    Route::delete('admin/{id}', ['uses' => 'AdminController@destroyAdmin', 'as' => 'admin.admins.destroy']);
    Route::get('admin/{id}/delete', ['uses' => 'AdminController@delete', 'as' => 'admin.admins.delete']);
    Route::get('admin/{id}', ['uses' => 'AdminController@showAdmin', 'as' => 'admin.admins.show']);

});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localize'] // Route translate middleware
    ], function () {
    

    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

    Route::group([
                'namespace' => 'Web'
            ], function () {
            Route::get('/',['uses' => 'ProjectsController@getIndex', 'as' => 'home']);
            
            
            Route::get('/{main}/{sub}', ['uses' => 'ProjectsController@getIndex', 'as' => 'home']);
            
            Route::get('works', function(){
                return redirect('/');
            });

            Route::post('works/project-list/{offset}/{limit}', 'ProjectsController@getProjectList');
            Route::post('/works/project/{slug}', 'ProjectsController@getProject');
            
            Route::post('about/worker-list/{offset}/{limit}', 'AboutController@getWorkerList');

            Route::post('/raw/html/{view}', 'RawController@getIndex');
            Route::post('/raw/json/{view}', 'RawController@getJson');

            Route::get('/{catchall}', ['uses' => 'ProjectsController@getIndex', 'as' => 'home']);

            Route::get('sitemap.xml', 'SitemapController@getIndex');
            Route::get('sitemap_{lang}.xml',  array('as' => 'lang', 'uses' => 'SitemapController@getLang'));

            //Route::get('workshop-un-touch-point-estrategico', ['uses' => 'WorkshopController@index', 'as' => 'workshop.index']);
            //Route::post('workshop-un-touch-point-estrategico', ['uses' => 'WorkshopController@create', 'as' => 'workshop.create']);

        });   

});







