<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectTranslations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_translations', function(Blueprint $table)
		{
			$table->increments('id');
		    $table->string('locale')->index();
		    $table->integer('project_id')->unsigned();

		    $table->string('title');
		    $table->string('caption')->default('');

		    $table->unique(['project_id','locale']);
		    $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_translations');
	}

}
