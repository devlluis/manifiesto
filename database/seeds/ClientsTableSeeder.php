<?php

namespace Seeds;

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientsTableSeeder extends Seeder {

	public function run()
	{

		Client::truncate();

		$clients = [
			['name'=>'Yamaha','slug'=>'yamaha'],
			['name'=>'Bollycao','slug'=>'bollycao'],
			['name'=>'Panrico','slug'=>'panrico'],
			['name'=>'Cinesa','slug'=>'cinesa'],
			['name'=>'Monster High','slug'=>'monster-high'],
			['name'=>'Mattel','slug'=>'mattel'],
			['name'=>'Fisher-Price','slug'=>'fisher-price'],
			['name'=>'Barbie','slug'=>'barbie'],
			['name'=>'Generalitat de Catalunya','slug'=>'generalitat-de-catalunya'],
			['name'=>'Dainese D-Store Madrid','slug'=>'dainese'],
            ['name'=>'Hot Wheels','slug'=>'hot-wheels'],
            ['name'=>'Purina','slug'=>'purina'],
			['name'=>'Pepsico','slug'=>'pepsico'],
			['name'=>'Cheetos','slug'=>'cheetos'],
			['name'=>'Hero','slug'=>'hero'],
			['name'=>'Babicka Vodka','slug'=>'babicka-vodka'],
			['name'=>'Danone','slug'=>'danone'],
			['name'=>'TostaRica Bizcochitos','slug'=>'tostarica'],
			['name'=>'Servei Català de Trànsit','slug'=>'servei-catala-de-transit'],
			['name'=>'Viña esmeralda','slug'=>'vina-esmeralda'],
			['name'=>'Mitsubishi Electric','slug'=>'mitsubishi-electric'],
			['name'=>'Asus','slug'=>'asus'],
			['name'=>'Beat Bikers','slug'=>'beat-bikers'],
			['name'=>'Más Movil','slug'=>'mas-movil'],
			['name'=>'Ajuntament de Barcelona','slug'=>'ajuntament-de-barcelona'],
			['name'=>'UNO','slug'=>'uno'],
			['name'=>'Scrabble','slug'=>'scrabble'],
		];

		foreach($clients as $client)
		{
			Client::create([
				'name' => $client['name'],
				'slug' => $client['slug'],
			]);
		}
	}

}