<?php

namespace Seeds;

use App\Models\Project;
use App\Models\Client;
use DB;
use Illuminate\Database\Seeder;


class ProjectsTableSeeder extends Seeder {

	public function run()
	{
		
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('project_translations')->truncate();
		DB::table('projects')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');

		$projects = [

			[
				'slug'=>'monstruos-camara-accion',
				'public' => 0,
				'order' => 27,
				'clients'=>[5],
				'en'=>[
						'title'=>'Frights! Camera! Action!',
						'caption'=> 'We made movie directors out of <strong>Monster High</strong> fans',
						],
				'es'=>[
						'title'=>'¡Monstruos! ¡Cámara! ¡Acción!',
						'caption'=> 'Convertimos a las fans de <strong>Monster High</strong> en directoras de cine',
						],
			],

			[
				'slug'=>'cinesa-fan-fan-day',
				'public' => 1,
				'order' => 23,
				'clients'=>[4],
				'en'=>[
						'title'=>'Oh! Fan fan day',
						'caption'=> 'We turned a simple 2x1 promotion into a <strong>branding campaign</strong>.',
						],
				'es'=>[
						'title'=>'Oh! Fan fan day',
						'caption'=> 'Convertimos una sencilla promoción 2x1 en una <strong>campaña de branding</strong>',
						],
			],

			[
				'slug'=>'fisher-price-casting',
				'public' => 1,
				'order' => 8,
				'clients'=>[7],
				'en'=>[
						'title'=>'The Casting',
						'caption'=> '<strong>We connect with mums</strong> through a powerful insight',
						],
				'es'=>[
						'title'=>'El Casting',
						'caption'=> '<strong>Conectamos con las mamis</strong> a través de un poderoso insight',
						],
			],

			[
				'slug'=>'cinesa-rewind',
				'public' => 1,
				'order' => 18,
				'clients'=>[4],
				'en'=>[
						'title'=>'Rewind',
						'caption'=> 'We got a whole generation to rewind <strong>back into the past</strong>',
						],
				'es'=>[
						'title'=>'Rewind',
						'caption'=> 'Hicimos que toda una generación <strong>rebobinase al pasado</strong>',
						],
			],

			[
				'slug'=>'be-the-uno',
				'public' => 1,
				'order' =>22,
				'clients'=>[26],
				'en'=>[
						'title'=>'Be the UNO',
						'caption'=> 'We got thousands of people to feel what is it like to <strong>be the numero UNO</strong>',
						],
				'es'=>[
						'title'=>'Be the UNO',
						'caption'=> 'Conseguimos que miles de personas sintieran qué es <strong>ser el número UNO</strong>',
						],
			],

			[
				'slug'=>'yamaha-xperience-madrid',
				'public' => 0,
				'order' => 28,
				'clients'=>[1],
				'en'=>[
						'title'=>'Yamaha Xperience Madrid',
						'caption'=> 'We turned a brand event into a <strong>HUGE motor festival</strong>',
						],
				'es'=>[
						'title'=>'Yamaha Xperience Madrid',
						'caption'=> 'Convertimos un evento de marca en una <strong>GRAN fiesta del motor</strong>',
						],
			],

			[
				'slug'=>'bollycao-toi-de-cine',
				'public' => 0,
				'order' => 25,
				'clients'=>[3],
				'en'=>[
						'title'=>'Movie Toi',

						'caption'=> 'We brought one of the <strong>classic Bollycao characters</strong> back to life<br>&nbsp;<br>',
						],
				'es'=>[
						'title'=>'Toi de cine',

						'caption'=> 'Revivimos a uno de los <strong>personajes clásicos</strong> de Bollycao<br>&nbsp;<br>',
						],
			],

			[
				'slug'=>'scrabble',
				'public' => 1,
				'order' => 21,
				'clients'=>[27],
				'en'=>[
						'title'=>'Scrabble',

						'caption'=> '<strong>We helped thousands of children</strong> through the simple use of words',
						],
				'es'=>[
						'title'=>'Scrabble',

						'caption'=> '<strong>Ayudamos a miles de niños</strong> gracias a algo tan sencillo como las <strong>palabras</strong>',
						],
			],

			[
				'slug'=>'yamaha-campaigns',
				'public' => 1,
				'order' => 20,
				'clients'=>[1],
				'en'=>[
						'title'=>'Campaigns',

						'caption'=> 'For years, we have been repositioning <strong>Yamaha Motor España</strong> through different visual campaigns',
						],
				'es'=>[
						'title'=>'Campañas',

						'caption'=> 'Llevamos años reposicionando a <strong>Yamaha Motor España</strong> a través de diferentes campañas gráficas',
						],
			],

			[
				'slug'=>'barbie-pearl-princess',
				'public' => 0,
				'order' => 26,
				'clients'=>[8],
				'en'=>[
						'title'=>'Barbie Pearl Princess Event',
						'caption'=> 'We managed to engage girls in the <strong>magical world of Barbie</strong>',
						],
				'es'=>[
						'title'=>'Evento Barbie Pearl Princess',
						'caption'=> 'Conseguimos que las niñas se sumergieran en el <strong>mundo mágico de Barbie</strong>',
						],
			],
			[
				'slug'=>'servei-catala-transit',
				'public' => 1,
				'order' => 24,
				'clients'=>[19],
				'en'=>[
						'title'=>'Servei Català de Trànsit',
						'caption'=> 'We got a <strong>simple gesture</strong> to become a viral campaign',
						],
				'es'=>[
						'title'=>'Servei Català de Trànsit',
						'caption'=> 'Conseguimos que un <strong>simple gesto</strong> se convirtiera en una campaña viral',
						],
			],
			[
				'slug'=>'dainese-la-invasion-roja',
				'public' => 1,
				'order' => 11,
				'clients'=>[10],
				'en'=>[
						'title'=>'The Red Invasion',
						'caption'=> 'Hundreds of people drove through the streets of <strong>Madrid</strong> via <strong>Google Street View</strong>',
						],
				'es'=>[
						'title'=>'La Invasión Roja',
						'caption'=> 'Cientos de personas condujeron por las calles de <strong>Madrid</strong> a través de <strong>Google Street View</strong>',
						],
			],
			[
				'slug'=>'hot-wheels-dia-del-padre',
				'public' => 1,
				'order' => 12,
				'clients'=>[11],
				'en'=>[
						'title'=>'Father\'s Day',
						'caption'=> 'Together with <strong>Hot Wheels</strong> we paid an original and emotional <strong>tribute to all fathers</strong>',
						],
				'es'=>[
						'title'=>'Día del padre',
						'caption'=> 'Junto a <strong>Hot Wheels</strong> rendimos un emotivo y original <strong>homenaje a todos los padres</strong>',
						],
			],
			[
				'slug'=>'fisher-price-little-discoveries',
				'public' => 1,
				'order' => 19,
				'clients'=>[7],
				'en'=>[
						'title'=>'Little Discoveries Ask the Experts',
						'caption'=> '<strong>Little experts give us their opinions</strong> on <strong>Fisher-Price</strong> products, with a healthy dose of humour',
						],
				'es'=>[
						'title'=>'Little Discoveries Ask the Experts',
						'caption'=> '<strong>Pequeños expertos opinan</strong> sobre los productos <strong>Fisher-Price</strong> con mucho sentido del humor',
						],
			],
            [
                'slug'=>'nestle-purina-petcare-europe-conference',
                'public' => 0,
                'order' => 29,
                'clients'=>[12],
                'en'=>[
                    'title'=>'Nestlé Purina PetCare Europe Conference',
                    'caption'=> 'The <strong>Nestlé Purina PetCare Europe Conference</strong> successfully surprised more than 150 managers from all over Europe',
                ],
                'es'=>[
                    'title'=>'Nestlé Purina PetCare Europe Conference',
                    'caption'=> '<strong>Nestlé Purina PetCare Europe Conference</strong> consiguió sorprender a más de 150 managers de toda Europa',
                ],
            ],
			[
				'slug'=>'tostarica-bizcochitos-fabrica-magica',
				'public' => 1,
				'order' => 16,
				'clients'=>[18],
				'en'=>[
					'title'=>'Magic Factory',
					'caption'=> 'Welcome to the <strong>TostaRica</strong> magic factory',
				],
				'es'=>[
					'title'=>'Fábrica Mágica',
					'caption'=> 'Bienvenidos a la Fábrica Mágica de <strong>TostaRica</strong>',
				],
			],
			[
				'slug'=>'baila-con-barbie',
				'public' => 1,
				'order' => 13,
				'clients'=>[8],
				'en'=>[
					'title'=>'Dance with Barbie',
					'caption'=> 'We demonstrate that anything is possible with <strong>Barbie</strong>',
				],
				'es'=>[
					'title'=>'Baila con Barbie',
					'caption'=> 'Demostramos que todo es posible con <strong>Barbie</strong>',
				],
			],
			[
				'slug'=>'babicka-vodka-the-other-side',
				'public' => 1,
				'order' => 6,
				'clients'=>[16],
				'en'=>[
					'title'=>'The Other Side',
					'caption'=> 'The <strong>vodka</strong> that moves into the most mysterious side',
				],
				'es'=>[
					'title'=>'The Other Side',
					'caption'=> 'El <strong>vodka</strong> que se mueve en el lado más misterioso',
				],
			],
			[
				'slug'=>'danone-pro',
				'public' => 1,
				'order' => 4,
				'clients'=>[17],
				'en'=>[
					'title'=>'Danone Pro',
					'caption'=> "We launched <strong>Danone Pro</strong>, the first <strong>Danone's</strong> yogurt for the professional trade",
				],
				'es'=>[
					'title'=>'Danone Pro',
					'caption'=> 'Lanzamos <strong>Danone Pro</strong>, el primer yogur <strong>Danone</strong> para el profesional de la hostelería',
				],
			],
			[
				'slug'=>'cheetos-minions-spot-tv',
				'public' => 1,
				'order' => 10,
				'clients'=>[14],
				'en'=>[
					'title'=>'Cheetos Minions spot',
					'caption'=> '<strong>Cheetos</strong> joins <strong>Minions</strong> with the most naughty "tazos"',
				],
				'es'=>[
					'title'=>'Spot Cheetos Minions',
					'caption'=> '<strong>Cheetos</strong> se une a <strong>Minions</strong> con los tazos más traviesos',
				],
			],
			[
				'slug'=>'hero-la-receta-de-la-vida',
				'public' => 1,
				'order' => 15,
				'clients'=>[15],
				'en'=>[
					'title'=>'The recipe of life',
					'caption'=> '<strong>Hero</strong> shows that no recipe makes sense if it is not shared in family',
				],
				'es'=>[
					'title'=>'La receta de la vida',
					'caption'=> '<strong>Hero</strong> demuestra que ninguna receta tiene sentido si no se comparte en familia',
				],
			],
            [
                'slug'=>'guardians-del-transit',
                'public' => 1,
                'order' => 17,
                'clients'=>[19],
                'en'=>[
                    'title'=>'The Guardians of the Traffic',
                    'caption'=> '<strong>The Guardians of the Traffic</strong> Land in the Barcelona International Comics Convention',
                ],
                'es'=>[
                    'title'=>'Els Guardians del Trànsit',
                    'caption'=> '<strong>Els Guardians del Trànsit</strong> aterrizan en el salón del cómic de Barcelona',
                ],
            ],
            [
                'slug'=>'torres-v-esmeralda',
                'public' => 1,
                'order' => 3,
                'clients'=>[20],
                'en'=>[
                    'title'=>'It\'s Time to Shine!',
                    'caption'=> 'Welcome to the <strong>Mediterranean Classic</strong>',
                ],
                'es'=>[
                    'title'=>'It\'s Time to Shine!',
                    'caption'=> 'Welcome to the <strong>Mediterranean Classic</strong>',
                ],
            ],
			[
				'slug'=>'al-volant-tria-el-bon-cami',
				'public' => 1,
				'order' => 9,
				'clients'=>[19],
				'en'=>[
						'title'=>'Being at the wheel, choose the right track',
						'caption'=> 'We show that traffic accidents  <strong>can be avoided</strong>',
				],
				'es'=>[
						'title'=>'Al volant, tria el bon camí',
						'caption'=> 'Demostramos que los accidentes de tráfico  <strong>se pueden evitar</strong>',
				],
			],
			[
				'slug'=>'mitsubishi-electric-tecnologia-con-la-que-puedes-contar',
				'public' => 1,
				'order' => 2,
				'clients'=>[21],
				'en'=>[
						'title'=>'Technology you can count on',
						'caption'=> 'The air conditioning you <strong>can count on</strong>',
				],
				'es'=>[
						'title'=>'Tecnología con la que puedes contar',
						'caption'=> 'Los aires acondicionados con los que  <strong>puedes contar</strong>',
				],
			],
			[
				'slug'=>'asus-friendship-is-incredible',
				'public' => 1,
				'order' => 1,
				'clients'=>[22],
				'en'=>[
						'title'=>'Friendship is incredible',
						'caption'=> 'We demonstrate friends are <strong>the family</strong> we choose.',
				],
				'es'=>[
						'title'=>'Friendship is incredible',
						'caption'=> 'Demostramos que los amigos son <strong>la familia</strong> que se elige',
				],
			],
			[
				'slug'=>'beatbikers-transformacion-digital',
				'public' => 1,
				'order' => 5,
				'clients'=>[23],
				'en'=>[
						'title'=>'Digital transformation',
						'caption'=> 'Digital <strong>transformation</strong>',
				],
				'es'=>[
						'title'=>'Transformación digital',
						'caption'=> 'Digital <strong>transformation</strong>',
				],
			],
			[
				'slug'=>'masmovil-branded-content',
				'public' => 1,
				'order' => 7,
				'clients'=>[24],
				'en'=>[
						'title'=>'El Resuelvedudas',
						'caption'=> 'Branded <strong>content</strong>',
				],
				'es'=>[
						'title'=>'El Resuelvedudas',
						'caption'=> 'Branded <strong>content</strong>',
				],
			],
			[
				'slug'=>'ajuntament-barcelona-autentic-paper-del-nadal',
				'public' => 1,
				'order' => 14,
				'clients'=>[25],
				'en'=>[
						'title'=>'The real Christmas\' paper',
						'caption'=> '<strong>Barcelona\'s</strong> first gift wrap',
				],
				'es'=>[
						'title'=>'L\'autèntic paper del Nadal',
						'caption'=> 'El primer papel de regalo de <strong>Barcelona</strong>',
				],
			]
			
		];

		foreach($projects as $project)
		{

			$p = Project::create([
				'slug'=>$project['slug'],
				'order'=>$project['order'],
				'public'=>$project['public'],
			]);

            $clients = Client::whereIn('id',$project['clients'])->pluck('id');
            $p->clients()->sync($clients->toArray());

			$p->translateOrNew('en')->title = $project['en']['title'];
			$p->translateOrNew('es')->title = $project['es']['title'];
			$p->translateOrNew('en')->caption = $project['en']['caption'];
			$p->translateOrNew('es')->caption = $project['es']['caption'];
			$p->save();
		}
	}

}