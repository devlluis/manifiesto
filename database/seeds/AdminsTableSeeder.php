<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $arrayAdmins = array(
        array(
            'name' => 'Lluis',
            'email' => 'lluis.puig@manifiesto.biz',
            'password' => 'manifiestamente1',
        ),
    );
    public function run()
    {
        self::seedAdmins();
    }

    public function seedAdmins()
    {
        DB::table('admins')->delete();
        foreach ($this->arrayAdmins as $admin)
        {
            $a = new Admin;
            $a->name = $admin['name'];
            $a->email = $admin['email'];
            $a->password = bcrypt($admin['password']);
            $a->save();
        }
    }
}
