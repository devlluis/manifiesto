<?php

use Illuminate\Database\Seeder;
use Seeds\ClientsTableSeeder;
use Seeds\ProjectsTableSeeder; 
use Seeds\WorkersTableSeeder; 

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(ClientsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(WorkersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);

    }
}
