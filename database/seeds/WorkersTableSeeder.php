<?php

namespace Seeds;

use Illuminate\Database\Seeder;
use App\Models\Worker;

class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Worker::truncate();

		$workers = [
            [
                'name'=>'Lluís Puig',
                'hashtag' => 'CodeExplorer',
                'position'=>'Online Developer',
                'slug'  => 'lluis-puig',
                'order' => 36
            ],
		];

		foreach($workers as $key => $worker)
		{
			Worker::create([
				'name' => $worker['name'],
				'hashtag' => $worker['hashtag'],
				'position' => $worker['position'],
				'slug' => $worker['slug'],
                'order' => $worker['order']
			]);
        }
    }
}
