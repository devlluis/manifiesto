require('jqueryui');

    //Javascript view /projects/menu.blade.php
    $.ajaxSetup({ 
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){

        var timeoutId;
        var seconds = 59;
            var url = window.location.href; 
            var minutes = 9;
            if (url.indexOf("login") < 0 && url.indexOf("admin") >= 0) {
                $(document).mousemove(function(event) {
                    clearTimeout(timeoutId);
                    timeoutId = setTimeout(function() {
                        minutes = 9;
                        seconds = 59;
                                    document.getElementById("timer").style.color="black";
                                    setTimeout(function(){
                                        $('.modal').on('show.bs.modal', function () {
                                            $('.modal').not($(this)).each(function () {
                                                $(this).modal('hide');
                                            });
                                        });
                                        $("#modalwarning").modal();
                                    }, 1000);
                                    var x = setInterval(function() {
                                        seconds --;
                                        $("#timer").text("00:"+"0"+minutes+":"+seconds);
                                        if (seconds<10){
                                            $("#timer").text("00:"+"0"+minutes+":0"+seconds);
                                        }
                                        if (seconds<1 && minutes>0 ){
                                            seconds = 60;
                                            minutes --;
                                        }
                                        if (minutes<1){
                                            document.getElementById("timer").style.color="red";
                                            if (seconds<1){
                                                console.log('aqui estoy');
                                                clearInterval(x);
                                                setTimeout(function(){
                                                    $('#timer').hide();
                                                },1000);
                                                setTimeout(function(){
                                                    $('#modalwarningh3').text("Cerrando sesión");
                                                    document.getElementById('loadinggif').style.display = "block";
                                                },1000);
                                                setTimeout(function(){
                                                    window.location.href = '/admin/logout';
                                                },4000);
                                            }
                                        }
                                        if ($(document).mousemove(function(event) {
                                            clearInterval(x);
                                            $("#modalwarning").modal('hide');
                                        }));

                                    },1000);

                                }, 100000);

                });
            }
        
        	
        $("#previewproject").click(function(){
                window.open(
                    'http://manifiesto.loc',
                    '_blank'
                );
        });
        $("#previewemployee").click(function(){
            window.open(
                'http://manifiesto.loc/es/about',
                '_blank'
            );
        })
        
        //View principal para los proyectos
        $("#buttoncreate").click(function(){ 
            $("#listallprojects").hide(); 
            $("#formcrearproyecto").fadeIn(1000);

        });
    
        $("#myFormEmployee").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'/admin/employees/postUpload',
                type: 'POST',
                data: formData,
                success: function(responseText) {
                    $responseData = JSON.parse(responseText);
                    if($responseData.status == 'trabajadortamañoincorrecto'){
                        $(".divtrabajadortamañoincorrecto").fadeIn(2000);
                        $(".divtrabajadortamañoincorrecto").fadeOut(5000);
                    }
                    else if($responseData.status == 'nohayimagen'){
                        $(".divtrabajadormal").fadeIn(2000);
                        $(".divtrabajadormal").fadeOut(5000);
                    }
                    else if($responseData.status == 'success'){
                        location.reload();
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        });

            //View para crear proyecto
    $("#myFormClient").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/clients/postUpload',
            type:'POST',
            data: formData,
            success: function(responseText){
                $responseData = JSON.parse(responseText);
                if($responseData.status == 'faltaimagen'){
                    $(".divfaltacliente").fadeIn(2000);
                    $(".divfaltacliente").fadeOut(5000);
                }
                else if($responseData.status == 'tamañoincorrecto'){
                    $(".divclientetamaño").fadeIn(2000);
                    $(".divclientetamaño").fadeOut(5000);
                }
                else if($responseData.status == 'success'){
                    location.reload();
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });


    
    //View para crear proyecto
    $("#myFormProject").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/projects/postUpload',
            type:'POST',
            data: formData,
            success: function(responseText){
                $responseData = JSON.parse(responseText);
                if($responseData.status == 'ok')
                    {
                    $("#formcrearproyecto").fadeOut(1000);
                    $("#formcreartraducciones").fadeIn(1000);
                }
                else if($responseData.status == 'error'){
                   $(".divheaderhometamaños").fadeIn(2000);
                   $(".divheaderhometamaños").fadeOut(5000);

                   //alert($responseData.warning); 
                }
                else if($responseData.status == 'faltahome'){
                    $(".divhomemal").fadeIn(2000);
                    $(".divhomemal").fadeOut(5000);
                }
                else if($responseData.status == 'faltaheader'){
                    $(".divheadermal").fadeIn(2000);
                    $(".divheadermal").fadeOut(5000);
                }
                else if($responseData.status == 'faltanlosdos'){
                    $(".divhomemal").fadeIn(2000);
                    $(".divhomemal").fadeOut(5000);
                    $(".divheadermal").fadeIn(2000);
                    $(".divheadermal").fadeOut(5000);
                }
                else if($responseData.status == 'hometamañoincorrecto'){
                    $(".divhometamañoincorrecto").fadeIn(2000);
                    $(".divhometamañoincorrecto").fadeOut(5000);
                }
                else if($responseData.status == 'headertamañoincorrecto'){
                    $(".divheadertamañoincorrecto").fadeIn(2000);
                    $(".divheadertamañoincorrecto").fadeOut(5000);
                }
                else if($responseData.status == 'headerhometamañoincorrecto'){
                    $(".divheaderhometamañoincorrecto").fadeIn(2000);
                    $(".divheaderhometamañoincorrecto").fadeOut(5000);
                    
                }
                else{
                    alert('Error happened in backend, but not handled');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    $("#UpdateProject").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/projects/{id}',
            type:'POST',
            data: formData,
            success: function(responseText){
                $responseData = JSON.parse(responseText);
                if($responseData.status == 'ok')
                    {
                       
                    }
                else if($responseData.status == 'error'){
                    alert($responseData.warning);
                }
                else{
                    alert('Error happened in backend, but no handled');
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    //View para crear traduccion
    $("#myFormTraduccion").submit(function(e){
        e.preventDefault();
        $.ajax({
            url:'/admin/projects/postUploadTranslation',
            type:'POST',
            data:$('#myFormTraduccion').serializeArray(),
            success: function(){
                $("#formcreartraducciones").fadeOut(1000);
                $("#divclientexist").fadeIn(2000);
                $(".colclientexist").fadeIn(2000);
                $("#divclientdoesntexist").fadeIn(2000);
            }
        });
        return false;
    });

    //Funcion para crear relacion proyecto-cliente cuando el cliente NO existe y se crea manualmente.
    $("#projectclientdoesntexist").submit(function(e){
        console.log('El cliente no existe');
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/projectsclients/postUpload',
            type:'POST',
            data: formData,
            success: function(){
                location.reload();
                //$("#formcrearcliente").modal('hide');
               // $("#divclientexist").hide();
              //  $("#divclientdoesntexist").hide();
              //  $("#listallprojects").fadeIn(1000);
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    //Funcion para crear relacion proyecto-cliente cuando el cliente SI existe.
    $("#projectclientexist").submit(function(e){
        console.log('El cliente existe');
       e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/projectsclientsifexist/postUpload',
            type:'POST',
            data: formData,
            success: function(){
                location.reload();
              //  $("#divclientexist").hide();
              //  $("#divclientdoesntexist").hide();
              //  $("#listallprojects").fadeIn(1000); 
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    //Funcion para hacer la preview de la imagen
    $("#browse_file").on('click',function(e){
        $("#pathheader").click();
    });
    $("#browse_file2").on('click',function(e){
        $("#pathhome").click();
    });
    $("#browse_file3").on('click',function(e){
        $("#pathimage").click();
    });
    $("#pathheader").on('change',function(e){
        var fileInput=this;
        if (fileInput.files[0])
            {
                var reader=new FileReader();
                reader.onload=function(e)
                {
                    $("#img").attr('src',e.target.result);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
    });
    $("#pathimage").on('change',function(e){
        var fileInput=this;
        if (fileInput.files[0])
            {
                var reader=new FileReader();
                reader.onload=function(e)
                {
                    $("#img").attr('src',e.target.result);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
    });

    $("#pathhome").on('change',function(e){
        var fileInput=this;
        if (fileInput.files[0])
            {
                var reader=new FileReader();
                reader.onload=function(e)
                {
                    $("#img2").attr('src',e.target.result);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
    });

    $("#view").click(function(e){
        e.preventDefault();
        $.ajax({
            url:'project/1',
            type:'GET',
        });
        return false;
    });

    $("#buttoncreateclient").click(function(){
        $("#listallclients").hide();
        $("#formcrearcliente").fadeIn(1000);

    });


     $("#buttoncreateemployee").click(function(){ 
        $("#listallemployees").hide(); 
        $("#formcreartrabajador").fadeIn(1000);

    });
    


    $(".buttonchangepublicemployee").click(function(e){
        e.preventDefault();
        var button = $(this);
        var id = button.data('id');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'employee/cambiarVisible/' + id,
            type: 'PUT',
            data: formData,
            success: function() {
                location.reload();
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    $(".buttonchangepublicproject").click(function(e){
        e.preventDefault();
        var button = $(this);
        var id = button.data('id');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'project/cambiarVisible/' + id,
            type: 'PUT',
            data: formData,
            success: function() {
                location.reload();
            },
            cache:false,
            contentType: false,
            processData: false
        });
        return false;
    });

    $("#buttoncreateadmin").click(function(){
        $("#listalladmins").hide();
        $("#formcrearadmin").fadeIn(1000);
    });

    $("#myFormAdmin").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'/admin/admins/postUpload',
            type: 'POST',
            data: formData,
            success: function() {
                location.reload();
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    }); 

$("#tabs").tabs();

//$("#tabs ul li a").droppable({
  //  hoverClass: "drophover",
    //tolerance: "pointer",
    //drop: function(e, ui) {
     //   var tabdiv = $(this).attr("href");
      //      $(tabdiv + " table tr:last").after("<tr>" + ui.draggable.html() + "</tr>");
      //  ui.draggable.remove();
   // }
//});∫
$('.touchtable').on('doubletap',function(e){ 
    mover(); 
}); 
function mover(){ 
    $("#tbodyproject").sortable({ 
    items: "> tr", 
    appendTo: "parent", 
    helper: "clone", 
    placeholder: "placeholder-style", 
    containment: ".table", 
    start: function(event, ui) { 
    
    var cantidad_real = $('.table thead tr th:visible').length; 
    var cantidad_actual = $(this).find('.placeholder-style td').length; 
    
    if(cantidad_actual > cantidad_real){ 
    var cantidad_a_ocultar = (cantidad_actual - cantidad_real); 
    
    for(var i = 0; i <= cantidad_a_ocultar; i++){ 
    $(this).find('.placeholder-style td:nth-child('+ i +')').addClass('hidden-td'); 
    } 
    } 
    
    ui.helper.css('display', 'table') 
    }, 
    stop: function(event, ui) { 
    ui.item.css('display', '') 
    }, 
    update: function( event, ui ) { 
    let newOrder = $(this).sortable('toArray'); 
    $.ajax({ 
    type: "POST", 
    url:'/admin/projects/updateOrder', 
    data: {ids: newOrder} 
    }) 
    .done(function( msg ) { 
    location.reload(); 
    }); 
} 
}).disableSelection(); 
}


if ($('.touchtable').on('doubletap',function(e){
    
$("#tbodyemployee").sortable({
        containment: "#tableemployees",
        items: "> tr",
        appendTo: "parent",
        helper: "clone",
        placeholder: "placeholder-style",
        start: function(event, ui) {
            var cantidad_real = $('.table thead tr th:visible').length;
            var cantidad_actual = $(this).find('.placeholder-style td').length;
              
              if(cantidad_actual > cantidad_real){
              var cantidad_a_ocultar = (cantidad_actual - cantidad_real);
              
              for(var i = 0; i <= cantidad_a_ocultar; i++){
                  $(this).find('.placeholder-style td:nth-child('+ i +')').addClass('hidden-td');
              }
            }
            
            ui.helper.css('display', 'block');
            ui.helper.css('width', '100% !important');
           
          },
        stop: function(event, ui) {
            ui.item.css('display', '')
        },
        update: function( event, ui ) {
            let newOrder = $(this).sortable('toArray');
            $.ajax({
                type: "POST",
                url:'/admin/employees/updateOrder',
                data: {ids: newOrder}
            })
           .done(function( msg ) {
            location.reload();        
           });
        }
    
    }).disableSelection();

}));


$("#urlproyectos").click(function(){
    window.open('http://'+window.location.hostname);
});
$("#urlnosotros").click(function(){
    window.open('http://'+window.location.hostname+'/es/about');
})
$("#urlproyecto").click(function(){
    var valorSlug = document.getElementById("slug").value;
    window.open('http://'+window.location.hostname+'/es/works/'+valorSlug)
})

});


