/*----------*/
/* Workshop */
/*----------*/
require('./app');

var Data = require('./lib/data');
require('./lib/plugins');
require('bootstrap');

$('.workshop form').submit(function(e){
    e.preventDefault();
    var data = new Data();
    var $btn = $('#btn-submit').button('loading');

    data.post($(this).attr('action'), $(this).serializeObject(), function(){})
    .done(function(response){
        $('.msg-errors').remove(); 
         $('.alert').remove();       
        if (!response.success) {
            $.each(response.message, function(i,v){
                var input$ = $('form input#'+i );
                input$.after('<span class="msg-errors">'+v+'</span>');
            });
        } else {
            $('form').trigger("reset");
            $('#wrapper').prepend('<div class="alert alert-success" role="alert">'+response.message+'</div>');
        }
    })
    .fail(function(){
        console.log('fail')
    })
    .always(function(){
        $btn.button('reset')
    });
});