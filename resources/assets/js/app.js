/*----------*/
/* Requires */
/*----------*/
require('gsap');
require('ScrollTo');


/*-----------*/
/*    Vue    */
/*-----------*/
require('./bootstrap');


import Vue from 'vue';
Vue.set(Vue.prototype, '_', _);
window.bus = new Vue()

//LOCALES
import locales from './lib/locales/vue-i18n-locales.generated.js';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

window.i18n = new VueI18n({
	locale: Config.locale,
	fallbackLocale: 'en',
	messages: locales
})

// Hot updates
if (module.hot) {
	module.hot.accept(['./en', './es'], function () {
	  i18n.setLocaleMessage('en', require('./en').default)
	  i18n.setLocaleMessage('es', require('./es').default)
	})
  }

//ROUTER
import VueRouter  from 'vue-router';
Vue.use(VueRouter)

//routes
const lr = '/:lang(es|en)'

const routes=[
	{   
		path: '/',
		redirect: '/es' 
	},
	{   path: lr+'/',
		components: {
			'main-container':require('./components/sections/Home.vue')
		},
		name: 'home',
		meta:{
			scrollTop:true,
			section:'home'
		}        

	},
	{   path: lr+'/contact',
		components: {
			'main-container':require('./components/sections/Contact.vue')
		},
		name: 'contact',
		meta:{
			scrollTop:true,
			section:'contact'
		}         
	},
	{   path: lr+'/services',
		components: {
			'main-container':require('./components/sections/Services.vue')
		},
		name: 'services',
		meta:{
			scrollTop:true,
			section:'services'
		} 
	},
	{   path: lr+'/about',
		components: {
			'main-container':require('./components/sections/About.vue')
		},
		name: 'about',
		meta:{
			scrollTop:true,
			section:'about'
		} 
	},
	{   path: lr+'/works/:slug',
		components: {
			'main-container':require('./components/sections/Project.vue')
		},
		name: 'projects',
		meta:{
			scrollTop:true,
			section:'project'
		}
	},
	{   path: lr+'/legal',
		components: {
			'main-container':require('./components/sections/Legal.vue')
		},
		name: 'legal',
		meta:{
			scrollTop:true,
			section:'legal'
		} 
	},
	{   
		path: '/:url',
		redirect: '/'+Config.locale+'/:url' 
	},
	{   
		path: '*',
		components: {
			'main-container':require('./components/sections/404.vue')
		},
		name: '404',
		meta:{
			scrollTop:true,
			section:'error'
		}        
	}
]

// scrollBehavior:
// - only available in html5 history mode
// - defaults to no scroll behavior
// - return false to prevent scroll
import {scrollBehavior} from './lib/vue-router-scrollbehavior'

//METAS
import VueHead from 'vue-head'
Vue.use(VueHead,{
        title: {
            separator: '·'
        }
    }
)

const router = new VueRouter({
	routes,
	mode: 'history',
	scrollBehavior
})



//IMAGE LAZY LOAD
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)

import VueCookie from 'vue-cookie';
Vue.use(VueCookie);

import VueTyperPlugin from 'vue-typer'
Vue.use(VueTyperPlugin)

var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

//COMPONENTS
import webm from './components/modules/Webm.vue'
Vue.component('webm', webm)

import projectAudio from './components/modules/project/ProjectAudio.vue'
Vue.component('project-audio', projectAudio)

import videoButton from './components/modules/project/VideoButton.vue'
Vue.component('video-button', videoButton)

import vuePicture from './components/modules/project/VuePicture.vue'
Vue.component('vue-picture', vuePicture)

import videoModal from './components/modules/project/VideoModal.vue'
Vue.component('video-modal', videoModal)

//NAVIGATION GUARDS
router.beforeEach((to, from, next) => {
	window.bus.$emit('showPreload')
	setTimeout(() => {
		next();
	}, 1000);
 })

 router.afterEach((to, from) => {
	
 })


 //ANALYTICS
import VueAnalytics from 'vue-analytics'

Vue.use(VueAnalytics, {
    id: 'UA-8165803-32', 
	router,
	checkDuplicatedScript: true,
    debug: {
        enabled: false,
        trace: false,
        sendHitTask: true
    },
    autoTracking: {
		pageviewOnLoad: false,
        pageviewTemplate: function (route) {
            return {
                path: route.path,
                title: document.title,
                location: window.location.href
            }
        }
    }
})

const app = new Vue({
	i18n,
	router,    
	data(){
		return{
			locale:Config.locale,
			menuOpen:false,
			menubHover:false,
		}
	},
	computed:{
		hashtags : function(){
			var array = []
			for (var i = 0; i < 10; i++) {
				array[i]=i18n.t('about.actitud.a'+i);
			}
			return array;
		}
	},
	created(){
		var vm = this;
		window.bus.$on('refresh', function () {
			vm.animationRefresh()
		});
		window.bus.$on('preloaderHidden', function () {

		});
		window.bus.$on('preloaderUpdate', function (p) {
			vm.preloaderUpdate(p.percentage);
		})
	},
	mounted () {
		var vm = this;
		vm.setGotoTop();
		setInterval(vm.doMenuHover,2500)        
		vm.animationInit();
	},
	components:{
		'logo':require('./components//modules/svg/Logo.vue'),
		'preloader':require('./components/modules/Preloader.vue'),
		'header-video':require('./components/modules/Video.vue'),
		'video-modal':require('./components/modules/project/VideoModal.vue'),
		'go-down':require('./components/modules/GoDown.vue'),
		'trans':require('./components/modules/Trans.vue'),
		'worker-list':require('./components/modules/about/WorkerList.vue'),
	},
	watch: {
		menuOpen: function () {
			var vm = this
			var overflowType = vm.menuOpen ? 'hidden':'visible'
			$('html').css('overflow', overflowType);
		}
	},
	methods: {
		setGotoTop: function() {
			$('body').on('click','#goto-top',function(event) {
				event.preventDefault();
				TweenMax.to(window, 1, { scrollTo: { y: 0, autoKill: true } });
			});
		},
		toggleMenu:function(){
			var vm = this;
			vm.menuOpen = !vm.menuOpen;
		},
		doMenuHover:function(){
			var vm = this;
			vm.menubHover = true
			setTimeout(function(){vm.menubHover = false},200)
		},
		changeLanguage(){
			window.location.href='/'+i18n.t('general.other-locale')+this.$route.fullPath.substring(3)
		},
		headerScroll: function() {
			//? TODO: modular?
			var $h = $('#header');

			if ($(document).scrollTop() > 75) {
				$h.addClass('scrolled');
			} else {
				$h.removeClass('scrolled');
			}
		},
		animationInit: function() {
			var vm = this;

			vm.animationRefresh();

			$(window).scroll(function() {
				vm.animationRefresh()
				vm.headerScroll();
			});

			$(window).resize(function() {
				vm.animationRefresh()
			});

		},
		animationAnimate: function() {
			$.each($('.preanimate:first'), function(index, val) {
				if ($(window).height() > 768) {
					var topOfWindow = $(window).scrollTop();
					if ($(this).offset().top < $(window).scrollTop() + ($(window).height() * 0.75)) {
						$(this).removeClass("preanimate").hide().fadeIn(300).addClass("animated");
					}
				} else {
					$(this).removeClass("preanimate");
				}
			});
		},
		animationRefresh: function() {
			var vm = this;
			vm.animationAnimate();
		},
		updateSectionPreloadJson:function(images){
			console.log('updateSectionPreloadJson',images);
		},
		preloaderUpdate: function(percentage) {
			jQuery({ someValue: old_percent }).animate({ someValue: percentage }, {
				duration: 500,
				step: function() {
					var ww = $(window).width();
					$('.percent').css('width', ww - ww * (percentage / 100));
				}
			});

			old_percent = percentage;

		}
	}
	
}).$mount('#app')


/*-----------*/
/* Variables */
/*-----------*/

var old_percent = 0

