<?php

return [
    'meta'  => [
        'description'   => 'Advertising Agency. Barcelona and Madrid',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona',
    ],
    'title' => [
        'common'    => 'Advertising Agency · Barcelona and Madrid',
        'home'      => 'Home',
    ],
];
