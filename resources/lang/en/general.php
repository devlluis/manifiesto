<?php

return array (
  'loading' => 'Loading',
  'see-more' => 'See more',
  'sharer' => 'Don\'t miss this campaign by Manifiesto.biz',
  'share' => 'Share it',
  404 => 'The requested URL <strong>:url</strong> was not found. <br><br>Anyway, never give up.',
  500 => 'Ha ocurrido un error en el servidor.<br><br>No te preocupes, sobrevivirás...',
  'and' => 'and',
  'other-locale' => 'es',
  'goto-top'=>'Go to top'
);
