<?php

return array (
  'works' => 'Projects',
  'about' => 'About Us',
  'services' => 'Services',
  'contact' => 'Contact',
  'otherlocale' => 'es',
  'language' => 'ESP',
);
