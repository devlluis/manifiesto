<?php

return [

/*
	|--------------------------------------------------------------------------
	| Meta Descriptions
	|--------------------------------------------------------------------------
	*/


    'about'     => [
        'description'   => 'If you want to know who we are, come in and meet some part of the team.',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona, team, professional, creative director, strategic planner, account director',
        'title'         => 'About us',
    ],
    'contact'   => [
        'description'   => 'Call us or come and meet us.',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona, team, professional, creative director, strategic planner, account director',
        'title'         => 'Contact',
    ],
    'home'      => [
        'description'   => 'Advertising Agency · Ideas that work · Barcelona Madrid and Mexico',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona',
        'title'         => 'Home',
    ],
    'legal'     => [
        'description'   => 'Advertising Agency · Barcelona and Madrid · Legal terms and Privacy',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona',
        'title'         => 'Legal terms and Privacy',
    ],
    'services'  => [
        'description'   => 'If you want to know what we do in Manifesto, enter and know our services.',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, Barcelona, services, advertising, digital, production, video, social media, strategy, branded content, media, spots, business intelligence',
        'title'         => 'Services',
    ],
    'projects'     => [
        'description'   => 'This is just a sample of our projects and what we can do for your brand.',
        'keywords'      => 'Manifiesto, advertising agency, creativity, campaign, interactive, website, ideas, branding, campaigns, digital, 360, design, planning, advertising, agency, design, interactive, emotions, Barcelona, portfolio, book, reel, projects, case study, video case',
        'title'         => 'Projects',
    ],
];
