<?php

return [
  'title' => 'Desayuno Workshop:'
  ,'title2' => 'UN TOUCH-POINT ESTRATÉGICO'
  ,'subtitle'  => 'Trampas y oportunidades en la “promoción punto de venta”'
  ,'form' => [
    'name' => 'Nombre'
    ,'position' => 'Cargo'
    ,'company'  => 'Empresa'
    ,'email'    => 'E-mail'
    ,'submit'   => 'Enviar'
    ,'success'  => 'Hemos recibido tu petición correctamente. ¡Muchas gracias!'
  ]
];
