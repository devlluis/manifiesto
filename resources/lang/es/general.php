<?php

return array (
  'share' => 'Compártelo',
  'see-more' => 'Ver más',
  'sharer' => 'No te pierdas esta campaña de Manifiesto.biz',
  'loading' => 'Cargando',
  404 => 'La URL <strong>:url</strong> no fue encontrada en este servidor.<br><br> De todos modos, no te des por vencid@.',
  500 => 'There has been an error in the server.<br><br>Don\'t worry, you will survive...',
  'and' => 'y',
  'other-locale' => 'en',
  'goto-top'=>'Subir'
);
