<?php

return [
    'meta'  => [
        'description'   => 'Agencia de publicidad. Barcelona y Madrid',
        'keywords'      => 'Manifiesto, agencia de publicidad, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, emotions, Barcelona',
    ],
    'title' => [
        'common'    => 'Agencia de publicidad · Barcelona y Madrid',
        'home'      => 'Inicio',
    ],
];
