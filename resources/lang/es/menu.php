<?php

return array (
  'works' => 'Proyectos',
  'about' => 'Nosotros',
  'services' => 'Servicios',
  'contact' => 'Contacto',
  'otherlocale' => 'en',
  'language' => 'ENG',
);
