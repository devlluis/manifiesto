<?php

return [

/*
	|--------------------------------------------------------------------------
	| Meta Descriptions
	|--------------------------------------------------------------------------
	*/


    'about'     => [
        'description'   => 'Si quieres saber quién es Manifiesto, entra y conoce a parte del equipo.',
        'keywords'      => 'Manifiesto, agencia de publicidad, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, Barcelona, equipo, team, profesional, director creativo, strategic planner, directora de cuentas,',
        'title'         => 'Nosotros',
    ],
    'contact'   => [
        'description'   => 'Llámanos o ven a conocernos.',
        'keywords'      => 'Manifiesto, agencia de publicidad, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, Barcelona, Madrid, Mexico, Querétaro',
        'title'         => 'Contacto',
    ],
    'home'      => [
        'description'   => 'Agencia de publicidad · Ideas que funcionan · Barcelona Madrid y Mexico',
        'keywords'      => 'Manifiesto, agencia de publicidad, agencia de publicidad Barcelona, agencia de publicidad Madrid, agencia de publicidad Mexico, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, emotions, Barcelona, Madrid, Mexico',
        'title'         => 'Inicio',
    ],
    'legal'     => [
        'description'   => 'Agencia de Publicidad · Barcelona y Madrid · Términos legales y Privacidad',
        'keywords'      => 'Manifiesto, agencia de publicidad, agencia de publicidad Barcelona, agencia de publicidad Madrid, agencia de publicidad Mexico, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, emotions, Barcelona, Madrid, Mexico',
        'title'         => 'Términos legales y Privacidad',
    ],
    'services'  => [
        'description'   => 'Si quieres saber qué hacemos en Manifiesto, entra y conoce nuestros servicios.',
        'keywords'      => 'Manifiesto, agencia de publicidad, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, Barcelona, servicios, publicidad, digital, producción, vídeo, social media, strategy, branded content, medios, spots, business intelligence',
        'title'         => 'Servicios',
    ],
    'projects'     => [
        'description'   => 'Algunos ejemplos de lo que hemos hecho y de lo que podemos hacer por tu marca.',
        'keywords'      => 'Manifiesto, agencia de publicidad, creatividad, campaña, interactiva, web, ideas, branding, campañas, digital, 360, diseño, planificación, advertising, agency, design, interactive, Barcelona, servicios, publicidad, digital, producción, vídeo, social media, strategy, branded content, medios, spots, business intelligence',
        'title'         => 'Proyectos',
    ],
];
