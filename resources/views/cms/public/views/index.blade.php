@extends('cms.public.layouts.default')
@section('content')
<body>
	<div style="width:100%;height:100%;">
		<div id="contenedor_superior" style="width:100%;height:50%;">
			<div id="contenedor_arriba_izquierda" style="width:50%;height:100%;background-color:f15d5e;float:left;">
                <ul style="text-align:center;margin-top:12%">
                    <h3>Crear proyectos</h3><br>
                    <h3>Ver proyectos</h3><br>
                    <h3>Actualizar proyectos</h3><br>
                    <h3>Borrar proyectos</h3>
                </ul>
            </div>
			<div id="contenedor_arriba_derecha" style="width:50%;height:100%;background-color:fed17b;float:right;">
                <ul style="text-align:center;margin-top:12%">
                    <h3>Crear clientes</h3><br>
                    <h3>Ver clientes</h3><br>
                    <h3>Actualizar clientes</h3><br>
                    <h3>Borrar clientes</h3>
                </ul>
            </div> 
		</div>
		<div id="contenedor_inferior" style="width:100%;height:50%;">
            <div id="contenedor_abajo_izquierda" style="width:50%;height:100%;background-color:009bc9;float:left;">
                <ul style="text-align:center;margin-top:12%">
                    <h3>Crear trabajadores</h3><br>
                    <h3>Ver trabajadores</h3><br>
                    <h3>Actualizar trabajadores</h3><br>
                    <h3>Borrar trabajadores</h3>
                </ul>
            </div>
            <div id="contenedor_abajo_derecha" style="width:50%;height:100%;background-color:50b948;float:right;">
                <ul style="text-align:center;margin-top:12%">
                    <h3>Crear administradores</h3><br>
                    <h3>Ver administradores</h3><br>
                    <h3>Actualizar administradores</h3><br>
                    <h3>Borrar administradores</h3>
                </ul>
            </div>
        </div>
	</div>
</body>
@stop