@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-10">
<h3 style="letter-spacing:40px;text-align:center;color:fed17b;">CLIENTES</h3>
</div>

<div class="col-md-12">
    @include('cms.public.views.partials._messages')			
		<div class="col-md-4">
		<h3 style="color:fed17b;">About Client</h3>
			
            <p class="lead">Id: {{ $client->id}}</p>
            <p class="lead">Name: {{ $client->name}}</p>
            <p class="lead">Slug: {{ $client->slug}}</p>
		<br><br>
      	</div>
		<div class="col-md-4">

		</div>
		<div class="col-md-4">

			<div class="well" style="background-color:black">

				<dl class="dl-horizontal">
					<dt>Create At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($client->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($client->updated_at)) }}</dd>
				</dl>

				<hr>

				<div class="row">
					<div class="col-sm-6">
    					<a href="{{ route('admin.clients.edit', $client->id)}}" class="btn btn-primary btn-block">Edit</a>
					</div>
					<div class="col-sm-6">
						<form method="POST" action="{{route('admin.clients.destroy',$client->id)}}" onsubmit="return ConfirmarBorrar()">
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
						<input type="hidden" name="_token" value="{{Session::token()}}">
						{{method_field('DELETE')}}
						</form>
					</div>
				</div>

			</div>

		</div>

</div>

	<script>
		function ConfirmarBorrar(){

			var x = confirm("¿Estas seguro de borrar este cliente?");
			if(x)
				return true;
			else 
				return false;
		}
	</script>

@endsection