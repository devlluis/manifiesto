@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-12" id="editclient">
  
  <div class="col-md-12">
    <h3 class="h3client h3clienteedit">CLIENTES</h3>
  </div>
  
    <div class="col-md-12" id="partialclientedit">
      @include('cms.public.views.partials._messages')	
    </div>
    
    <div class="col-md-4" id="editformclient">

    <h3>{{$client->name}}</h3>
    <hr>

      <form method="POST" action="{{ route('admin.client.update', $client->id) }}" enctype="multipart/form-data">
         
            <div class="form-group">
                <label><strong>Nombre</strong></label>
    	        <input type="text" class="form-control form-control-sm resizenone inputeditclient" id="name" name="name" value="{{$client->name}}"></input>

            </div>

    		  <div class="form-group">
    			  <label><strong>Slug</strong></label>
     			  <input type="text" class="form-control form-control-sm resizenone inputeditclient" id="slug" name="slug" value="{{$client->slug}}"></input>
   	  		  <button type="submit" class="btn btn-success updateeditclient btn-block canceleditclient2">Save Changes</button>
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                  {{ method_field('PUT') }}
          </div>

    </div>
            <br><br>
    <div class="col-md-4">
    <div class="form-group">
           <table class="tableeditclient">
                <tr>
                  <td>
                      <img src="{{ asset('/storage/clients/'.$client->slug.'.png') }}" id="img" class="img imgeditclient" onerror="this.src='{{asset('/storage/clients/'.'notfound.png')}}'">
                      <input type="file" name="pathimage" id="pathimage"  class="form-control-file" aria-describedby="fileHelp" style="display:none;">
                  </td>
                 
                </tr>
                <tr>
                  <td>
                      <input type="button" name="" value="Seleccionar imagen" id="browse_file3" class="btn btn-danger form-control seleccionarimgclientedit">
                      <p style="color:black;" class="psizeimage ">200x200</p>
                  </td>
                </tr>
            </table>
     </div>
    </div>

    <div class="col-md-4" id="createdupdatedclient">
      
      <div class="well welleditclient">

        <dl class="dl-vertical">
          <dt>Creado el:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($client->created_at)) }}</dd>
        </dl>

        <dl class="dl-vertical">
          <dt>Última actualización:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($client->updated_at)) }}</dd>
        </dl>
        <hr>

        <div class="row roweditclient">
              <div class="col-sm-6">
                  <a href="{{ route('admin.clients') }}" class="btn btn-danger canceleditclient btn-block">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <button type="submit" class="btn btn-success updateeditclient btn-block">Guardar</button>
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                  {{ method_field('PUT') }}
              </div>
        </div> 
        </form> 
      </div>

    </div>
    <div class="col-md-12 listprojects">
    <hr class="hroneditclient">

@if ($client_projects->count())
    <h3 class="h3client h3clienteedit">Proyectos de {{$client->name}}</h3>

          @foreach ($client_projects as $project)

            <div class="col-md-3 col3projectofclient">
              <h4 class="h4traductiontitle"><a href="/admin/project/{{$project->id}}/edit" class="atraductiontitle">{{$project->title}}</a></h4>
              <a href="/admin/project/{{$project->id}}/edit"><img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}" class="imgprojectoneditclient"></a>
            </div>

          @endforeach
  @endif
    </div>
</div>
@endsection