@extends('cms.public.layouts.default')
@section('content')
<div id="listallclients">

  <div class="col-md-10" id="clients">
    <h3 class="h3cliente">CLIENTES</h3>
  </div>

  <div class="col-md-2" id="buttonaddclient">
    <button type="button" id="buttoncreateclient" class="btn btn-danger">Crear Cliente</button>
  </div>

 <div class="col-md-12" id="partialclient">
      @include('cms.public.views.partials._messages')	
    </div>

  <div class="col-md-12">

    <div id="tableclients">		
      <table class="table">
        <thead>
          <tr>
            <th class="columnidclient thcenter">ID<a href="{{route('admin.clients.order', ['field' => 'id','order' => 'asc'])}}"><i class="fa fa-caret-up" id="orderByAsc"></i></a><a href="{{route('admin.clients.order',['field' => 'id','order' => 'desc'])}}"><i class="fa fa-caret-down" id="orderByIdDesc"></i></a></th>
            <th>Nombre<a href="{{route('admin.clients.order', ['field' => 'name','order' => 'asc'])}}"><i class="fa fa-caret-up" id="orderByAsc"></i></a><a href="{{route('admin.clients.order',['field' => 'name','order' => 'desc'])}}"><i class="fa fa-caret-down" id="orderByNameDesc"></i></a></th>
            <th>Slug<a href="{{route('admin.clients.order', ['field' => 'slug','order' => 'asc'])}}"><i class="fa fa-caret-up" id="orderByAsc"></i></a><a href="{{route('admin.clients.order',['field' => 'slug','order' => 'desc'])}}"><i class="fa fa-caret-down" id="orderBySlugDesc"></i></a></th>
            <th>Imagen</th>
            <th><span class="glyphicon glyphicon-cog"></span>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($clients as $key => $client)
            <tr>
              <td class="idclients tdcenter"><p id="margindata">{{$client->id}}</p></td>
              <td class="nameclient"><p id="margindata" class="pclient">{{$client->name}}</p></td>
              <td class="slugclient"><p id="margindata" class="pclient">{{$client->slug}}</p></td>
              <td class="imgclient"><img src="{{ asset('/storage/clients/'.$client->slug.'.png')}}" class="sizeimageclient" onerror="this.src='{{asset('/storage/clients/'.'notfound.png')}}'"></td>
              <td class="actionsclient">
                <a href="{{ route('admin.clients.edit', $client->id)}}" class="btn btn-success btn-sm editmenuclient" id="margindata">Editar</a>
                    <button type="button" class="btn btn-danger btn-sm deletemenuclient" data-toggle="modal" data-target="#formdeleteclient_{{$client->id}}" id="margindata">Borrar</button>
              
              <div id="formdeleteclient_{{$client->id}}" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
      
         <div class="modal-dialog" style="">

         <div class="modal-content" style="">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <h4 class="modal-title" style="color:black;">¿Estas seguro de borrar el cliente?</h4>

            </div>

            <div class="modal-body">

                <div class="col-sm-6">
                            <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-block colsm6btnno">No</a>
                </div>
                <div class="col-sm-6">
                    <form method="POST" action="{{route('admin.clients.destroy',$client->id)}}">
                        <input type="submit" value="Si" class="btn btn-danger btn-block colsm6btnsi">
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                        {{method_field('DELETE')}}
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                    
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>

            </div>

         </div>

       </div>

     </div>
              </td>
              </tr>
            @endforeach
          </tbody>

        </table>

      </div>
  <br><br>
    </div>
  </div>
<div id="formcrearcliente" class="col-md-12"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
    <div>
    <h3>Crear nuevo cliente</h3>
    <div class="col-md-12 divfaltacliente" style="display:none;">
      <p class="pDivError">Para poder crear un cliente tienes que añadir una imagen.</p>
    </div>
    <div class="col-md-12 divclientetamaño" style="display:none;">
      <p class="pDivError">El tamaño de la imagen no es el correcto.</p>
    </div>
    <div id="formcreateclient">
        <form  enctype="multipart/form-data" id="myFormClient" name="myFormClient">
         <input type="hidden" name="_token" value="{{ Session::token() }}">

          <div class="form-group">
            <label name="name">Nombre:</label>
            <input type="text" id="nameClient" name="nameClient" placeholder="Generalitat de Catalunya" class="form-control form-control-sm" required><br>
            <label name="slug">Slug:</label>
            <input type="text" id="slugClient" name="slugClient" placeholder="generalitat-de-catalunya" class="form-control form-control-sm" required><br>
            <br>
            <table class="tablecreateemployee">
                <tr>
                  <td>
                      <img src="" id="img" class="img" style="background-color:#ccc;border:2px solid gray;width:200px;height:200px;" onerror="this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='">
                      <input type="file" name="pathimage" id="pathimage"  class="form-control-file" aria-describedby="fileHelp">
                  </td>
                </tr> 
                <tr>
                  <td>
                      <input type="button" name="" value="Seleccionar imagen" id="browse_file3" class="btn btn-danger seleccionarimgclientcreate form-control">
                      <p class="pclientinfo">200x200</p>
                  </td>
                </tr>
              </table>
            
            <input type="submit" value="Crear Cliente" id="createclientsubmit" class="btn btn-danger btn-md">
            <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-block hrefcancel">Cancelar</a>

            
            <br><br><br>

          </div>
        </form>

      </div>
</div> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 END HERE-->

@stop