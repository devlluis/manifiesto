@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-10">
    <h3 style="letter-spacing:40px;text-align:center;color:f15d5e;">PROYECTOS</h3>
</div>

<div class="col-md-12">
    @include('cms.public.views.partials._messages')			
		<div class="col-md-4">
		<h3 style="color:f15d5e;">About Project</h3>
			
            <p class="lead">Id: {{ $project->id}}</p>
            <p class="lead">Slug: {{ $project->slug}}</p>
            <p class="lead">Order: {{ $project->order}}</p>
            <p class="lead">Public: {{ $project->public}}</p>
			<p class="lead"><img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}"style="width:400px;"><br>Header</p><br>
		<p class="lead"><img src="{{ asset('/storage/projects/'.$project->slug.'/home.jpg') }}" style="width:300px;"><br>Home</p><br><br>
		
			<br><br><br><br><br><br><br>
      	</div>
		<div class="col-md-4">
		<h3 style="color:f15d5e;">About Translation</h3>
		@foreach ($translation as $locale)
			<p class="lead">Id: {{$locale->id}}</p>
			<p class="lead">Locale: {{$locale->locale}}</p>
			<p class="lead">Project id: {{$locale->project_id}}</p>
			<p class="lead">Title: {{$locale->title}}</p>
			<p class="lead">Caption: {{$locale->caption}}</p>
		@endforeach
		</div>
		<div class="col-md-4">

			<div class="well" style="background-color:black">

				<dl class="dl-horizontal">
					<dt>Create At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($project->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($project->updated_at)) }}</dd>
				</dl>

				<hr>

				<div class="row">
					<div class="col-sm-6">
    					<a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-primary btn-block">Edit</a>
					</div>
					<div class="col-sm-6">
						<form method="POST" action="{{route('admin.projects.destroy',$project->id)}}" onsubmit="return ConfirmarBorrar()">
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
						<input type="hidden" name="_token" value="{{Session::token()}}">
						{{method_field('DELETE')}}
						</form>
					</div>
				</div>

			</div>

		</div>
		</div>
		
</div>

	<script>
		function ConfirmarBorrar(){

			var x = confirm("¿Estas seguro de borrar este proyecto?");
			if(x)
				return true;
			else 
				return false;
		}
	</script>

@endsection