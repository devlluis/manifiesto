@extends('cms.public.layouts.default')
@section('content')
<div class="col-md-10">
    <h3 style="letter-spacing:40px;text-align:center;color:f15d5e;">PROYECTOS</h3>
</div>

<div id="orderByIdAsc" style="display:none"> <!-- DIV TO LIST ALL THE PROJECTS START HERE -->
        <div class="col-md-2" style="padding:20px;">
          <button type="button" id="buttoncreate" class="btn btn-danger">Crear Proyecto</button>

        </div>
                      <table class="table">
                  <thead style="color:white">
                    <tr>
                      <th><a href="{{ route('admin.projects.orderByIdAsc')}}"><span class="glyphicon glyphicon-arrow-up" id="orderByIdAsc"></span></a>Id<a href="{{ route('admin.projects.orderByIdDesc')}}"><span class="glyphicon glyphicon-arrow-down" id="orderByIdDesc"></span></a></th>
                      <th>Slug</th>
                      <th><a href="{{ route('admin.projects.orderByOrderAsc')}}"><span class="glyphicon glyphicon-arrow-up" id="orderByOrderAsc"></span></a>Order<a href="{{ route('admin.projects.orderByOrderDesc')}}"><span class="glyphicon glyphicon-arrow-down" id="orderByOrderDesc"></span></a></th>
                      <th><a href="{{ route('admin.projects.orderByPublicAsc')}}"><span class="glyphicon glyphicon-arrow-up" id="orderByPublicAsc"></span></a>Public<a href="{{ route('admin.projects.orderByPublicDesc')}}"><span class="glyphicon glyphicon-arrow-down" id="orderByPublicDesc"></span></a></th>
                      <th>Fecha creación</th>
                      <th>Fecha ultima actualización</th>
                      <th><span class="glyphicon glyphicon-cog"></span></th>
                    </tr>
                  </thead>
                  <tbody style="color:white">
                  @foreach ($projects as $key => $project)
                    <tr>
                      <th>{{$project->id}}</th>
                      <td>{{$project->slug}}</td>
                      <td>{{$project->order}}</td>
                      <td>{{$project->public}}</td>
                      <td>{{ date('M j, Y', strtotime($project->created_at))}}</td>
                      <td>{{ date('M j, Y', strtotime($project->updated_at))}}</td>
                      <td><a href="{{ route('admin.projects.show', $project->id)}}" class="btn btn-info btn-sm">View</a> <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm">Edit</a>
                  @endforeach
                    </tr>
                  </tbody>
                </table>
  <br><br>
</div>  <!-- DIV TO LIST ALL THE PROJECTS END HERE -->

<div id="orderByIdDesc"> <!-- DIV TO LIST ALL THE PROJECTS START HERE -->
        <div class="col-md-2" style="padding:20px;">
          <button type="button" id="buttoncreate" class="btn btn-danger">Crear Proyecto</button>

        </div>
                      <table class="table">
                  <thead style="color:white">
                    <tr>
                      <th><span class="glyphicon glyphicon-arrow-up" href="{{ route('admin.projects.orderByIdAsc')}}" id="orderByIdAsc"></span>Id<span class="glyphicon glyphicon-arrow-down" href="{{ route('admin.projects.orderByIdDesc')}}" id="orderByIdDesc"></span></th>
                      <th>Slug</th>
                      <th><span class="glyphicon glyphicon-arrow-up" href="{{ route('admin.projects.orderByOrderAsc')}}" id="orderByOrderAsc"></span>Order<span class="glyphicon glyphicon-arrow-down" href="{{ route('admin.projects.orderByOrderDesc')}}" id="orderByOrderDesc"></span></th>
                      <th><span class="glyphicon glyphicon-arrow-up" href="{{ route('admin.projects.orderByPublicAsc')}}" id="orderByPublicAsc"></span>Public<span class="glyphicon glyphicon-arrow-down" href="{{ route('admin.projects.orderByPublicDesc')}}" id="orderByPublicDesc"></span></th>
                      <th>Fecha creación</th>
                      <th>Fecha ultima actualización</th>
                      <th><span class="glyphicon glyphicon-cog"></span></th>
                    </tr>
                  </thead>
                  <tbody style="color:white">
                  @foreach ($projects as $key => $project)
                    <tr>
                      <th>{{$project->id}}</th>
                      <td>{{$project->slug}}</td>
                      <td>{{$project->order}}</td>
                      <td>{{$project->public}}</td>
                      <td>{{ date('M j, Y', strtotime($project->created_at))}}</td>
                      <td>{{ date('M j, Y', strtotime($project->updated_at))}}</td>
                      <td><a href="{{ route('admin.projects.show', $project->id)}}" class="btn btn-info btn-sm">View</a> <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm">Edit</a>
                  @endforeach
                    </tr>
                  </tbody>
                </table>
  <br><br>
</div>  <!-- DIV TO LIST ALL THE PROJECTS END HERE -->


@stop
