@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-12" id="editproject">
<div class="col-md-12">
    <h3 class="h3project h3projectedit">PROYECTOS</h3>
</div>

<div class="col-md-12" id="partialprojectedit">
            @include('cms.public.views.partials._messages')			
</div>

  <div class="col-md-4 col4editproject">

    <h3 class="h3slugprojectedit2">{{$project->slug}}</h3>
    <hr>

    <form method="POST" action="{{ route('admin.project.update', $project->id) }}" enctype="multipart/form-data">
         
            <div class="form-group">
                <label><strong>Slug</strong></label>
    	          <input type="text" class="form-control form-control-sm resizenone" id="slug" name="slug" value="{{$project->slug}}" required ></input>
    	      </div>

    			  <div class="form-group">
     			      <input type="number" class="form-control form-control-sm resizenone" style="display:none;" id="order" name="order" rows="1" value="{{$project->order}}"></input>
   	  			</div>
            <div class="form-group">
                <label><strong>Visible</strong></label><br>
                @if ($project->public==1)
                <input type="radio" name="public" value="1" class="visible" id="public1" checked>
                <label for="public1">Sí</label>
                <input type="radio" name="public" value="0" class="novisible" id="public0">
                <label for="public0">No</label>
                @elseif ($project->public==0)
                <input type="radio" name="public" value="1" class="visible" id="public1">
                <label for="public1">Sí</label>
                <input type="radio" name="public" value="0" class="novisible" id="public0" checked>
                <label for="public0">No</label>
                @endif
            </div>
             <table class="edittableproject">
                <tr>
                  <td>
                      <img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}" id="img" class="img">
                      <input type="file" name="pathheader" id="pathheader" class="form-control-file" aria-describedby="fileHelp"><br>
                      <input type="button" name="" value="Seleccionar header" id="browse_file" class="btn btn-danger form-control seleccionarheaderedit">
                      <p class="psizeimage editproject">1920x812</p>
                  </td>
                </tr>
                <tr>
                  <td>
                      <img src="{{ asset('/storage/projects/'.$project->slug.'/home.jpg') }}" id="img2" class="img2">
                      <input type="file" name="pathhome" id="pathhome" class="form-control-file" aria-describedby="fileHelp"><br>
                      <input type="button" name="" value="Seleccionar home" id="browse_file2" class="btn btn-danger form-control seleccionarhomeedit">
                      <p class="psizeimage editproject">768x345</p>
                  </td>
                </tr><br>
            </table>

    </div>
    <div class="col-md-4 col4editproject">
    <h3 class="h3slugprojectedit2">Actualizar traducción</h3>
    <hr>
      @foreach ($translation as $locale)
      <div class="form-group">
    	  <label>
            @if ($locale->locale=='en') 
              <img src="/cms/img/flags/uk.gif" class="imgedit">
            @elseif ($locale->locale=='es')
              <img src="/cms/img/flags/es.gif" class="imgedit">
            @endif
        </label>

   	  </div>
      <div class="form-group">
        <label><strong>Title</strong></label>
        <input type="text" class="form-control form-control-sm resizenone" name="title[]" value="{{$locale->title}}"></input>
      </div>

      <div class="form-group">
        <label><strong>Caption</strong></label>
        <input type="text" class="form-control form-control-sm resizenone" name="caption[]" rows="2" value="{{$locale->caption}}"></input>
      </div>
      <input type="hidden" name="locale_id[]" value="{{$locale->id}}" hidden>
      <input type="hidden" name="locale[]" value="{{$locale->locale}}"></input>

      @endforeach
<br><br>    </div>

    <div class="col-md-4">
      <div class="well welleditproject">

        <dl class="dl-vertical">
          <dt>Creado el:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($project->created_at)) }}</dd>
        </dl>

        <dl class="dl-vertical">
          <dt>Última actualización:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($project->updated_at)) }}</dd>
        </dl>
        <hr>

        <div class="row">
              <div class="col-sm-6">
                  <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block canceleditproject">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <button type="submit" class="btn btn-success btn-block updateeditproject">Guardar</button>
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                  {{ method_field('PUT') }}
              </div>
              <button type="button" class="btn btn-primary buttonpreviewproject" target="_blank" id="urlproyecto" >
                                     Ver Proyecto
              </button>
              
              <h4>Cliente: 
              <a href="/admin/client/{{$client->id}}/edit">{{$client->name}}</a> 
              </h4>
        </div> 
        
      </div>
    </div>


</form>
</div>
</div>
@endsection