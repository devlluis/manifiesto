@extends('cms.public.layouts.default')
@section('content')

<div id="listallprojects"> <!-- DIV TO LIST ALL THE PROJECTS START HERE -->

    <div class="col-md-10" id="projects">
      <h3 class="h3project">PROYECTOS</h3>
    </div>

    <div class="col-md-2" id="buttonaddproject">
        <button type="button" id="buttoncreate" class="btn">Crear Proyecto</button>
    </div>
    <div class="col-md-12" id="partialproject">
                @include('cms.public.views.partials._messages')	
    </div>	

    <div id="tabs">
        <div class="col-md-12">
            <div id="table1">
              <table class="table touchtable">
                <thead>
                  <tr>
                    <th class="thcenter idprojectth thmenuproject"><!--<a href="{{route('admin.projects.order', ['field' => 'id','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByIdAsc"></span></a>-->ID<!--<a href="{{route('admin.projects.order', ['field' => 'id','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByIdDesc"></span></a>--></th>
                    <th class="thcenter visibleproject thmenuproject"><!--<a href="{{route('admin.projects.order', ['field' => 'slug','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderBySlugAsc"></span></a>-->Visible<!--<a href="{{route('admin.projects.order',['field' => 'slug','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderBySlugDown"></span></a>--></th>
                    <th class="nombreproject thmenuproject"><!--<a href="{{route('admin.projects.order', ['field' => 'order','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByOrderAsc"></span></a>-->Slug<!--<a href="{{route('admin.projects.order', ['field' => 'order','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByOrderDesc"></span></a>--></th>
                    <th class="headerproject thmenuproject">Header</th>
                    <th class="homeproject thmenuproject">Home</th>
                    <th class="thcenter ordenproject thmenuproject"><!--<a href="{{route('admin.projects.order', ['field' => 'public','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByPublicAsc"></span></a>-->Orden<!--<a href="{{route('admin.projects.order', ['field' => 'public','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByPublicDesc"></span></a>--></th>
                    <th class="thaccionesproyectos thmenuproject"><span class="glyphicon glyphicon-cog"></span>Acciones</th>
                  </tr>
                </thead>

                <tbody id="tbodyproject"> 
                  @foreach ($projects as $key => $project)
                    <tr id="{{$project->id}}" class="trdrag">
                      <td class="idrow tdcenter"><p id="margindata">{{$project->id}}</p></td>
                      <td class="visibleproject tdcenter"> 
                      <form action="{{route('admin.projects.cambiarVisible',$project->id)}}">
                      <button type="button" id="buttonchangepublicproject" data-id="{{$project->id}}" class="buttonchangepublicproject">
                      @if ($project->public == '1')
                          <i class="fa fa-check" aria-hidden="true" id="margindata"></i>
                      @else
                      <i class="fa fa-times" aria-hidden="true" id="margindata"></i>
                      @endif
                      </button>
                      <input type="hidden" name="_token" value="{{Session::token()}}">
                      </form>
                      </td>
                      <td class="nameproject"><p id="margindata" class="namemenuproject">{{$project->slug}}</p></td>
                      <!--<td class="nameproject2"><p id="margindata" class="namemenuproject">{{$project->slug}}</p>-->
                      <td class="nameproject2"><p id="margindata" class="namemenuproject">{{$project->slug}}</p>
                    
                      @if ($project->public == '1')
                            <p class="pvisible">Visible:</p><i class="fa fa-check" aria-hidden="true" id="margindata"></i>
                      @else
                            <p class="pvisible">Visible:</p><i class="fa fa-times" aria-hidden="true" id="margindata"></i>
                      @endif
                      </td>
                      <td class="imgproject" id="headerimgmenuproject"><img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}" class="sizeheader"></td>
                      <!--<td class="imgproject" id="headerimgmenuproject2"><img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}" class="sizeheader">-->
                      <td class="imgproject headerimgmenuproject2" id=""><img src="{{ asset('/storage/projects/'.$project->slug.'/header.jpg') }}" class="sizeheader">
                                                  <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm editmenuproject" id="margindata">Edit</a> 
                      </td>
                      <!--<td class="imgproject" id="homeimgmenuproject"><img src="{{ asset('/storage/projects/'.$project->slug.'/home.jpg')}}" class="sizehome"></td>-->
                      <td class="imgproject" id="homeimgmenuproject2"><img src="{{ asset('/storage/projects/'.$project->slug.'/home.jpg')}}" class="sizehome"></td>
                      <td class="imgproject homeimgmenuproject" id=""><img src="{{ asset('/storage/projects/'.$project->slug.'/home.jpg')}}" class="sizehome">
                      <form method="POST" action="{{route('admin.projects.destroy',$project->id)}}" onsubmit="return ConfirmarBorrarProyecto()" class="editdeleteproyecto editdeleteproyectoinsideorden">
                      <!--<form method="POST" action="{{route('admin.projects.destroy',$project->id)}}" onsubmit="return ConfirmarBorrarProyecto()" class="editdeleteproyecto hidden-td">-->
						                  <input type="submit" value="Delete" class="btn btn-danger btn-sm deletemenuproject" id="margindata">
						                  <input type="hidden" name="_token" value="{{Session::token()}}">
					                  	{{method_field('DELETE')}}
                      </form>
                      </td>
                      <td class="orderproject tdcenter hidden-td"><p id="margindata">{{$project->order}}</p>
                        <div class="editdeleteproyecto editdeleteproyectoinsideorden">
                            <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm editmenuproject" id="margindata">Editar</a> 
                            <button type="button" class="btn btn-danger btn-sm deletemenuproject" data-toggle="modal" data-target="#formdeleteproject_{{$project->id}}" id="margindata">Borrar</button>
                        </div>
                      </td>
                      <td class="orderproject tdcenter"><p id="margindata">{{$project->order}}</p>
                      <div class="editdeleteproyecto editdeleteproyectoinsideorder">
                        <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm editmenuproject" id="margindata">Editar</a>
                        <button type="button" class="btn btn-danger btn-sm deletemenuproject" data-toggle="modal" data-target="#formdeleteproject_{{$project->id}}" id="margindata">Borrar</button>
                      </div>
                          <div id="formdeleteproject_{{$project->id}}" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
                             <div class="modal-dialog" style="background-color:#23517F;">
                             <div class="modal-content" style="background-color:#23517F;">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                               <h4 class="modal-title" style="color:black;">¿Estas seguro de borrar el proyecto?</h4>
                             </div>
                             <div class="modal-body">
                              <div class="col-sm-6">
                                <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block colsm6btnno">No</a>
                              </div>
                              <div class="col-sm-6">
                               <form method="POST" action="{{route('admin.projects.destroy',$project->id)}}">
                                <input type="submit" value="Si" class="btn btn-danger btn-block colsm6btnsi project">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                {{method_field('DELETE')}}
                               </form>
                              </div>
                             </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>
                            </div>
                         </div>
                         </div>
                         </div>

                      <td class="formactionsmenuprojects">

                        <a href="{{ route('admin.project.edit', $project->id)}}" class="btn btn-success btn-sm editmenuproject" id="margindata">Editar</a> 
                        <button type="button" class="btn btn-danger btn-sm deletemenuproject" data-toggle="modal" data-target="#formdeleteproject_{{$project->id}}" id="margindata">Borrar</button>
                        
                      </td>

                        <div id="formdeleteproject_{{$project->id}}" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
                        <div class="modal-dialog" style="">
                        <div class="modal-content" style="">
                          <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" style="color:black;">¿Estas seguro de borrar el proyecto?</h4>
                          </div>
                          <div class="modal-body">
                            <div class="col-sm-6">
                              <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block colsm6btnno">No</a>
                            </div>
                            <div class="col-sm-6">
                              <form method="POST" action="{{route('admin.projects.destroy',$project->id)}}">
                                <input type="submit" value="Si" class="btn btn-danger btn-block colsm6btnsi project">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                 {{method_field('DELETE')}}
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>
                          </div>
                        </div>
                       </div>
                       </div>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <br><br>
            </div>
        </div>
    </div>
</div>  <!-- DIV TO LIST ALL THE PROJECTS END HERE -->
<div id="formcrearproyecto" class="col-md-12">
<h3>Crear nuevo proyecto</h3>

                  <div class="col-md-12 divheadermal" style="display:none;">
                    <p class="pDivError">Para poder crear un proyecto tienes que añadir una imagen como header.</p>
                  </div>

                  <div class="col-md-12 divhomemal" style="display:none;">
                    <p class="pDivError">Para poder crear un proyecto tienes que añadir una imagen como home.</p>
                  </div>

                  <div class="col-md-12 divheaderhometamaños" style="display:none;">
                    <p class="pDivError">El tamaño de las imagenes no es el correcto.</p>
                  </div>

                  <div class="col-md-12 divhometamañoincorrecto" style="display:none;">
                    <p class="pDivError">El tamaño de la imagen home no es el correcto.</p>
                  </div>

                  <div class="col-md-12 divheadertamañoincorrecto" style="display:none;">
                    <p class="pDivError">El tamaño de la imagen header no es el correcto.</p>
                  </div>

                  <div class="col-md-12 divheaderhometamañoincorrecto" style="display:none;">
                    <p class="pDivError">El tamaño de las dos imagenes no es el correcto.</p>
                  </div>
    <div id="formcreateproject">
        <form  enctype="multipart/form-data" id="myFormProject" name="myFormProject">
         <input type="hidden" name="_token" value="{{ Session::token() }}">

          <div class="form-group">
            <label name="title">Slug:</label>
            <input type="text" id="slug" name="slug" placeholder="ejemplo-de-slug" class="form-control form-control-sm slugnameproject slugnameprojectcreate" required><br><br>
      <!--      <label name="order">Order:</label>
            <input type="number" id="order" name="order" class="form-control form-control-sm">-->
            <!--<label name="public">Public:</label>-->
              <table class="tablecrearproyecto">
                <tr>
                  <td>

                      <img src="" id="img" class="img imgcreate" onerror="this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='">
                      <input type="file" name="pathheader" id="pathheader"  class="form-control-file" aria-describedby="fileHelp">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="button" name="" value="Seleccionar header" id="browse_file" class="btn btn-danger form-control seleccionar" required>
                    <p class="pprojectimageinfo">1920x812</p>
                  </td>
                </tr>
                <tr>
                  <td>
                  
                      <img src="" id="img2" class="img2 imgcreate" onerror="this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='">
                      <input type="file" name="pathhome" id="pathhome" class="form-control-file" aria-describedby="fileHelp"><br>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="button" name="" value="Seleccionar home" id="browse_file2" class="btn btn-danger form-control seleccionar">
                      <p class="pprojectimageinfo">768x345</p>

                  </td>
                </tr>
              </table><br>
            <input type="submit" value="Crear Proyecto" id="createprojectsubmit" class="btn btn-danger btn-md">
            <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block hrefcancel">Cancelar</a>
            <br><br><br>

          </div>
        </form>

      </div>
</div> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 END HERE-->

<div id="formcreartraducciones" class="col-md-12">
      <h3>Crear nuevas traducciones</h3><br>
        <form enctype="multipart/form-data" id="myFormTraduccion" name="myFormTraduccion"><!--FIRST FORM TO TRANSLATE -->
          <input type="hidden" name="_token" value="{{ Session::token() }}">
            <div class="form-group">
              <div class="col-md-6 col6translate">
                  <p class="lead"><img src="/cms/img/flags/uk.gif" class="ukflag"></p>
                  <!--<input type="text" id="locale-0" name="locale-0" value="en" class="form-control form-control-sm">-->
                  <label name="Title">Title:</label>
                  <input type="text" id="title-0" name="title-0" class="form-control form-control-sm inputstranslate" required><br>
                  <label name="Caption">Caption:</label>
                  <input type="text" id="caption-0" name="caption-0" class="form-control form-control-sm inputstranslate" required><br>
              </div>
              <div class="col-md-6 col6translatees">
                  <p class="lead"><img src="/cms/img/flags/es.gif" class="esflag" ></p>
                  <!--<input type="text" id="locale-1" name="locale-1" value="es" class="form-control form-control-sm">-->
                  <label name="Titulo">Titulo:</label>
                  <input type="text" id="title-1" name="title-1" class="form-control form-control-sm inputstranslate" required><br>
                  <label name="Subtitulo">Subtitulo:</label>
                  <input type="text" id="caption-1" name="caption-1" class="form-control form-control-sm inputstranslate" required><br>
              </div>
              <input type="submit" value="Crear Traduccion" id="createtranslatesubmit" class="btn btn-danger btn-md">
              <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block hrefcanceltraduccion">Cancelar</a>
              <br><br><br>
            </div>
       </form>  
</div>

<div id="divclientexist" class="col-md-12">

     <h3 class="h3elclienteexiste">Selecciona al cliente que pertenece el proyecto</h3><br>
      <form enctype="multipart/form-data" id="projectclientexist" name="projectclientexist">

      @foreach ($clients as $key => $client)
          <div class="radio paddingleftprojectclient">
                  <div class="col-md-3 col3">
                    <label class="paddingleftprojectclient"><input type="radio" name="client" value="{{$client->id}}">{{$client->name}}</label>
                  </div>
          </div>
      @endforeach
      <br>
</div>
      <div class="colclientexist col-md-12">
      <button type="submit" id="relationclientexist" name="relationclientexist" class="btn btn-success">Crear relacion Proyecto-Cliente</button><br>
      </form>

      </div>


<div id="divclientdoesntexist" class="col-md-12">

  <h3 class="h3elclientenoexiste">¿El cliente aun no existe?</h3>
  
    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#formcrearcliente" id="relationclientnotexist">Crear nuevo cliente</button>
                <a href="{{ route('admin.projects') }}" class="btn btn-danger btn-block hrefcancelrelacion">Cancelar</a>
    <br><br><br>
    
    <div id="formcrearcliente" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
      
       <div class="modal-dialog">

       <div class="modal-content">

        <div class="modal-header">

           <button type="button" class="close" data-dismiss="modal">&times;</button>

           <h4 class="modal-title">Crear nuevo cliente</h4>

        </div>

        <div class="modal-bodycreateclient">

              <div id="formcreateclient">
                    <form  enctype="multipart/form-data" id="projectclientdoesntexist" name="projectclientdoesntexist">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">

                    <div class="form-group form-group-clienterelacion">
                      <label class="newclientlabels">Nombre:</label>
                      <input type="text" id="nameClient" name="nameClient" placeholder="Generalitat de Catalunya" class="form-control form-control-sm nameClientModal"><br>
                      <label class="newclientlabels">Slug:</label>
                      <input type="text" id="slugClient" name="slugClient" placeholder="generalitat-de-catalunya" class="form-control form-control-sm slugClientModal"><br>
                      <input type="submit" value="Crear Cliente" id="createclientsubmit" class="btn btn-danger btn-md buttoncreateclientmodal">
                      <br><br><br>

                    </div>

                    </form>

              </div>

        </div>

        <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>

        </div>

        </div>

      </div>

    </div>

 
</div>


@stop
