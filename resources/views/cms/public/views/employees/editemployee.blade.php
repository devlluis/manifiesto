@extends('cms.public.layouts.default')
@section('content')
<div class="col-md-12" id="editemployee">

    <div class="col-md-12">
        <h3 class="h3employee h3employeeedit">TRABAJADORES</h3>
    </div>

    <div class="col-md-12" id="partialemployeeedit">
                @include('cms.public.views.partials._messages')			
    </div>

    <div class="col-md-4 editinfoemployee editinputsemployee">
        <h3>{{$employee->name}}</h3>
        <hr>

        <form method="POST" action="{{ route('admin.employee.update', $employee->id) }}" enctype="multipart/form-data">
         
            <div class="form-group">
                <label><strong>Nombre</strong></label>
    	        <input type="text" class="form-control form-control-sm resizenone nameeditemployee" id="name" name="name" value="{{$employee->name}}"></input>
    	    </div>

    		<div class="form-group">
    			<label><strong>#Hashtag</strong></label>
     			<input type="text" class="form-control form-control-sm resizenone hashtageditemployee" id="hashtag" name="hashtag" value="{{$employee->hashtag}}"></input>
   	  		</div>

            <div class="form-group">
                <label><strong>Slug</strong></label>
                <input type="text" class="form-control form-control-sm resizenone slugeditemployee" id="slug" name="slug" value="{{$employee->slug}}"></input>
            </div>

            <div class="form-group">
                <label><strong>Posicion</strong></label>
                <input type="text" class="form-control form-control-sm resizenone positioneditemployee" id="position" name="position" value="{{$employee->position}}"></input>
            </div>

            <div class="form-group">
                <input type="text" class="form-control form-control-sm resizenone" id="order" name="order" rows="1" style="display:none;" value="{{$employee->order}}"></input>
            </div>

            <div class="form-group">
                <label><strong>Visible</strong></label><br>
                @if ($employee->public==1)
                <input type="radio" name="public" value="1" class="visible" id="public1" checked>
                <label for="public1">Sí</label>
                <input type="radio" name="public" value="0" class="novisible" id="public0">
                <label for="public0">No</label>
                @elseif ($employee->public==0)
                <input type="radio" name="public" value="1" class="visible" id="public1">
                <label for="public1">Sí</label>
                <input type="radio" name="public" value="0" class="novisible" id="public0" checked>
                <label for="public0">No</label>
                @endif
            </div>
            <br>
    </div>

    <div class="col-md-4 editinfoemployee editimageemployee" id="imageupdateemployee">
     
     <div class="form-group">
           <table class="tableeditemployee">
                <tr>
                  <td>
                      <img src="{{ asset('/storage/employees/'.$employee->slug.'.jpg') }}" id="img" class="img imgeditemployee">
                      <input type="file" name="pathimage" id="pathimage"  class="form-control-file" aria-describedby="fileHelp" style="display:none;">
                  </td>
                 
                </tr>
                <tr>
                  <td>
                      <input type="button" name="" value="Seleccionar imagen" id="browse_file3" class="btn btn-danger form-control seleccionarimgempedit">
                      <p style="color:black;" class="psizeimage ">300x277</p>
                  </td>
                </tr>
            </table>
     </div>
    </div>

    <div class="col-md-4" id="createdupdatedemployee">
      <div class="well welleditemployee" style="color:black;">

        <dl class="dl-vertical" style="text-align: center;">
          <dt>Creado el:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($employee->created_at)) }}</dd>
        </dl>

        <dl class="dl-vertical" style="text-align: center;">
          <dt>Última actualización:</dt>
          <dd>{{ date('M j, Y h:i:s a', strtotime($employee->updated_at)) }}</dd>
        </dl>
        <hr>

        <div class="row botoneseditemployee">
              <div class="col-sm-6">
                  <a href="{{ route('admin.employees')}}" class="btn btn-danger btn-block btncanceleditemployee">Cancelar</a>
              </div>
              <div class="col-sm-6">
                  <button type="submit" class="btn btn-success btn-block btnupdateeditemployee">Guardar</button>
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                  {{ method_field('PUT') }}
              </div>
              
        </div> 
        </form>
      </div>
    </div>
</div>
</div>
@endsection