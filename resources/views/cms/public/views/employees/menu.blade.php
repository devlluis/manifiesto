@extends('cms.public.layouts.default')
@section('content')
<div id="listallemployees">

  <div class="col-md-10" id="employees">
    <h3 class="h3employee">TRABAJADORES</h3>
  </div>
 
  <div class="col-md-2" id="buttonaddemployee">
    <button type="button" id="buttoncreateemployee" class="btn btn-danger">Crear Trabajador</button>
  </div>

     <div class="col-md-12" id="partialemployee">
    @include('cms.public.views.partials._messages')	
    </div>

  <div class="col-md-12">
    <div id="tableemployees" class="tableemployeesclass">		
      <table class="table touchtable">
        <thead>
          <tr class="tremployee">
            <th class="columnidemployee thcenter thmenuemp">ID</th>
            <th class="thcenter thmenuemp columnvisibleemp" >Visible</th>
            <th class="thmenuemp thmenuempname">Nombre</th>
            <th class="thmenuemp">#Hashtag</th>
            <th class="thmenuemp thmenuslug">Slug</th>
            <th class="thmenuemp thmenuspuesto">Puesto</th>
            <th class="thmenuemp thmenusimagen">Imagen</th>
            <th class="thcenter thmenuemp thmenuorden">Orden</th>
            <th class="thaccionesemployee thmenuemp"><span class="glyphicon glyphicon-cog"></span>Acciones</th>
          </tr>
        </thead>
        <tbody id="tbodyemployee">
          @foreach ($employees as $key => $employee)
            <tr id="{{$employee->id}}">
              <td class="idemployees tdcenter"><p id="margindataemployee">{{$employee->id}}</p></td>
              <td class="visibleemployee tdcenter"> 
              <form action="{{route('admin.employees.cambiarVisible',$employee->id)}}" >
              <button type="button" id="buttonchangepublicemployee" data-id="{{ $employee->id }}" class="buttonchangepublicemployee">
                  @if ($employee->public == '1')
                            <i class="fa fa-check" aria-hidden="true" id="margindataemployee" class="cambiarsiporno"></i>
                   @else
                    <i class="fa fa-times" aria-hidden="true" id="margindataemployee"></i>
                   @endif
              </button>
              <input type="hidden" name="_token" value="{{Session::token()}}">
              </form>
              </td>
              <td class="nameemployee"><p id="margindataemployee">{{$employee->name}}</p></td>
              <td class="nameemployee2"><p id="margindataemployee">{{$employee->name}}</p><img src="{{ asset('/storage/employees/'.$employee->slug.'.jpg')}}" class="sizeimage"></td>
              <td class="hashtagemp"><p id="margindataemployee">#{{$employee->hashtag}}</p></td>
              <td class="hashtagemp2"><p id="margindataemployee" class="hashtagempp">#{{$employee->hashtag}}</p>  
              @if ($employee->public == '1')
                       <p class="pvisible">Visible:</p><i class="fa fa-check" aria-hidden="true" id="margindata"></i>
              @else
                       <p class="pvisible">Visible:</p><i class="fa fa-times" aria-hidden="true" id="margindata"></i>
              @endif
              </td>
              <td class="slugemployee"><p id="margindataemployee" >{{$employee->slug}}</p></td>
              <td class="slugemployee2"><p id="margindataemployee">{{$employee->slug}}</p>
               <form method="POST" action="{{route('admin.employees.destroy',$employee->id)}}" onsubmit="return ConfirmarBorrarTrabajadores()" class="formeditdeletemenuemployee4">
						    <input type="submit" value="Delete" class="btn btn-danger btn-danger-emp btn-sm" id="margindataemployee">
						    <input type="hidden" name="_token" value="{{Session::token()}}">
						    {{method_field('DELETE')}}
						  </form>
              </td>
              <td class="puestoemployee"><p id="margindataemployee">{{$employee->position}}</p></td>
              <td class="imagenemployee"><img src="{{ asset('/storage/employees/'.$employee->slug.'.jpg')}}" class="sizeimage"></td>
              <td class="ordenemployee tdcenter"><p id="margindataemployee">{{$employee->order}}</p>
              <div class="formeditdeletemenuemployee">
                <a href="{{ route('admin.employees.edit', $employee->id)}}" class="btn btn-success btn-success-emp btn-sm" id="margindataemployee">Editar</a>
                <button type="button" class="btn btn-danger btn-sm deletemenuemployee" data-toggle="modal" data-target="#formdeleteemployee_{{$employee->id}}" id="margindata">Borrar</button>
               </div>
              </td>
              <td class="formactionmenuemployee"> 
                <a href="{{ route('admin.employees.edit', $employee->id)}}" class="btn btn-success btn-success-emp btn-sm" id="margindataemployee">Editar</a>
                <button type="button" class="btn btn-danger btn-sm deletemenuemployee" data-toggle="modal" data-target="#formdeleteemployee_{{$employee->id}}" id="margindata">Borrar</button>
            </td>

              <div id="formdeleteemployee_{{$employee->id}}" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
                          <div class="modal-dialog" style="">
                          <div class="modal-content" style="">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color:black;">¿Estas seguro de borrar el trabajador?</h4>
                          </div>
                          <div class="modal-body">
                            <div class="col-sm-6">
                                      <a href="{{ route('admin.employees') }}" class="btn btn-danger btn-block colsm6btnno">No</a>
                            </div>
                            <div class="col-sm-6">
                              <form method="POST" action="{{route('admin.employees.destroy',$employee->id)}}">
                                  <input type="submit" value="Si" class="btn btn-danger btn-block colsm6btnsi inputsiemployee">
                                  <input type="hidden" name="_token" value="{{Session::token()}}">
                                  {{method_field('DELETE')}}
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>
                          </div>
                        </div>
                        </div>
                        </div>
         </tr>
         @endforeach
       </tbody>
     </table>
     <br><br>
   </div>
</div>
</div>

<div id="formcreartrabajador" class="col-md-12"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
    <div>
    <h3>Crear nuevo trabajador</h3>

      <div class="col-md-12 divtrabajadortamañoincorrecto" style="display:none;">
                    <p class="pDivError">El tamaño de la imagen del trabajador no es el correcto.</p>
      </div>

      <div class="col-md-12 divtrabajadormal" style="display:none;">
          <p class="pDivError">Para poder crear un trabajador tienes que añadir una imagen.</p>
      </div>
    </div>
    <div id="formcreateclient">
        <form  enctype="multipart/form-data" id="myFormEmployee" name="myFormEmployee">
         <input type="hidden" name="_token" value="{{ Session::token() }}">

          <div class="form-group">
            <label name="name">Nombre:</label>
            <input type="text" id="nameEmployee" name="nameEmployee" class="form-control form-control-sm editnameproject" required>
            <label name="hashtag">Hashtag:</label>
            <input type="text" id="hashtagemp" name="hashtagEmployee" class="form-control form-control-sm edithashtagproject" required>
            <label name="slug">Slug</label>
            <input type="text" id="slugEmployee" name="slugEmployee" class="form-control form-control-sm editslugproject" required>
            <label name="position">Position</label>
            <input type="text" id="positionEmployee" name="positionEmployee" class="form-control form-control-sm editpositionproject" required><br><br>
            <table class="tablecreateemployee">
                <tr>
                  <td>
                      <img src="" id="img" class="img" style="background-color:#ccc;border:2px solid gray;width:300px;height:277px;" onerror="this.src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='">
                      <input type="file" name="pathimage" id="pathimage"  class="form-control-file" aria-describedby="fileHelp">
                  </td>
                </tr> 
                <tr>
                  <td>
                      <input type="button" name="" value="Seleccionar imagen" id="browse_file3" class="btn btn-danger seleccionaremp form-control">
                      <p>300x277</p>
                  </td>
                </tr>
              </table>
            <br>
            <input type="submit" value="Crear Trabajador" id="createclientsubmit" class="btn btn-danger btn-md">
            <a href="{{ route('admin.employees') }}" class="btn btn-danger btn-block hrefcancel">Cancelar</a>
            <br><br><br>

          </div>
        </form>

      </div>
</div> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 END HERE-->

@stop