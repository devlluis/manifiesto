@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-10">
<h3 style="letter-spacing:40px;text-align:center;color:009bc9;">TRABAJADORES</h3>
</div>

<div class="col-md-12">
		<div class="col-md-4">
		<h3 style="color:fed17b;">About Employee</h3>
			
            <p class="lead">Id: {{ $employee->id}}</p>
            <p class="lead">Name: {{ $employee->name}}</p>
            <p class="lead">Hashtag: {{ $employee->hashtag}}</p>
            <p class="lead">Slug: {{ $employee->slug}}</p>
            <p class="lead">Position: {{ $employee->position}}</p>
            <p class="lead">Order: {{ $employee->order}}</p>
            <p class="lead">Public: {{ $employee->public}}</p>
				

      	</div>
		<div class="col-md-4">
			<p class="lead"><img src="{{ asset('/storage/employees/'.$employee->slug.'.jpg') }}" id="img" class="img" style="background-color:#ccc;border:2px solid gray;"></p>
			<br><br>
		
		</div>
		<div class="col-md-4">

			<div class="well" style="background-color:black">

				<dl class="dl-horizontal">
					<dt>Create At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($employee->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($employee->updated_at)) }}</dd>
				</dl>

				<hr>

				<div class="row">
					<div class="col-sm-6">
    					<a href="{{ route('admin.employees.edit', $employee->id)}}" class="btn btn-primary btn-block">Edit</a>
					</div>
					<div class="col-sm-6">
						<form method="POST" action="{{route('admin.employees.destroy',$employee->id)}}" onsubmit="return ConfirmarBorrar()">
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
						<input type="hidden" name="_token" value="{{Session::token()}}">
						{{method_field('DELETE')}}
						</form>
					</div>
				</div>

			</div>

		</div>

</div>

	<script>
		function ConfirmarBorrar(){

			var x = confirm("¿Estas seguro de borrar este cliente?");
			if(x)
				return true;
			else 
				return false;
		}
	</script>

@endsection