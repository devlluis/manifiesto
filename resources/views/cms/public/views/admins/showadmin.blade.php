@extends('cms.public.layouts.default')
@section('content')

<div class="col-md-10">
<h3 style="letter-spacing:40px;text-align:center;color:50b948;">ADMINS</h3>
</div>

<div class="col-md-12">
    @include('cms.public.views.partials._messages')			
		<div class="col-md-6">
		<h3 style="color:fed17b;">About Admin</h3>
			
            <p class="lead">Id: {{ $admin->id}}</p>
            <p class="lead">Name: {{ $admin->name}}</p>
            <p class="lead">Email: {{ $admin->email}}</p>
            <p class="lead">Password: No es visible</p>
		<br><br>
      	</div>
		<div class="col-md-6">

			<div class="well" style="background-color:black">

				<dl class="dl-horizontal">
					<dt>Create At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($admin->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($admin->updated_at)) }}</dd>
				</dl>

				<hr>

				<div class="row">
					<div class="col-sm-6">
    				    <a href="{{ route('admin.admins') }}" class="btn btn-danger btn-block">Cancel</a>
					</div>
					<div class="col-sm-6">
						<form method="POST" action="{{route('admin.admins.destroy',$admin->id)}}" onsubmit="return ConfirmarBorrar()">
						<input type="submit" value="Delete" class="btn btn-danger btn-block">
						<input type="hidden" name="_token" value="{{Session::token()}}">
						{{method_field('DELETE')}}
						</form>
					</div>
				</div>

			</div>

		</div>

</div>

	<script>
		function ConfirmarBorrar(){

			var x = confirm("¿Estas seguro de borrar este administrador?");
			if(x)
				return true;
			else 
				return false;
		}
	</script>

@endsection