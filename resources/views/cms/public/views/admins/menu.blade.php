@extends('cms.public.layouts.default')
@section('content')

<div id="listalladmins">

    <div class="col-md-10" id="admins">
    <h3 class="h3administradores">ADMINISTRADORES</h3>
    </div>

    <div class="col-md-2" id="buttonaddadmin">
        <button type="button" id="buttoncreateadmin" class="btn btn-danger">Crear Admin</button>
    </div>

    <div class="col-md-12" id="partialadmin">
        @include('cms.public.views.partials._messages')
    </div>

    <div class="col-md-12">
        <div id="tableadmin">
        <table class="table tablemenuadmin">
    <thead>
      <tr>
        <th class="columnidadmins thcenter thmenuadmin"><!--<a href="{{route('admin.admins.order', ['field' => 'id','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByAsc"></span></a>-->ID<!--<a href="{{route('admin.admins.order',['field' => 'id','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByIdDesc"></span></a>--></th>
        <th class="thmenuadmin"><!--<a href="{{route('admin.admins.order', ['field' => 'name','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByAsc"></span></a>-->Nombre<!--<a href="{{route('admin.admins.order',['field' => 'name','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByNameDesc"></span></a>--></th>
        <th class="thmenuadmin thmenuemailadmin"><!--<a href="{{route('admin.admins.order', ['field' => 'email','order' => 'asc'])}}"><span class="glyphicon glyphicon-arrow-up" id="orderByAsc"></span></a>-->Email<!--<a href="{{route('admin.admins.order',['field' => 'email','order' => 'desc'])}}"><span class="glyphicon glyphicon-arrow-down" id="orderByEmailDesc"></span></a>--></th>
        <th class="thmenuadmin"><span class="glyphicon glyphicon-cog"></span>Acciones</th>
      </tr>
    </thead>
    <tbody>
     @foreach ($admins as $key => $admin)
      <tr>
        <td class="idadmin tdcenter"><p id="margindata" class="tdmenuadmin">{{$admin->id}}</p></td>
        <td class="nameadmin"><p id="margindata" class="tdmenuadmin">{{$admin->name}}</p></td>
        <td class="emailadmin"><p id="margindata" class="tdmenuadmin">{{$admin->email}}</p></td>
        <td class="actionsadmin tdmenuadmin">

         <button type="button" class="btn btn-danger btn-sm deletemenuadmin" data-toggle="modal" data-target="#formdeleteadmin_{{$admin->id}}" id="margindata">Borrar</button>
        
         <div id="formdeleteadmin_{{$admin->id}}" class="modal fade" role="dialog"> <!-- DIV TO SHOW THE CREATE PROJECT FORM 1 START HERE-->
      
         <div class="modal-dialog" style="">

         <div class="modal-content" style="">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <h4 class="modal-title" style="color:black;">¿Estas seguro de borrar el administrador?</h4>

            </div>

            <div class="modal-body">

                <div class="col-sm-6">
                            <a href="{{ route('admin.admins') }}" class="btn btn-danger btn-block colsm6btnno">No</a>
                </div>
                <div class="col-sm-6">
                    <form method="POST" action="{{route('admin.admins.destroy',$admin->id)}}">
                        <input type="submit" value="Si" class="btn btn-danger btn-block colsm6btnsi">
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                        {{method_field('DELETE')}}
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                    
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="closemodal">Cerrar</button>

            </div>

         </div>

       </div>

     </div>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
  <br><br>
</div>
</div>

<div id="formcrearadmin" class="col-md-12">
        <div>
        <h3 class="crearnuevoadmin">Crear nuevo admin</h3>
        </div>
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar</div>
                <div class="panel-body">
                 <form class="form-horizontal" enctype="multipart/form-data" id="myFormAdmin">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6 divinputadminregister">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Direccion E-Mail</label>

                            <div class="col-md-6 divinputadminregister">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6 divinputadminregister">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 divinputadminregister">
                                <button type="submit" class="btn btn-primary btncrearadmin">
                                    Registrar
                                </button>
                                                                            <a href="{{ route('admin.admins') }}" class="btn btn-danger btn-block hrefcancel hrefcanceladmin">Cancelar</a>

                            </div>
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@stop