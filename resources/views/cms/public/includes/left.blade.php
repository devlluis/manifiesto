
                        <!-- #menu -->
                        <ul id="menu">
                                  <li class="">
                                    <a href="/admin/projects">
                                     <span class="link-title">&nbsp;&nbsp;&nbsp;<i class="fa fa-folder-open" aria-hidden="true"></i>

Proyectos</span>
                                    </a>
                                  </li>
                                  <li class="">
                                    <a href="/admin/clients">
                                        <span class="link-title">&nbsp;&nbsp;&nbsp;<i class="fa fa-money" aria-hidden="true"></i>

 Clientes</span>
                                    </a>
                                  </li>
                                  <li class="">
                                    <a href="/admin/employees">
                                    &nbsp;&nbsp;&nbsp;<i class="fa fa-user-circle" aria-hidden="true"></i>
 Trabajadores
                                    </a>
                                  </li>
                                  <li>
                                    <a href="/admin/admins">
                                      <span class="link-title">
                                      &nbsp;&nbsp;&nbsp;<i class="fa fa-user-plus" aria-hidden="true"></i>
 Administradores
                                    </span>
                                    </a>
                                  </li>
                                  <li class="fapoweroff">
                                    <a href="{{url('admin/logout')}}">
                                      <span class="link-title">
                            &nbsp;&nbsp;&nbsp;<i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;Cerrar sesión
                            </span>
                                    </a>
                                  </li>
                                  
                                </ul>
                        <!-- /#menu -->