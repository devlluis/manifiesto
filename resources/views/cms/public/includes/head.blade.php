<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=0, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Manifiesto - Admin</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/cms/lib/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/cms/lib/font-awesome/css/font-awesome.min.css">
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="/cms/css/main.css">
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="/cms/lib/metismenu/metisMenu.css">
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="/cms/lib/onoffcanvas/onoffcanvas.css">
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="/cms/lib/animate.css/animate.css">

    <link rel="stylesheet" href="/cms/css/style-switcher.css">

    <link rel="stylesheet" type="text/css" href="/assets/css/cms.css">
    <link rel="stylesheet/less" type="text/css" href="/cms/less/theme.less">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    @include('cms.public.includes.scripts')

  </head>