<div id="modalwarning" class="modal fade" role="dialog">
    <div class="modal-dialog" style="">
        <div class="modal-content" style="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:black;">Llevas 10 minutos inactivo, la sesión sera cerrada en breves.</h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <h3 id="timer"></h3>
                    <div class="col-sm-6"><h3 id="modalwarningh3"></h3></div>
                    <div id="loadinggif" class="col-sm-6"><img src="/cms/img/loading.gif" class="imgloadinggif"></div>
                </div>
            </div>
        </div>
    </div>
</div>

