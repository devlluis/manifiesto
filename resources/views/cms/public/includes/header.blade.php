  <!-- .navbar -->
             
                    <!-- /.navbar -->
                        <header class="head">
                                <div class="search-bar col-md-3">
                                  <h4 class="h4manifiesto">MANIFIESTO</h3>

                                    <!-- /.main-search -->                                
                                </div>
                            @if (\Request::is('admin/project*'))
                            <div class="col-md-6 previewproject">
                                    <button type="button" class="btn btn-default btn-sm on buttonpreview" target="_blank" rel="noopener" id="urlproyectos">
                                      <span class="glyphicon glyphicon-eye-open"></span>Previsualizar
                                     </button>
                            </div>
                            @elseif (\Request::is('admin/admin*'))
                            <div class="col-md-6 previewproject">
                                    <button type="button" class="btn btn-default btn-sm on buttonpreview" target="_blank" rel="noopener" id="urlproyectos">
                                      <span class="glyphicon glyphicon-eye-open"></span>Previsualizar
                                     </button>
                            </div>
                            @elseif (\Request::is('admin/client*'))
                            <div class="col-md-6 previewproject">
                                    <button type="button" class="btn btn-default btn-sm on buttonpreview" target="_blank" rel="noopener" id="urlproyectos">
                                      <span class="glyphicon glyphicon-eye-open"></span>Previsualizar
                                     </button>
                            </div>
                            @elseif (\Request::is('admin/employee*'))
                            <div class="col-md-6 previewemployee">
                                      <button type="button" class="btn btn-default btn-sm on buttonpreviewemployee" target="_blank" rel="noopener" id="urlnosotros">
                                      <span class="glyphicon glyphicon-eye-open"></span>Previsualizar
                                      </button>
                            </div>
                            @endif
                            <div class="user col-md-3">
                            <span class="glyphicon glyphicon-user"></span> <p class="name">{{Auth::user()->name}}<p>
                            </div>
                            <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i>
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-left">
                              <li class="limenu"><a href="/admin/projects"><i class="fa fa-folder-open" aria-hidden="true"></i>Proyectos</a></li>
                              <li class="limenu"><a href="/admin/clients"><i class="fa fa-money" aria-hidden="true"></i>Clientes</a></li>
                              <li class="limenu"><a href="/admin/employees"><i class="fa fa-user-circle" aria-hidden="true"></i>Trabajadores</a></li>
                              <li class="limenu"><a href="/admin/admins"><i class="fa fa-user-plus" aria-hidden="true"></i>Administradores</a></li>
                              <li class="divider limenu"></li>
                              <li class="fapoweroff limenu"><a href="{{url('admin/logout')}}"><i class="fa fa-power-off" aria-hidden="true"></i>Cerrar sesión</a></li>
                            </ul>
                          </div>
                        </header>
                        
                        <!-- /.head -->
