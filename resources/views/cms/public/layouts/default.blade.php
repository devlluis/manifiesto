<html>
<head><meta name="csrf-token" content="{{ csrf_token() }}">
</head>
    @include('cms.public.includes.head')
        <body>
            
            @include('cms.public.includes.modalwarning')

            <div class="bg-dark dk" id="wrap">

                <div id="top">
                    @include('cms.public.includes.header') <!-- TOP DIV -->
                </div>
                
                <div id="left" class="col-md-3">
                            @include('cms.public.includes.left') <!-- LEFT DIFT -->
                </div>

                <div id="content">

                    @yield('content')
           
                </div>

                <div id="right" class="onoffcanvas is-right is-fixed bg-light" aria-expanded=false>
                       
                        @include('cms.public.includes.header')                   
                   
                </div>

                
            </div>

            <footer class="footer">
                @include('cms.public.includes.footer') <!-- FOOTER -->
            </footer>
    
        </body>

</html>
