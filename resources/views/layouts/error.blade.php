<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Manifiesto | @lang('header.title.common',[], $locale)</title>
    <meta name="description" content="@lang('header.meta.description',[], $locale)">
    <meta name="keywords" content="@lang('header.meta.keywords',[], $locale)">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:title" content="Manifiesto, @lang('header.title.common',[], $locale)" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{Request::url()}}" />

    <meta property="og:description" content="@lang('header.meta.description',[], $locale)" />
    <meta property="og:image" content="{{Config::get('app.url')}}/assets/img/facebook.jpg" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" >

    <link rel="stylesheet" href="/assets/css/app.css"/>


    @yield('styles')

    {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
	{{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
	<!--[if lt IE 9]>
    <script src="/assets/js/vendor/html5shiv.js"></script>
    <script src="/assets/js/vendor/respond.js"></script>
    <![endif]-->


</head>
<body>
    
    <div id="app" class="{{$view_name}} level">

        <preloader></preloader>

        <header>
            @include('includes.header')
        </header>
        
        @yield('content')
        <div class="overlay"></div>

        @include('includes.footer')
        
        <div id="fb-root"></div>
    </div>

    @include('includes.config')
    @yield('js')
 {{--     @include('js.analytics')  --}}

</body>
</html>
