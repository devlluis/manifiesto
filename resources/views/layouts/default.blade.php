<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    @include('includes.metas')

    <link href="/favicon.ico" rel="icon" type="image/x-icon" />
    <link href="/favicon.ico" rel="shortcut icon">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=5Ae2wbPRMM">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=5Ae2wbPRMM">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=5Ae2wbPRMM">
    <link rel="manifest" href="/assets/favicon/manifest.json?v=5Ae2wbPRMM">
    <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=5Ae2wbPRMM" color="#5bbad5">
    <link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=5Ae2wbPRMM">
    <meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=5Ae2wbPRMM">
    <meta name="theme-color" content="#2f2222">

    <link href="/assets/css/app.css" rel="stylesheet"/>

    @yield('styles')

    {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
	{{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
	<!--[if lt IE 9]>
    <script src="/assets/js/vendor/html5shiv.js"></script>
    <script src="/assets/js/vendor/respond.js"></script>
    <![endif]-->


</head>
<body>
    <div id="app" :class="[$route.meta.section]" v-cloak>

        <preloader></preloader>

        <header>
            @include('includes.header')
        </header>
    
        @yield('content')
        

        @include('includes.footer')

        {{--  <div id="fb-root"></div>  --}}
    </div>
    @include('includes.config')
    @yield('js')
   {{--   @include('js.analytics')  --}}

</body>
</html>
