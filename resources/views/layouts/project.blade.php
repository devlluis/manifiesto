<div id="projectContent">
    
    <div class="projectHeaderImg {{$projectSlug}}">
        <span>{!! $caption !!}</span>
        <div class="overlay"></div>
        <div class="bg" v-lazy:background-image="'{{$image_path}}header.jpg'"></div>
    </div>
    
    
    <article class="projectInfo" id="{{ $projectSlug }}">
        <header>
            <h1 class="font-black size-title">{!! $title !!}</h1>
    
            <p class="font-bold size-subtitle">{{$clientsString}}</p>
            @include('includes.projects.sharer')
    
        </header>
    
        <section class="row">
            <div class="col-md-4">
                @yield('col1')
            </div>
            <div class="col-md-4">
                @yield('col2')
            </div>
            <div class="col-md-4">
                @yield('col3')
            </div>
        </section>
    
        <section class="row">
            <div class="col-md-12">
                @yield('mainVideo')
            </div>
        </section>
    
        @yield('case')
    
    </article>

</div>    