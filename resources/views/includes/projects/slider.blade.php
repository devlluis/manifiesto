<div class="flexslider fadeInUp preanimate">
	<ul class="slides">
		@for ($i = 1; $i <= $numPics; $i++)
		<li>
            {{--  @include('includes.picture' ,['image' => 'slide-'.$i.'.jpg', 'alt' => $title . ', ' .$clientsString])  --}}
		<img v-lazy="'{{$image_path}}slide-{{$i}}.jpg'" alt="{{$title}}, {{$clientsString}}">
		</li>
		@endfor
	</ul>
</div>
