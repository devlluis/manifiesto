<social-sharing url="http://manifiesto.biz/{{$locale}}/works/{{$projectSlug}}"
				title="@lang('general.sharer'): {{$clientsString}}, {{$title}}"
				description="@lang('general.sharer'): {{$clientsString}}, {{$og_description}}"
				quote="@lang('general.sharer'): {{$clientsString}}, {{$og_description}}"
				inline-template>
	<ul class="projects sharer fadeIn animated">
		<li>
			@lang('general.share',[], $locale)
		</li>
		<li>
			<network network="facebook" class="facebook"><i class="fa fa-facebook"></i></network>
		</li>
		<li>
			<network network="linkedin" class="linkedin"><i class="fa fa-linkedin"></i></network>			
		</li>
		<li>
			<network network="twitter" class="twitter"><i class="fa fa-twitter"></i></network>						
		</li>
		<li>
			<network network="whatsapp" class="whatsapp"><i class="fa fa-whatsapp"></i></network>								
		</li>
	</ul>
</social-sharing>
<hr>
