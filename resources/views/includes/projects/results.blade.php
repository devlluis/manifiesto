<div class="media results">
	<div class="media-left fadeInUp preanimate">
	<img src="/assets/img/projects/icons/{{$icon}}.png" alt="{{$icon}}">
	</div>
	<div class="media-body fadeInRight preanimate">
		<h3>
			<div class="media-heading font-black">@if (isset($title_es) && $locale=='es') {{$title_es}} @elseif (isset($title_en) && $locale=='en') {{$title_en}} @else {{$title}}@endif</div>
			@if ($locale=='es') {{$es}} @elseif ($locale=='en') {{$en}} @endif
		</h3>
	</div>

</div>