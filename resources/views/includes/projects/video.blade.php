<div class="video smallVideo" >
    <iframe src="//player.vimeo.com/video/{{$id}}?api=1&player_id=main" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <img src="/assets/img/video-placeholder.png">
</div>
