<div class="col-md-2 audio-content">
    <img src="/assets/img/projects/icons/audio.png"/>
        <audio controls>
            <source src="/assets/audio/projects/{{$projectSlug}}/{{$audio}}.{{$type}}" type="audio/{{$type}}">
        </audio>
</div>