<ul class="socialLinks">
	<li>
		<a href="https://www.facebook.com/manifiesto.biz" target="_blank" rel="noopener" class="facebook"><i class="fa fa-facebook"></i></a>
	</li>
	<li>
		<a href="http://vimeo.com/reelmanifiesto" target="_blank" rel="noopener" class="vimeo"><i class="fa fa-vimeo-square"></i></a>
	</li>
	<li>
		<a href="http://linkedin.com/company/manifiesto" target="_blank" rel="noopener" class="linkedin"><i class="fa fa-linkedin"></i></a>
	</li> 
</ul>