<footer id="footer">
	<div id="footerInner" class="columns level">
		<div class="footer-1 column is-4 is-paddingless">
			Manifiesto.biz &copy; {{date("Y")}} - @lang('footer.all-rights-reserved',[], $locale) | 
			<strong><router-link to="/{{$locale}}/legal" class="">@lang('footer.legal-link',[], $locale)</router-link></strong>
		</div>
		<div class="footer-2 column is-4 is-paddingless">
			<img id="logo" v-lazy="'/assets/img/logos/logo-m.png'">
			<div class="actitud">
				#<vue-typer :text='hashtags'
                    :repeat='Infinity'
                    :shuffle='false'
                    initial-action='erasing'
                    :pre-type-delay='70'
                    :type-delay='50'
                    :pre-erase-delay='2000'
                    :erase-delay='10'
                    erase-style='backspace'
                    :erase-on-complete='false'
                    caret-animation='phase'></vue-typer>
				</div>
		</div>
		<div class="footer-3 column is-4 is-paddingless">
			@include('includes.social')
		</div>
	</div>
</footer>
