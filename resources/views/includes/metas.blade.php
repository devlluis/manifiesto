<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Manifiesto · @lang('header.title.common',[], $locale) · @yield('title')</title>

{{--  @yield('metas')  --}}
<meta name="description" content="@lang('metas.home.description',[], $locale)" id="desc"/>
<meta name="keywords" content="@lang('metas.home.keywords',[], $locale)" id="keywords"/>

{{-- Chrome, Firefox OS and Opera --}}
<meta name="theme-color" content="#000000">
<meta name="mobile-web-app-capable" content="yes">
{{-- Windows Phone --}}
<meta name="msapplication-navbutton-color" content="#000000">
{{-- iOS Safari --}}
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="Manifiesto">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

{{-- Control the behavior of search engine crawling and indexing --}}
<meta name="robots" content="index,follow">{{-- All Search Engines --}}
<meta name="googlebot" content="index,follow">{{-- Google Specific --}}

{{--  facebook  --}}
<meta property="og:type" content="website" />
<meta property="og:site_name" content="Manifiesto" />
<meta property="og:locale" content="{{$locale}}" />


{{--  Twitter  --}}
<meta name="twitter:card" content="summary_large_image">