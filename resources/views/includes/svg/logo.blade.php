<svg version="1.1" id="logo-svg" class="logo-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 202.4 24.6" style="enable-background:new 0 0 202.4 24.6;" xml:space="preserve">
    <a class="hide-on-fallback">
        <g id="text-svg" class="st0">
            <polygon id="M" class="letter" points="6.7,18.8 7.9,10.6 13.3,23.9 15.6,23.9 21.2,10.6 22.9,23.9 28.9,23.9 25.5,0.8 19.5,0.8 14.5,13.1 9.8,0.8 3.9,0.8 0.9,18.8 0.9,18.8 0.9,18.8 	"/>
            <path id="A" class="letter" d="M48.6,23.9l-1.5-4h-8.3l-1.6,4h-6.3l8.9-23.1h6.5L55,23.9H48.6z M42.9,8l-2.5,7.3h5L42.9,8z"/>
            <path id="N" class="letter" d="M74.6,23.9l-11-14.1v14.1h-6V0.8h6l11,14.1V5.7h6v18.2H74.6z"/>
            <path id="I" class="letter" d="M113.8,18.8v-18h6v18H113.8z"/>
            <path id="F" class="letter" d="M102.3,5.9v3.8h7.2v5.1h-7.2v9.1h-6V0.8h13.2v5.1H102.3z"/>
            <path id="I2" class="letter" d="M85.5,23.9V0.8h6v23.1H85.5z"/>
            <path id="E" class="letter" d="M124,23.9V0.8h13.1v5.1h-7.1v3.8h7.1v5.1h-7.1v4h7.1v5.1H124z"/>
            <path id="S" class="letter" d="M156,20.4c-1.5,2.9-5.1,4.1-8.1,4.1c-2.9,0-5.7-1-8-2.6l2.6-4.8c1.5,1.3,3.3,2.4,5.3,2.4 c1.5,0,2.9-0.6,2.9-2.3c0-4-9.8-1-9.8-9.1c0-4.7,3.2-7.8,7.8-7.8c2.6,0,5.1,0.7,7.3,1.9l-2.4,4.7c-1.1-0.9-2.4-1.6-3.9-1.6 c-1.2,0-2.6,0.7-2.6,2c0,3.4,9.8,1.2,9.8,9.1C156.9,17.6,156.6,19.3,156,20.4z"/>
            <path id="T" class="letter" d="M169.9,5.9v18h-6v-18h-4.9V0.8h10.9V5.9L169.9,5.9z"/>
            <path id="O" class="letter" d="M189.6,24.6c-7.1,0-12.8-5.1-12.8-12.3c0-7.2,5.7-12.3,12.8-12.3c7.1,0,12.8,5.1,12.8,12.3 C202.4,19.6,196.7,24.6,189.6,24.6z M189.6,5.8c-3.6,0-6.5,2.9-6.5,6.5c0,3.6,2.9,6.5,6.5,6.5c3.6,0,6.5-2.9,6.5-6.5 C196.1,8.7,193.2,5.8,189.6,5.8z"/>
        </g>
        <polygon id="red" class="st1" points="0.9,18.8 0,23.9 6,23.9 6.7,18.8 6.7,18.8 "/>
        <path id="yellow" class="st2" d="M74.6,0.8h6v4.9h-6V0.8z"/>
        <path id="blue" class="st3" d="M113.8,23.9v-5.1h6v5.1H113.8z"/>
        <path id="green" class="st4" d="M169.9,5.9V0.8h5v5.1H169.9z"/>
    </a>
    <image src="/assets/img/logos/logo.png" xlink:href=""/>
</svg>
