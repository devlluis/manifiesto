<script>
    function Config () {
    }
    Config.locale = '{{App::getLocale()}}';
    Config.url = '{{ config('app.url') }}';
    Config.csrf = '{{ csrf_token() }}';

</script>