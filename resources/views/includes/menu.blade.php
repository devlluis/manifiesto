<ul class="menu">
	<li><a href="/{{Lang::get('menu.otherlocale',[], $loc)}}/" class="red" @click="changeLanguage()">@lang('menu.language',[], $loc)</a></li>
	<li><router-link exact to="/{{$loc}}/" class="" >@lang('menu.works',[], $loc)</router-link></li>
	<li><router-link to="/{{$loc}}/about" class="">@lang('menu.about',[], $loc)</router-link></li>
	<li><router-link to="/{{$loc}}/services" class="">@lang('menu.services',[], $loc)</router-link></li>
    <li><router-link to="/{{$loc}}/contact" class="">@lang('menu.contact',[], $loc)</router-link></li>
    <li><a href="http://blog.manifiesto.biz/" target="_blank" rel="noopener">Blog</a></li>
</ul>
