<div id="menuMobile" :class="['nexablack', 'size-menu',{'open':menuOpen}]">
    <nav id="menuMobileContainer" @click.prevent="menuOpen = false">
        @include('includes.menu',['loc'=>LaravelLocalization::getCurrentLocale()])
    </nav>
</div>
<div id="header" class="font-black">
    @include('includes.colors')
    <div id="headerInner" class="level is-mobile">
        <div id="logo" class="itemMenu level-left">
            <router-link :to="'/'+locale" class="level-item">
                <logo/>
            </router-link>
        </div>

        <div class="typer level-item">
            #<vue-typer :text='hashtags'
                        :repeat='Infinity'
                        :shuffle='false'
                        initial-action='erasing'
                        :pre-type-delay='70'
                        :type-delay='50'
                        :pre-erase-delay='2000'
                        :erase-delay='10'
                        erase-style='backspace'
                        :erase-on-complete='false'
                        caret-animation='phase'>
            </vue-typer>
		</div>

        @include('includes.social')
        
        <div id="menuButtonMobile" class="level-right">
            <div class="clicker" @click="toggleMenu" @mouseenter="menubHover = true" @mouseleave="menubHover = false"></div>
            <div :class="['burger',{'cross':menuOpen},{'hover':menubHover && !menuOpen}] "></div>
        </div>
    </div>


</div>
<div id="pre-preloader" class="preload">
</div>


