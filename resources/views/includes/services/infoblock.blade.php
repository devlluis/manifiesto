<div class="infoblock">
    <h3>@lang('services.p'.$id.'-title',[], LaravelLocalization::getCurrentLocale())</h3>
    @lang('services.p'.$id.'-description',[], LaravelLocalization::getCurrentLocale())
</div>