{{--different image size loads--}}
{{--   <picture>
    <source srcset="/i/mobile/{{$projectSlug}}/{{$image}}" media=" (max-width: 480px)">
    <source srcset="/i/tablet/{{$projectSlug}}/{{$image}}" media="(max-width: 990px)">
    <source srcset="{{$image_path}}{{$image}}">
    <img src="{{$image_path}}{{$image}}" class="@if(isset($class)){{$class}}@endif" id="@if(isset($id)){{$id}}@endif" alt="@if(isset($alt)){{$alt}}@endif">
</picture>   --}} 
<img v-lazy="'{{$image_path}}{{$image}}'"
        :data-srcset="'/i/mobile/{{$projectSlug}}/{{$image}} 320w, /i/tablet/{{$projectSlug}}/{{$image}} 640w, {{$image_path}}{{$image}} 1024w'"
            sizes="(min-width: 768px) 50vw, 100vw"
            class="lazy @if(isset($class)){{$class}}@endif"
            id="@if(isset($id)){{$id}}@endif"
            alt="@if(isset($alt)){{$alt}}@endif"/>