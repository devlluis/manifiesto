<div>

<div class="row" id="legalContainer">
    <div class="col-md-offset-2 col-md-8 col-xs-offset-1 col-xs-10">

        <h1 class="size-title font-black">@lang('footer.legal-link')</h1>

        @include('includes.projects.project-dash')
        <p>
            @if ($locale=='es')
            Para dar cumplimiento a lo establecido en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico, a continuaci&oacute;n se indican los datos de informaci&oacute;n general del sitio web www.manifiesto.biz:
            @endif

            @if ($locale=='en')
            Pursuant to the provisions of Law 34/2002 of 11 July on Services of the Information Society and Electronic Commerce, the general details of the website www.manifiesto.biz are set out below:
            @endif

        </p>

        <p>
            <strong class="">Comunicaci&oacute;n Creativa Manifiesto, S.L. </strong> <br>
            <span class="red">C/ Trafalgar 38-46, Primera Planta </span><br>
            <span class="red">08010 Barcelona </span><br>
            <span class="red">C.I.F.: B-63367478 </span><br>
            <span class="red">Direcci&oacute;n de correo electr&oacute;nico: info@manifiesto.biz </span><br>
            <span class="red">Tel&eacute;fono: +34 933091299 </span><br>
        </p>

        <p>
         @if ($locale=='es')
         Inscrita en el Registro Mercantil de Barcelona, Tomo 36.145, Folio 102, Hoja n&ordm; B-278727
         @endif

         @if ($locale=='en')
         Listed in the Companies Registry of Barcelona in Volume 36.145, Sheet 102, Page nº B-278727
         @endif
     </p>

     <h2 class="size-subtitle font-black">COPYRIGHT</h2>
     @if ($locale=='es')
     <p>Queda prohibido cualquier acto de explotaci&oacute;n o utilizaci&oacute;n de los contenidos de esta p&aacute;gina web sin la autorizaci&oacute;n previa y por escrito de sus leg&iacute;timos titulares. En concreto, se proh&iacute;be todo acto de reproducci&oacute;n, distribuci&oacute;n, comunicaci&oacute;n p&uacute;blica o transformaci&oacute;n, total o parcial, de cualquier elemento de la p&aacute;gina web, muy especialmente las fotograf&iacute;as, los gr&aacute;ficos, los spots, los fotogramas, las animaciones, los textos y su c&oacute;digo fuente. Los titulares de dichos derechos se reservan las acciones legalmente oportunas para resarcirse de los da&ntilde;os y perjuicios causados por cualquier acto que vulnere los derechos de propiedad intelectual sobre dichos contenidos. </p>
     <p>Asimismo, queda totalmente prohibido establecer links o enlaces de hipertexto directamente con cualquier contenido del sitio web sin autorizaci&oacute;n por escrito de <span class="red">MANIFIESTO</span>. </p>
     @endif

     @if ($locale=='en')
     <p>Any reproduction or use of the contents of this website is expressly prohibited without the prior authorisation in writing by their legitimate owners. In particular, any reproduction, distribution, public communication or transformation either wholly or in part of any element on this website is prohibited, in particular the photographs, graphics, advertisements, frames, animations, texts and source code. The owners of these rights reserve the right to take any type of legal action for damages arising as a result of any action that infringes the intellectual property rights in relation to said contents.</p>
     <p>It is also expressly forbidden to place any hypertext links to any contents on the website without the written authorisation of <span class="red">MANIFIESTO</span>.</p>
     @endif

     @if ($locale=='es')
     <h2 class="size-subtitle font-black">ACEPTACI&Oacute;N POR EL USUARIO</h2>

     <p>El presente documento (en adelante Aviso Legal) tiene por objeto regular el uso esta p&aacute;gina Web (en adelante Web) que <span class="red">Comunicaci&oacute;n Creativa Manifiesto, S.L.</span> (en adelante <span class="red">MANIFIESTO</span>) pone a disposici&oacute;n del p&uacute;blico en la URL <a href="http://www.manifiesto.biz">http://www.manifiesto.biz</a></p>

     <p>La utilizaci&oacute;n del Web por un tercero le atribuye la condici&oacute;n de Usuario y, supone la aceptaci&oacute;n plena del Usuario de todas y cada una de las Condiciones que se incorporan en el presente Aviso Legal.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">USER ACCEPTANCE</h2>

     <p>This document (hereinafter the “Legal Notice”) is intended to regulate the use of this website (hereinafter the “Website”) that <span class="red">Comunicaci&oacute;n Creativa Manifiesto, S.L.</span> (hereinafter <span class="red">MANIFIESTO</span>) has made available to the public at <a href="http://www.manifiesto.biz">http://www.manifiesto.biz</a></p>
     <p>The use of the website by third parties confers User status and implies the full acceptance by the User of all of the Terms and Conditions included in this Legal Notice.</p>

     @endif

     @if ($locale=='es')
     <h2 class="size-subtitle font-black">USO CORRECTO DEL SITIO WEB</h2>
     <p>El Usuario se compromete a utilizar el Web, los contenidos y servicios de conformidad con la Ley, el presente Aviso Legal, las buenas costumbres y el orden p&uacute;blico. Del mismo modo el Usuario se obliga a no utilizar el Web o los servicios que se presten a trav&eacute;s de &eacute;l con fines o efectos il&iacute;citos o contrarios al contenido del presente Aviso Legal, lesivos de los intereses o derechos de terceros, o que de cualquier forma pueda da&ntilde;ar, inutilizar o deteriorar el Web o sus servicios o impedir un normal disfrute del Web por otros Usuarios.</p>

     <p>Asimismo, el Usuario se compromete expresamente a no destruir, alterar, inutilizar o, de cualquier otra forma, da&ntilde;ar los datos, programas o documentos electr&oacute;nicos y dem&aacute;s que se encuentren en la Web. De igual forma, el Usuario se compromete a no obstaculizar el acceso de otros usuarios al servicio de acceso mediante el consumo masivo de los recursos inform&aacute;ticos a trav&eacute;s de los cuales <span class="red">MANIFIESTO</span> presta el servicio, as&iacute; como realizar acciones que da&ntilde;en, interrumpan o generen errores en dichos sistemas.</p>

     <p>El Usuario se compromete a no introducir programas, virus, macros, applets, controles ActiveX o cualquier otro dispositivo l&oacute;gico o secuencia de caracteres que causen o sean susceptibles de causar cualquier tipo de alteraci&oacute;n en los sistemas inform&aacute;ticos de <span class="red">MANIFIESTO</span> o de terceros.</p>

     <p>El Usuario se compromete a no obtener informaciones, mensajes, gr&aacute;ficos, dibujos, archivos de sonido y/o imagen, fotograf&iacute;as, grabaciones, software y en general, cualquier clase de material accesible a trav&eacute;s del Web o de los servicios ofrecidos en el mismo.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">CORRECT USE OF THE WEBSITE</h2>

     <p>The User agrees to use the Website, its contents and services in accordance with the law, this Legal Notice, good practice and public order. Similarly, Users are obliged to refrain from using the Website or the services it provides for illegal purposes or any other purpose contrary to the contents of this Legal Notice, which may harm the interests or rights of any third parties, or which in any way may damage, disable or harm the Website or its services, or hinder the normal enjoyment of the Website by other Users.</p>
     <p>The User also expressly agrees not to destroy, alter, disable or in any other way damage the electronic or other types of data, programmes or electronic documents contained on the Website.</p>
     <p>Similarly, the User agrees not to hinder in any way the access by other users to the service by means of making massive use of the computer resources through which <span class="red">MANIFIESTO</span> provides its services, or to carry out any other type of actions that damage, interrupt or cause errors to these systems.</p>
     <p>The User agrees not to upload any programmes, viruses, macros, applets, ActiveX controls or any other type of logical device or sequence of characters that may cause or be likely to cause any type of alteration to the computer systems of <span class="red">MANIFIESTO</span> or third parties.</p>
     <p>The User agrees not to obtain information, messages, graphics, illustrations, sound and/or image files, photographs, recordings, software, and in general, any other type of material accessible through the Website or the services offered therein.</p>

     @endif

     @if ($locale=='es')
     <h2 class="size-subtitle font-black">PROPIEDAD INTELECTUAL E INDUSTRIAL</h2>
     <p>Todos los contenidos del Web, salvo que se indique lo contrario, son titularidad exclusiva de <span class="red">MANIFIESTO</span>, o de terceros que han autorizado su inclusi&oacute;n en el Web y, con car&aacute;cter enunciativo, que no limitativo, el dise&ntilde;o gr&aacute;fico, c&oacute;digo fuente, logos, textos, ilustraciones, fotograf&iacute;as, y dem&aacute;s elementos que aparecen en el Web.</p>

     <p>Igualmente, todos los nombres comerciales, marcas o signos distintivos de cualquier clase contenidos en el Web est&aacute;n protegidos por la Ley.</p>

     <p><span class="red">MANIFIESTO</span> no concede ning&uacute;n tipo de licencia o autorizaci&oacute;n de uso personal al Usuario sobre sus derechos de propiedad intelectual e industrial o sobre cualquier otro derecho relacionado con su Web y los servicios ofrecidos en la misma.</p>

     <p>Por ello, el Usuario reconoce que la reproducci&oacute;n, distribuci&oacute;n, comercializaci&oacute;n o transformaci&oacute;n no autorizadas de los elementos indicados en los apartados anteriores constituye una infracci&oacute;n de los derechos de propiedad intelectual y/o industrial de <span class="red">MANIFIESTO</span> o del titular de los mismos.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">INTELLECTUAL AND INDUSTRIAL PROPERTY</h2>

     <p>All of the contents of the Website, unless indicated to the contrary, are the exclusive property of <span class="red">MANIFIESTO</span> or third parties who have authorised their inclusion on the Website, including but not limited to the graphic design, source code, logos, texts, illustrations, photos and other elements contained on the Website.</p>
     <p>Similarly, all trademarks, business names or distinctive symbols of any kind contained on the Website are protected by Law.</p>
     <p><span class="red">MANIFIESTO</span> does not grant any type of licence or authorisation for personal use to the User in relation to its intellectual or industrial property rights, or to any other right related to its Website and the services offered therein.</p>
     <p>As a result, the User accepts that any unauthorised reproduction, distribution, commercialisation or transformation of the elements indicated above constitutes an infringement of the intellectual and/or industrial property rights of <span class="red">MANIFIESTO</span> or the owner of the same.</p>

     @endif

     @if ($locale=='es')

     <h2 class="size-subtitle font-black">R&Eacute;GIMEN DE RESPONSABILIDAD</h2>

     <strong>A) Responsabilidad por el Uso del Web</strong>

     <p>El Usuario es el &uacute;nico responsable de las infracciones en las que pueda incurrir o de los perjuicios que pueda causar o causarse por la utilizaci&oacute;n del Web, quedando <span class="red">MANIFIESTO</span>, exonerada de cualquier clase de responsabilidad que se pudiera derivar por las acciones del Usuario.</p>

     <p>El Usuario es el &uacute;nico responsable frente a cualquier reclamaci&oacute;n o acci&oacute;n legal, judicial o extrajudicial, iniciada por terceras personas contra <span class="red">MANIFIESTO</span> basada en la utilizaci&oacute;n por el Usuario del Web. En su caso, el Usuario asumir&aacute; cuantos gastos, costes e indemnizaciones sean irrogados a <span class="red">MANIFIESTO</span> con motivo de tales reclamaciones o acciones legales.</p>

     <strong>B) Responsabilidad por el funcionamiento del Web</strong>

     <p><span class="red">MANIFIESTO</span> excluye toda responsabilidad que se pudiera derivar de interferencias, omisiones, interrupciones, virus inform&aacute;ticos, aver&iacute;as telef&oacute;nicas o desconexiones en el funcionamiento operativo del sistema electr&oacute;nico, motivado por causas ajenas a <span class="red">MANIFIESTO</span>. </p>

     <p>Asimismo, <span class="red">MANIFIESTO</span> tambi&eacute;n excluye cualquier responsabilidad que pudiera derivarse por retrasos o bloqueos en el funcionamiento operativo de este sistema electr&oacute;nico causado por deficiencias o sobre carga en las l&iacute;neas telef&oacute;nicas o en Internet, as&iacute; como de da&ntilde;os causados por terceras personas mediante intromisiones ilegitimas fuera del control de <span class="red">MANIFIESTO</span>. </p>

     <strong>C) Responsabilidad por links</strong>

     <p><span class="red">MANIFIESTO</span> declina toda responsabilidad respecto a la informaci&oacute;n que se halle fuera del Web ya que la funci&oacute;n de los links que puedan aparecer es &uacute;nicamente la de informar al Usuario sobre la existencia de otras fuentes de informaci&oacute;n sobre un tema en concreto.</p>

     <p><span class="red">MANIFIESTO</span> se exonera de toda responsabilidad por el correcto funcionamiento de tales enlaces, del resultado obtenido a trav&eacute;s de dichos enlaces, de la veracidad y licitud del contenido o informaci&oacute;n a la que se puede acceder as&iacute; como de los perjuicios que pueda sufrir en virtud de la informaci&oacute;n encontrada en el Web enlazado.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">LIABILITY REGIME</h2>
     <strong>A) Liability for Use of the Website</strong>
     <p>The User shall assume sole liability for any infringements or damage they may cause or which may be caused as a result of using the Website, holding <span class="red">MANIFIESTO</span> harmless against any type of liability that may arise as a result of the User’s actions.</p>
     <p>The User shall assume sole liability for any claim or legal proceedings or out-of-court actions brought by third persons against <span class="red">MANIFIESTO</span> arising from the use of the Website by the User. Where applicable, the User shall be responsible for any expenses, costs and compensation affecting <span class="red">MANIFIESTO</span> as a result of any such claims or legal proceedings.</p>
     <strong>B) Liability for the operation of the Website</strong>
     <p><span class="red">MANIFIESTO</span> may not be held liable for any interference, omissions, interruptions, computer viruses, damage to telephone lines or disconnections in the operation of the electronic system resulting from causes that are not attributable to <span class="red">MANIFIESTO</span>.</p>
     <p>Additionally, <span class="red">MANIFIESTO</span> may not be held liable for any delays or blockages in the operation of this electronic system caused by defects or overloading of the telephone lines or Internet, or for any damages caused by third persons arising from illegal interference beyond the control of <span class="red">MANIFIESTO</span>.</p>
     <strong>C) Liability for links</strong>
     <p><span class="red">MANIFIESTO</span> accepts no liability for any information outside of the Website, as the purpose of any links that may be shown is solely to inform the User of the existence of other sources of information in relation to a specific subject.</p>
     <p><span class="red">MANIFIESTO</span> may not be held liable for the correct functioning of any such links, the results obtained from these links, the veracity and legitimacy of the contents or information that may be accessed, or for any damage arising as a result of the information found on the linked website.</p>

     @endif


     @if ($locale=='es')
     <h2 class="size-subtitle font-black">MODIFICACIONES DEL AVISO LEGAL</h2>

     <p><span class="red">MANIFIESTO</span> se reserva el derecho a modificar unilateralmente en cualquier momento y sin aviso previo, la presentaci&oacute;n y configuraci&oacute;n de este Web, as&iacute; como los servicios y condiciones requeridas para usar el Web y sus servicios.</p>

     <p>El Usuario quedar&aacute; obligado autom&aacute;ticamente por el Aviso Legal que se halle vigente en el momento en que acceda al Web, por lo que deber&aacute; leer peri&oacute;dicamente dicho Aviso Legal.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">MODIFICATIONS TO THE LEGAL NOTICE</h2>
     <p><span class="red">MANIFIESTO</span> reserves the right to alter at any moment and without prior notice the appearance and configuration of this Website, as well as the services and terms and conditions required in order to use the Website and its services.</p>
     <p>The User shall be automatically bound by the terms and conditions of the Legal Notice in force at the moment they access the Website, as a result of which they must regularly refer to the Legal Notice.</p>

     @endif



     @if ($locale=='es')
     <h2 class="size-subtitle font-black">CONFIDENCIALIDAD</h2>

     <p><span class="red">MANIFIESTO</span> se compromete al cumplimiento de su obligaci&oacute;n de secreto de los datos de car&aacute;cter personal y de su deber de guardarlos de forma confidencial y adoptar&aacute; las medidas necesarias para evitar su alteraci&oacute;n, p&eacute;rdida, tratamiento o acceso no autorizado, habida cuenta en todo momento del estado de la tecnolog&iacute;a.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">CONFIDENTIALITY</h2>
     <p><span class="red">MANIFIESTO</span> agrees to comply with its obligation to ensure the secrecy of all personal data and its duty to safeguard them in a confidential manner and take all necessary steps to prevent their alteration, loss, unauthorised treatment or access, taking into account the state of the art at all times.</p>

     @endif



     @if ($locale=='es')
     <h2 class="size-subtitle font-black">OBSERVACIONES DEL USUARIO</h2>

     <p>El Usuario garantiza que la informaci&oacute;n, material, contenidos u observaciones que no sean sus propios datos personales y que sean facilitados a <span class="red">MANIFIESTO</span> a trav&eacute;s del Web, no infringen los Derechos de Propiedad Intelectual o Industrial de terceros, ni ninguna otra disposici&oacute;n legal.</p>

     <p>La informaci&oacute;n, materiales, contenidos u observaciones que el Usuario facilite a <span class="red">MANIFIESTO</span> se considerar&aacute;n no confidenciales, reserv&aacute;ndose <span class="red">MANIFIESTO</span>, el derecho a usarlas de la forma que considere m&aacute;s adecuada.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">USER COMMENTS</h2>
     <p>The User warrants that any information, material, contents or comments that are not their own personal data and which are provided to <span class="red">MANIFIESTO</span> through the Website do not represent any infringement of the Intellectual or Industrial Property Rights of any third parties or any other legal provision.</p>
     <p>Any information, material, contents or comments provided by the User to <span class="red">MANIFIESTO</span> shall be considered as non-confidential, with <span class="red">MANIFIESTO</span> reserving the right to use them in the manner it considers most appropriate.</p>


     @endif



     @if ($locale=='es')
     <h2 class="size-subtitle font-black">LEGISLACI&Oacute;N APLICABLE Y COMPETENCIA JUDICIAL</h2>

     <p>Cualquier controversia surgida de la interpretaci&oacute;n o ejecuci&oacute;n del presente Aviso Legal se interpretar&aacute; bajo la legislaci&oacute;n espa&ntilde;ola.</p>

     <p>Asimismo, <span class="red">MANIFIESTO</span> y el Usuario, con renuncia a cualquier otro fuero, se someten al de los juzgados y tribunales del domicilio del Usuario para cualquier controversia que pudiera surgir. En el caso de que el Usuario tenga su domicilio fuera de Espa&ntilde;a, <span class="red">MANIFIESTO</span> y el Usuario se someten a los juzgados y tribunales de la ciudad de Barcelona.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">APPLICABLE LAW AND JURISDICTION</h2>
     <p>Any dispute arising as a result of the interpretation or application of this Legal Notice shall be governed in accordance with Spanish Law.</p>
     <p>Similarly, <span class="red">MANIFIESTO</span> and the User waive any other jurisdiction that may correspond to them and agree to submit any dispute to the courts and tribunals of the User’s place of residence. In the event of the User residing outside of Spain, <span class="red">MANIFIESTO</span> and the User shall submit any disputes to the courts and tribunals of the City of Barcelona, Spain.</p>


     @endif



     @if ($locale=='es')
     <h2 class="size-subtitle font-black">POL&Iacute;TICA DE PRIVACIDAD</h2>

     <p>En cumplimiento de lo establecido en la Ley Org&aacute;nica 15/1999, de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal, te informamos que tus datos personales quedar&aacute;n incorporados y ser&aacute;n tratados en un fichero propiedad de <span class="red">MANIFIESTO</span> con la exclusiva finalidad de poder dar respuesta a tu demanda de empleo con alguna oferta de trabajo.</p>

     <p>Te informamos sobre la posibilidad que tienes de ejercer, en cualquier momento, tus derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n de tus datos de car&aacute;cter personal mediante <a href="mailto:contacto@manifiesto.biz">correo electrónico</a> o bien mediante escrito dirigido a Comunicaci&oacute;n Creativa Manifiesto, S.L. / Protecci&oacute;n de datos, Trafalgar n&ordm; 38, planta 1&ordf;, 08010 Barcelona.</p>
     @endif

     @if ($locale=='en')
     <h2 class="size-subtitle font-black">PRIVACY POLICY</h2>
     <p>Pursuant to the provisions of Organic Law 15/1999 of 13 December on the Protection of Personal Data, you are hereby informed that your personal data will be included and treated on a file owned by <span class="red">MANIFIESTO</span> for the exclusive purpose of replying to your request for employment with a job offer.</p>
     <p>You are further informed that you may access, rectify, cancel or oppose your personal data at any time by sending an e-mail to <a href="mailto:contacto@manifiesto.biz">e-mail</a> or otherwise by writing to: Comunicación Creativa Manifiesto, S.L. / Protección de datos, C/ Trafalgar 38, Primera Planta, 08010, Barcelona.</p>

     @endif



 </div>
 </div>
</div>
 