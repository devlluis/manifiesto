@extends('layouts.error')

@section('styles')

@stop

@section('js')
<script src="/assets/js/legal.js"></script>
@stop


@section('content')

<div class="content-fullwidth columns level is-mobile">
	<div class="column is-8 is-offset-2 is-size-3 level">
		<h1 class="font-black">ERROR 404</h1>
		@lang('general.404',['url'=>Request::path()], $locale)
	</div>
</div>

<div class="video-iframe">
	
	<iframe width="100%" height="100%" src="https://www.youtube.com/embed/dQw4w9WgXcQ?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1&autohide=1&loop=1" frameborder="0" allowfullscreen></iframe>

</div>	

@stop

