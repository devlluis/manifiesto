@extends('layouts.error')

@section('styles')

@stop

@section('js')

@stop


@section('content')

	<div class="col-md-6">
	<h1>ERROR 500</h1>
	@lang('general.500',['url'=>Request::path(), $locale])
	<br><br>
	<iframe width="100%" height="250" src="https://www.youtube.com/embed/ZTidn2dBYbY?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1&autohide=1&" frameborder="0" allowfullscreen></iframe>
	</div>

@stop

