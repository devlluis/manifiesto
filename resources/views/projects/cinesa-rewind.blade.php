@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Cinesa quería lanzar un nuevo producto cinematográfico: sesiones de películas clásicas.
	<h2>La búsqueda</h2>
	Para poder hacer la campaña perfecta, primero identificamos a nuestro target: Personas nacidas entre los 70 y los 90, amantes del cine y a lo que les gustaría ver otra vez sus pelis favoritas en el cine. El objetivo: llamar su atención y conseguir que acudieran a las sesiones de Cinesa.
	@stop

	@section('col2')
	<h2>El insight</h2>
	Hay un sentimiento que nuestro público tenía en común: LA NOSTALGIA, las ganas de rebobinar al pasado, revivir momentos únicos y recordar sus años locos.
	<h2>La idea</h2>
	A partir de este insight, definimos el nuevo naming y la imagen de la campaña. Las sesiones se llamarían REWIND y el claim: películas que te harán rebobinar. Después, desarrollamos un fantástico spot para cines y una potente campaña online y offline.
	@stop

	@section('col3')


	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	Cinesa wanted to launch a new cinema product: classic movie sessions.
	<h2>The search</h2>
	To create the perfect campaign, we first identified our target: Film lovers born from the 70s to the 90s who wanted to see their favourite movies in the theatre once again. The objective: grab their attention and get them to come to the Cinesa sessions.
	@stop

	@section('col2')
	<h2>The insight</h2>
	There is one emotion that all of our audience has in common: NOSTALGIA, the desire to rewind back into the past, to relive unique moments and remember their crazy years.
	<h2>The idea</h2>
	On the basis of this insight, we defined the new name and image for the campaign. The sessions would be called REWIND and the slogan was "películas que te harán rebobinar" ("Movies that will make you rewind"). We then produced a wonderful commercial to show in cinemas and a powerful online and offline campaign.
	@stop

	@section('col3')

	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'108263592'])

@stop

@section('case')

	<section class="row">
		<div class="col-md-5">

			@include('includes.projects.h3-row',['title'=>Lang::get('project.spot-cinema',[], $locale)])

		</div>
		<div class="col-md-offset-1 col-md-6">
			@include('includes.projects.video',['id'=>'76408380'])

		</div>
	</section>

	<hr>

	@include('includes.projects.h3-row',['title'=>Lang::get('project.campaign',[], $locale)])
	
	<section class="row">
		<div class="col-md-4">
			<h4>@lang('project.offline')</h4>
				<h5>@lang('project.spot-cinema')</h5>
				<h5>@lang('project.mupis')</h5>
				<h5>@lang('project.print')</h5>
				<h5>@lang('project.spot-radio')</h5>
		</div>
		<div class="col-md-4">
			<h4>@lang('project.online')</h4>
				<h5>@lang('project.facebook-tab')</h5>
				<h5>@lang('project.facebook-ads')</h5>
				<h5>@lang('project.community')</h5>
				<h5>@lang('project.banner-campaign')</h5>
		</div>
	</section>

	<section class="row bg-gray">
		<div class="col-md-12 col-lg-offset-2 col-lg-8">
			<h4>@lang('project.poster')</h4>
			@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'poster.jpg'])

		</div>
	</section>

	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-md-12">
			@include('includes.projects.results',['icon'=>'users','title'=>'11.000','es'=>'espectadores acudieron a las sesiones Rewind','en'=>'people came to watch the Rewind sessions'])
		</div>
	</div>
	
	</section>

@stop
