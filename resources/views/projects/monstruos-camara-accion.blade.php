@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Monster High nos pidió idear un concurso que sorprendiera a los fans y que reforzara el lanzamiento de la nueva película ¡Monstruos!, ¡Cámara!, ¡Acción!.
	<h2>La búsqueda</h2>
	Para conectar con los fans de Monster High, necesitábamos ofrecerles algo que jamás nadie había hecho antes, que les sorprendiera y les aportara mucho más que un simple premio. Analizamos a nuestro público y encontramos la idea que necesitábamos.
	@stop

	@section('col2')
	<h2>La idea</h2>
	El argumento de la película Monster High ¡Monstruos!, ¡Cámara!, ¡Acción! une la diversión por viajar y el mundo del cine. Y pensamos, ¿por qué no crear una aplicación que permita a las niñas crear su propia película mientras viajan por los diferentes escenarios de la peli? <br>
	Y así nació el concurso Monster High ¡Monstruos!, ¡Cámara!, ¡Acción!, una aplicación en la que las fans podían crear su propia versión de la película y convertirse en auténticas monstro-directoras de cine. La historia empezaba en el aeropuerto y las niñas debían coger vuelos y crear las diferentes secuencias de la peli a medida que iban viajando por el mundo.
	@stop

	@section('col3')
	<h2>La gala final</h2>
	De todas las películas creadas, las 25 que obtuvieron el mayor número de votaciones asistieron como finalistas a la gran gala de entrega de premios, la Garra de Plata. Todas las demás participantes podían acudir también para ver la entrega de premios. <br>
	Así, creamos una gala virtual e interactiva en la que las niñas podían vivir en primera persona la entrega de premios, pasear por la alfombra roja, firmar autógrafos y hacerse fotos con las monstruitas. Toda una experiencia para las usuarias.
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	Monster High asked us to come up with a competition to surprise their fans and support the launch of the new movie called Frights, Camera, Action!
	<h2>The search</h2>
	To connect with Monster High fans, we needed to offer them something that nobody had done before, that would surprise them and that would give them much more than a mere prize. We analysed our audience and found the idea we were looking for.
	@stop

	@section('col2')
	<h2>The idea</h2>
	The plot of the Monster High Frights, Camera, Action! movie combines the fun of travelling with the movie world. And we thought, why not create an app that girls could use to create their own movie as they travel through the different scenes in the movie?<br>
	And so we came up with the Monster High Frights, Camera, Action! competition, an app that fans could use to create their own version of the movie and feel like real monster-movie directors. The story started at the airport and the girls had to catch flights and create different sequences from the movie as they travelled around the world.
	@stop

	@section('col3')
	<h2>The final gala</h2>
	Out of all the movies that were created, the 25 that got the most votes came as finalists to the grand prize-giving gala, the Silver Claw. All of the other entrants could also come to watch the prize-giving ceremony.<br>
	So we created a virtual and interactive gala where the girls could enjoy the prize-giving ceremony in person, walk on the red carpet, sign autographs, and take photos with the monsters. A great experience for the users.
	@stop
	
@endif


@section('mainVideo')

	{{--@include('includes.projects.mainvideo',['id'=>'89413338'])--}}

@stop

@section('case')
	
	<hr>

	@include('includes.projects.h3-row',['title'=>Lang::get('project.campaign',[], $locale)])

	<section class="row">

		<div class="col-md-12 fadeInUp preanimate">

			<h4>@lang('project.website',[], $locale)</h4>

			@if ($locale=='es')
			Los niños y niñas podían crear sus episodios a través de la aplicación.
			@else
			Boys and girls could use the app to create their own episodes.
			@endif

			@include('includes.projects.slider',['numPics'=>6])


		</div>
		<div class="col-md-12 fadeInLeft preanimate">
			@include('includes.projects.project-dash')

			@if ($locale=='es')
				<h4>Gala de la Garra de Plata</h4>
				Diseño y producción de la gala interactiva.
			@else
				<h4>Silver Claw Gala</h4>
				Design and production of the interactive gala.
			@endif


		</div>

	</section>

	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			<h2>@lang('project.video-viral',[], $locale)</h2>

			@if ($locale=='es')
				Creación del vídeo explicando el concurso
			@else
				Creation of a video to introduce the competition
			@endif

			@include('includes.projects.project-dash')

		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video',['id'=>'89413338'])

		</div>
		<div class="col-md-12 fadeInUp preanimate">
			<h2>@lang('project.newsletters',[], $locale)</h2>

			@if ($locale=='es')
				Más de 200.000 newsletters enviadas
			@else
				More than 200,000 newsletters sent out
			@endif

			<br><br>

			@lang('project.and-also',[], $locale):
			<ul class="list-also">
				<li>@lang('project.video-banners',[], $locale)</li>
				<li>@lang('project.community',[], $locale)</li>
				<li>@lang('project.press-releases',[], $locale)</li>
			</ul>
	
		</div>
	</section>

	<section class="row row-results">

	@include('includes.projects.h3-row',['title'=>Lang::get('project.results-europe',[], $locale)])
	
	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'screen','title'=>'460.000','es'=>'visitas en el site','en'=>'visits to the site'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'users','title'=>'12.000','es'=>'usuarios registrados','en'=>'registered users'])
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'movie','title'=>'7.660','es'=>'películas creadas','en'=>'movies created'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'check','title'=>'17','es'=>'minutos en el site de media','en'=>'minutes on the site on average'])
		</div>
	</div>

	</section>

@stop
