@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Para la temporada de Navidad, <strong>Asus</strong> nos pidió una campaña digital que conectara con un público de 25 a 30 años. ¿Qué podíamos hacer para destacar ante toda la avalancha de virales que se mueven en estas fechas? 
@stop

@section('col2')
    <h2>El concepto</h2>
    Decidimos hablar de algo que va más allá de la Navidad y el espíritu familiar: la amistad. Con el concepto <strong>Friendship is incredible</strong> pudimos transmitir que los amigos son la familia que se elige y todos seríamos capaces de hacer cualquier cosa por ellos, sea nochebuena o no.
@stop

@section('col3')
    <h2>La idea</h2>
    Creamos un spot para internet con una historia, que no tenía nada que ver con la Navidad, pero que mantenía la emoción e incertidumbre hasta el final. A partir de esta pieza, articulamos una <strong>campaña de banners, video In Stream, influencers</strong> y <strong>redes sociales</strong> para llegar a conectar con el target group. 
    <br/><br/>
    La campaña estaba monitorizada a tiempo real y eso nos permitió hacer ajustes, afinar con la segmentación y <strong>mejorar el engagement y alcance</strong> en todo momento.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    For Christmas, <strong>Asus</strong> asked for a digital campaign that connected with a public averaging 25 to 30 years old. What should we do to stand in front of all virals that are running those dates?
@stop

@section('col2')
    <h2>The concept</h2>
    We tried to talk about something more than Christmas and family spirit: friendship. With <strong>Friendship is incredible</strong> we transmitted that friends are the family we choose and we all do almost anything for them, even if it's Christmas Eve.
@stop

@section('col3')
    <h2>The idea</h2>
    We created an internet spot with an story that wasn't related to Christmas, but that mantained emotion and uncertainity until the end. Through that piece, we created a <strong>banner, video In Stream, influencers</strong> and <strong>social media campaing</strong>, trying to reach the target group. 
    <br/><br/>
    The campaign was real time monitored, and that allowed us to make adjustments, refine segmentation, <strong>improve engagement and reach</strong> at all times.
@stop

@endif


@section('case')

    <section class="row">
        <div class="col-md-4 fadeInLeft preanimate">
            <h3 style="margin-top: 40px">{{Lang::get('project.video-case',[], $locale)}}</h3>
            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-8 fadeInRight preanimate">
            @include('includes.projects.video',['id'=>'208809935'])
        </div>
    </section>
    
    <section class="row">
        <div class="col-md-4 fadeInLeft preanimate">
            <h3 style="margin-top: 40px">{{Lang::get('project.spot-online',[], $locale)}}</h3>
            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-8 fadeInRight preanimate">
            @include('includes.projects.video-yt',['id'=>'7GfslU9GC98'])
        </div>
    </section>

    <section class="row bg-gray">
        <div class="col-md-12">
            <h3 style="margin-top: 40px">@include('includes.trans',['es'=>'Campaña en redes sociales','en' => 'Social media campaign'])</h3>
            @include('includes.projects.project-dash')
        </div>

        <div class="col-md-12">
            <div class="twitter-container">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'twitter-icon.jpg','class'=>'fadeInLeft preanimate twitter-icon'])
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'twitter-feed.jpg','class'=>'fadeInUp preanimate twitter-feed'])
                <webm class="fadeInUp preanimate twitter-gif" source="{{$image_path}}twitter-gif" fallback="{{$image_path}}twitter-gif.gif"></webm>

           </div>
           <div class="instagram-facebook-container">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'instagram-icon.jpg','class'=>'fadeInLeft preanimate instagram-icon'])
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'instagram-feed.jpg','class'=>'fadeInUp preanimate instagram-feed'])

                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'facebook-icon.jpg','class'=>'fadeInRight preanimate facebook-icon'])
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'facebook-feed.jpg','class'=>'fadeInUp preanimate facebook-feed'])
                <webm class="fadeInUp preanimate facebook-gif" source="{{$image_path}}facebook-gif" fallback="{{$image_path}}facebook-gif.gif"></webm>
                
           </div>
           <div class="youtube-container">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'youtube-icon.jpg','class'=>'fadeInRight preanimate youtube-icon'])
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'youtube-feed.jpg','class'=>'fadeInUp preanimate youtube-feed'])
                <iframe class="fadeInUp preanimate youtube-gif" src="https://www.youtube.com/embed/7GfslU9GC98" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>  
           </div>
        </div>

         <div class="col-md-12">
            <h3 style="margin-top: 100px">@include('includes.trans',['es'=>'Campaña de influencers','en' => 'Influencer campaign'])</h3>
            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-12 text-center">
            <div class="post-influencer fadeInUp preanimate">
                @for($i=1;$i<=4;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=5;$i<=9;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=10;$i<=14;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=15;$i<=18;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=19;$i<=23;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=24;$i<=27;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
            <div class="post-influencer fadeInUp preanimate">
                @for($i=28;$i<=31;$i++)
                    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'influencer-' .$i. '.png'])<br>
                @endfor
            </div>
        </div>
    </section>


    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'video','title_es'=>'1.132.663','es'=>'reproducciones en solo 15 días','title_en'=>'1.132.663','en'=>'views in 15 days'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'eye','title_es'=>'15.911.874','es'=>'de impresiones','title_en'=>'15.911.874','en'=>'impressions'])
            </div>
            <div class="col-md-4" style="margin-top:0px"></div>
        </div>
        <div class="row">
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'engagement','title_es'=>'100%','es'=>'engagement','title_en'=>'100%','en'=>'engagement'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'like','title_es'=>'99%','es'=>'comentarios positivos','title_en'=>'99%','en'=>'positive comments'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users-play','title_es'=>'26%','es'=>'visualización in stream','title_en'=>'26%', 'en'=>'In Stream visualization'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'twitter','title_es'=>'+86%','es'=>'retweets ','title_en'=>'+86%','en'=>'retweets'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'heart','title_es'=>'1 nuevo','es'=>'posicionamiento más emocional','title_en'=>'1 new','en'=>'and more emotional positioning'])
            </div>
            <div class="col-md-4" style="margin-top:0px"></div>
        </div>
    </section>

@stop