@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Yamaha nos planteó realizar un evento para llegar al público joven de entre 18 y 28 años.
	<h2>La búsqueda</h2>
	La primera pregunta era evidente: ¿dónde podemos encontrar a los jóvenes de Madrid? Y los fuimos a buscar donde están, o debería estar: EN LA UNIVERSIDAD.
	@stop

	@section('col2')
	<h2>El evento</h2>
	Primero elegimos el lugar: la Universidad Rey Juan Carlos de Madrid. Después, decidimos crear y organizar una auténtica experiencia de marca para que los jóvenes pasaran un día alucinante. Y la mejor manera de hacerlo era dejando que probasen ellos mismo los vehículos Yamaha.<br>
	Llamamos al evento “Yamaha Xperience Madrid”. Durante todo un día, los universitarios pudieron probar scooters, quads e incluso motos de agua en el lago de la Universidad.
	@stop

	@section('col3')
	<h2>&nbsp;</h2>
	Montamos un photocall muy especial, organizamos talleres para crear merchandising personalizado y convertimos el evento en una fiesta con espectáculos Freestyle y la actuación del batería Deivhook para rematar.
	@stop

@endif

@if ($locale=='en')

	@@section('col1')
	<h2>The Task</h2>
	Yamaha asked us to organise an event to reach young people aged between 18 and 28 years.
	<h2>The search</h2>
	The first question was obvious: Where could we find young people in Madrid? So we went to place where they go, or should go: THE UNIVERSITY.
	@stop

	@section('col2')
	<h2>The event</h2>
	First we picked the place: At Rey Juan Carlos University, Madrid. Then we went about creating and organising a veritable brand experience that gave young people a fantastic day. And the best way to do that was by giving them a chance to try out Yamaha vehicles for themselves.<br>
	We called the event “Yamaha Xperience Madrid”. For a whole day, students could try out scooters, quads and even jet-skis on the university lake. 
	@stop

	@section('col3')
	<h2>&nbsp;</h2>
	We arranged a very special photocall, organized workshops to create personalised merchandising and put on a real show with Freestyle displays and a performance by the drummer Deivhook to finish.
	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'113023478'])

@stop

@section('case')
	
	<hr>

	<section class="row">
		<div class="col-md-12">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.online-campaign',[], $locale)])

		@if ($locale=='es')
		Los usuarios podían reservar su vehículo a través de la web
		@else
		Users could book their vehicles on the website
		@endif

		<br><br>
		</div>
	    @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'web.jpg','class'=>'col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10'])

	</section>
	
	<section class="row">
	@include('includes.projects.project-dash')
	</section>

	<section class="row bg-gray">
		<div class="col-md-12 col-lg-6 no-padding">
			<h4>@lang('project.signage',[], $locale)</h4>
            @include('includes.picture' ,['image' => 'poster.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])

        </div>
		<div class="col-md-12 col-lg-6 ">
		<h4 class="side-padding">@lang('project.rollup',[], $locale)</h4>
		@for ($i = 1; $i <= 6; $i++)
			<div class="col-xs-6 col-md-4 side-padding">
                @include('includes.picture' ,['image' => 'rollup-'.$i.'.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
            </div>
		@endfor
		</div>

		<div class="col-md-12 clearfix side-padding">
		<h4>@lang('project.event',[], $locale)</h4>
		@for ($i = 1; $i <= 7; $i++)
			<div class="col-md-6">
                @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
            </div>
		@endfor

		<div class="col-md-6 full-padding">
			@lang('project.and-also',[], $locale):
				<ul class="list-also">
					<li>@lang('project.flags',[], $locale)</li>
					<li>@lang('project.perching',[], $locale)</li>
					<li>@lang('project.tfts',[], $locale)</li>
					<li>@lang('project.press-releases',[], $locale)</li>
				</ul>
			</div>
		</div>
		

	</section>
	
	
	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'users','title'=>'1650','es'=>'estudiantes acudieron al evento','en'=>'students attended the event'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'cog','title'=>'70','es'=>'productos Yamaha sorteados','en'=>'Yamaha products were drawn as a gift'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'check','title'=>'Likes','es'=>'Instagram se llenó de publicaciones','en'=>'Instagram was filled with posts'])
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'music','title'=>'shake it','es'=>'¡Incluso surgió un Harlem Shake improvisado!','en'=>'There was even an impromptu Harlem Shake!'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'voice','title'=>'publicity','es'=>'Conseguimos múltiples apariciones en medios online y offline','en'=>'We achieved several appearances in online and offline media'])
		</div>
	</div>

	</section>

@stop
