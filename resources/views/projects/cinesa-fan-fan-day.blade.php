@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Cinesa nos pidió realizar la promoción más sencilla del mundo: si eres fan de Cinesa, tienes un 2x1 en la entrada.
	<h2>La búsqueda</h2>
	El reto era: ¿cómo sacarle el máximo partido a un tradicional 2x1? <br>Queríamos realizar una campaña notoria y divertida que fuese más allá de la típica promoción de Facebook. Y la idea nos llegó como caída del cielo.
	@stop

	@section('col2')
	<h2>La idea – Storytelling</h2>
	Decidimos crear una campaña de 2x1 que conectara con nuestro público a través del Storytelling. <br>
	Cinesa lleva años y años pensando promociones para que los fans vayan más al cine y esta vez había llegado el momento de agradecer su fidelidad y recompensarles por su lealtad. <br>
	@stop

	@section('col3')
	<h2>&nbsp;</h2>
	¿Y qué mejor manera de hacerlo que creando el día del fan? Primero, le pusimos un nombre a la promoción: El Oh! Fan Fan Day. Empezamos a llamar a los fans de Cinesa como “fieles seguidores”. Incorporamos a un predicador y también a un coro.  ¡Incluso creamos un jingle! Lo animamos todo, y así nació nuestro spot.

	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	Cinesa asked us to produce the simplest promotion in the world: if you are a fan of Cinesa, you get a 2x1 ticket.
	<h2>The search</h2>
	The challenge was how to get the most out of a traditional 2x1 offer.<br>
	We wanted to put together a catchy, fun campaign that did a bit more than the typical Facebook promotion. And the idea truly was made in heaven.
	@stop

	@section('col2')
	<h2>The idea – Storytelling</h2>
	We decided to create a 2x1 campaign to connect with our audience through Storytelling. <br>
	For years and years, Cinesa has been devising promotions to get fans to come to the cinema. The time had come to thank and reward them for their loyalty.<br>
	@stop

	@section('col3')
	<h2>&nbsp;</h2>
	And what better way to do that than by creating a fans day? We first came up with a name for the promotion: The Oh! Fan Fan Day. We started referring to Cinesa fans as “faithful followers”. We included a preacher and a choir too. We even created a jingle! We used all of those elements in an animated commercial film.

	@stop
	
@endif


@section('mainVideo')

	<h3>@lang('project.video-case')</h3>

	@include('includes.projects.project-dash')

	@include('includes.projects.mainvideo',['id'=>'107893518'])

@stop

@section('case')

	<section class="row">
		<div class="col-md-6">
			<h3>@lang('project.spot-online-cinema')</h3>

			@include('includes.projects.project-dash')

		</div>

		<div class="col-md-6">
			@include('includes.projects.video',['id'=>'78144477'])
		</div>
	</section>

    <section class="row">
        <div class="col-md-6">
            <h3>@lang('project.event-cinema')</h3>

            @include('includes.projects.project-dash')
        </div>

        <div class="col-md-6">
            @include('includes.projects.video',['id'=>'80445417'])

        </div>
    </section>

	<hr>

	@include('includes.projects.h3-row',['title'=>Lang::get('project.campaign',[], $locale)])

	<section class="row">

		<div class="col-md-6">
			<h4>@lang('project.offline')</h4>
				<h5>@lang('project.spot-online-cinema')</h5>
				<h5>@lang('project.posters')</h5>
				<h5>@lang('project.mupis-digital')</h5>
				<h5>@include('includes.trans',['es'=>'Decoración de 41 salas de cine','en'=>'Decorations in 41 movie theatres'])</h5>
				<h5>@include('includes.trans',['es'=>'Evento en 1 de los cines','en'=>'Event in 1 of the theatres'])</h5>
				<h5>@include('includes.trans',['es'=>'¡Dimos vida al predicador y regalamos premios en directo!','en'=>'We brought the preacher to life and he gave away prizes!'])</h5>
		</div>
		<div class="col-md-6">
			<h4>@lang('project.online')</h4>
				<h5>@lang('project.facebook-tab')</h5>
				<h5>@lang('project.community')</h5>
				<h5>@lang('project.facebook-ads')</h5>
				<h5>@lang('project.banners')</h5>
				<h5>@include('includes.trans',['es'=>'Los fans podían descargarse el cupón y participar en el sorteo de un año de cine gratis.','en'=>'Fans could download the voucher and take part in a draw for a year of free movie tickets.'])</h5>
		</div>

	</section>

	<section class="row bg-gray">
		<div class="col-md-12">
			<h4>@lang('project.event-cinema')</h4>
	            @include('includes.projects.project-dash')
			
			@for ($i = 1; $i <= 4; $i++)
			<div class="col-md-6">
	            @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
	        </div>
			@endfor
		</div>
		
		<div class="col-sm-12 col-md-12 col-lg-6">
			<h4>@lang('project.posters')</h4>
            @include('includes.projects.project-dash')
			
            @include('includes.picture' ,['image' => 'poster.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])

        </div>

		<div class="col-sm-12 col-md-12 col-lg-6">
			<h4>@lang('project.facebook-tab')</h4>
            @include('includes.projects.project-dash')
			
            @include('includes.picture' ,['image' => 'facebook.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])

        </div>

	</section>
	

	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'heart','title'=>'40.000','es'=>'nuevos fans de Cinesa en solo 2 semanas','en'=>'new Cinesa fans in just 2 weeks'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'download','title'=>'9.000','es'=>'descargas del 2x1 en 1 semana','en'=>'downloads of 2x1 vouchers in 1 week'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'users','title'=>'25.000','es'=>'personas celebraron el Oh! Fan Fan Day','en'=>'people celebrated Oh! Fan Fan Day'])
		</div>
	</div>
	</section>

@stop
