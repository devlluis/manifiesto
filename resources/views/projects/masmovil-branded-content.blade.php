@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Másmóvil nos pidió crear unos vídeo-tutoriales que explicaran el funcionamiento de la compañía a diferentes niveles. Querían algo dicáctico, fresco y muy fácil de entender. 
	@stop

	@section('col2')
	<h2>El concepto</h2>
	Los clientes de Másmóvil iban a consultar estos vídeos para resolver dudas, ¿no? Por eso creamos un personaje: "El Resuelvedudas", un concepto transversal que convertiría los vídeo-tutoriales en branded content. 
	@stop

	@section('col3')
	<h2>La idea</h2>
	Dimos vida a nuestro personaje a partir de una voz y le dotamos de un tono y un estilo que solo podía tener una compañía como Másmóvil. Cada vídeo se convertiría en una pieza única llena de humor, que vehiculamos a través de insights propios de su público objetivo. ¿El objetivo? Que los vídeo-tutoriales nunca más sean aburridos. 
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	Másmovil asked us to create video tutorials that explain the company proceedings at different levels. They wanted something didactic, fresh and very easy to understand.
    @stop

	@section('col2')
	<h2>The concept</h2>
	Másmovil costumers would be leaded up to solve their questions via these videos, didn't they? That's why we created the character: "El Resuelvedudas" (TroubleSolver), a cross-cutting concept that turns video tutorials into branded content.
	@stop

	@section('col3')
	<h2>The idea</h2>
	We gave life to our characther from a voice and we gave him a tone and style that only a company like Másmovil could have. Every video would become a piece full of humor, which we drive through its target insights. The challenge? Video tutorials won't be no longer boring.
	@stop
	
@endif


@section('case')

    <section class="row">   
            <div class="col-md-12">
                <h3>{{Lang::get('project.video-tutorials',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'188827354'])
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'188827353'])
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'188827352'])
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'188827351'])
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'188827350'])
            </div>
            <div class="col-md-6 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'224179110'])
            </div>
    </section>

    <section class="row bg-gray">
        <div class="col-md-12">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1.jpg','class'=>''])
        </div>
        <div class="col-md-12" style="margin-top: 2.5%;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-2.png','class'=>''])
        </div>
    </section>

    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'question','title_es'=>'+15.000','es'=>'dudas resueltas','title_en'=>'+15.000','en'=>'solved doubts'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users','title_es'=>'700.000','es'=>'clientes sin dudas','title_en'=>'700.000','en'=>'costumers without questions'])
            </div>
        </div>
    </section>

@stop
