@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	El Servei Català de Trànsit nos hizo un encargo muy especial: una campaña para redes sociales con motivo del Día Mundial de las Víctimas de Tráfico.
	<h2>El objetivo</h2>
	El objetivo era prevenir los accidentes de tráfico y recordar a las víctimas. Para conseguirlo, decidimos crear una campaña que los usuarios pudiesen hacerse suya, participando y colaborando para que el mensaje se convirtiera en viral.
	@stop

	@section('col2')
	<h2>La idea</h2>
	Primero, decidimos ejemplificar la actitud que debía tener un conductor en la carretera con un gesto que sirviera de símbolo de toda la campaña. Para ello, utilizamos de base la expresión “tener dos dedos de frente” e invitamos a todo el mundo a unirse a la acción compartiendo una foto con el simple gesto de ponerse dos dedos en la frente. Este gesto lo acompañamos del claim <strong>“Més seny, menys víctimes”</strong> y a nivel online, del hashtag <strong>#johipososeny</strong>.<br>
	@stop

	@section('col3')
	<h2>La estrategia</h2>
	Para lanzar la campaña, apostamos por una estrategia de marketing de influencers en la que participaron un centenar de famosos de todos los ámbitos, desde actores a políticos. Además, lanzamos un vídeo a través de Twitter y Facebook donde explicamos en qué consistía la campaña y cómo unirse a la acción.
	@stop

@endif

@if ($locale=='en')

@section('col1')
	<h2>The Task</h2>
	The Servei Català de Trànsit commissioned us with a very special project: a campaign on social networks to mark the World Day of Remembrance for Road Traffic Victims.
	<h2>The goal</h2>
	The aim was to prevent road traffic accidents and to remember their victims. To do that, we had to create a campaign that users could make their own by participating and collaborating in order to make the message viral.
	@stop

	@section('col2')
	<h2>The idea</h2>
	First, we decided to set an example of proper driver behaviour on the road using a gesture that would symbolise the whole campaign. To do this, we used the basic idea of the expression “tener dos dedos de frente” ("have two fingers of forehead", meaning to have common sense) and invited everybody to join in with the campaign by sharing photos of themselves putting two fingers on their forehead. We accompanied this gesture with the slogan “Més seny, més víctimes” ("More common sense, less victims") and the hashtag #johipososeny.
	@stop

	@section('col3')
	<h2>The strategy</h2>
	To launch the campaign, we adopted an influencer marketing strategy involving one hundred celebrities from a wide range of fields, from actors to politicians. We also launched a video on Twitter and Facebook that explained what the campaign was about and how people could join in.
	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'125160884'])

@stop

@section('case')

	<section class="row">
		<div class="col-md-5">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.video-campaign',[], $locale)])

		</div>
		<div class="col-md-offset-1 col-md-6">
			@include('includes.projects.video',['id'=>'121042592'])

		</div>
	</section>

	<hr>

	@include('includes.projects.h3-row',['title'=>Lang::get('project.campaign',[], $locale)])
	<h4>@lang('project.graphic',[], $locale)</h4>

	<section class="row bg-gray">

		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-1.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-2.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-3.jpg', 'alt' => $title . '. ' .$clientsString])
		</div>


	</section>


	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
		<div class="row">
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'users','title'=>'3.500','es'=>'personas participaron a través de las redes sociales en solo 3 días','en'=>'people took part via social networks in just 3 days'])
			</div>
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'voice','title'=>'Publicity','es'=>'Muchos medios se hicieron eco de la iniciativa','en'=>'Several media reported on the initiative'])
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'arrows','title'=>'Viral','es'=>'Convertimos un simple gesto en un símbolo de prevención para evitar accidentes','en'=>'We turned a simple gesture into a symbol for the prevention of accidents'])
			</div>
		</div>

	</section>

@stop
