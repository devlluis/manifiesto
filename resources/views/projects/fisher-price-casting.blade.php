@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	El objetivo de Fisher-Price era lanzar una campaña online que involucrara a las madres con la marca de una manera emotiva.
	<h2>La búsqueda</h2>
	¿Cómo podíamos involucrar a las madres con una campaña online? Ese fue el reto que nos planteamos al empezar el proyecto. Analizamos qué era lo más importante para las madres y qué podía ofrecerles una marca como Fisher-Price.
	@stop

	@section('col2')
	<h2>La idea</h2>
	Lo más especial en la vida de una madre es su hijo. Entonces, ¿cómo podemos conectar a las madres con la marca? <br><br>
	"Convirtiendo a su hijo en alguien especial para la marca"<br><br>
	Y así, dimos con la nueva campaña <br>
	<strong>Fisher-Price "El Casting"</strong>.<br>
	Un casting que pretendía encontrar al bebé protagonista del siguiente spot de la marca y con el que además, regalaría juguetes para toda la vida para el ganador.
	@stop

	@section('col3')
	<br><br>
	La campaña se centraba en 3 fases estratégicas:
	<ol>
		<li><strong>Viralidad de la campaña</strong></li>
		<li><strong>Generar repercusión en medios</strong></li>
		<li><strong>Materialización del casting a través de website/app</strong></li>
	</ol>
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	Fisher-Price's aim was to launch an online campaign that would emotionally involve mothers with the brand.
	<h2>The search</h2>
	How could we involve mothers in an online campaign? That was the challenge we set ourselves at the start of the project. We analysed what was most important for mothers and what a brand like Fisher-Price could offer them.
	@stop

	@section('col2')
	<h2>The idea</h2>
	The most special thing in a mother's life is her child. So, how could we connect mothers with the brand?<br><br>
	"Make your child somebody special to the brand"<br><br>
	And so we came up with the new Fisher-Price campaign: <br>
	<strong>"The Casting".</strong>.<br>
	A casting that sought to find a baby to star in the brand's next commercial, and whereby the winner would get free toys for the rest of their life.
	@stop

	@section('col3')
	<br><br>
	The campaign focused on 3 strategic phases:
	<ol>
		<li><strong>Getting the campaign to go viral</strong></li>
		<li><strong>Generating media interest</strong></li>
		<li><strong>Implementation of the casting via website/app</strong></li>
	</ol>
	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.mainvideo',['id'=>'108263641'])

@stop

@section('case')


	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			@include('includes.projects.h3-row',['title'=>Lang::get('project.insight',[], $locale).'; '.Lang::get('project.video-online',[], $locale)])

			@if ($locale=='es')
			Durante la primera fase del casting el objetivo era conectar para ser virales. Y encontramos un poderoso insight: el amor de mami. A partir de ese insight lanzamos el video viral de que redirigiría al website del casting.
			@else
			During the first phase of the casting, the idea was to connect in order to go viral. And we found a powerful insight: a mother's love. On the basis of this insight, we launched the viral video that linked back to the casting website.
			@endif
			
		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video',['id'=>'76413403'])

		</div>
	</section>

	<hr>

	@include('includes.projects.h3-row',['title'=>Lang::get('project.campaign-online',[], $locale)])

	<section class="row">

		<div class="col-md-12 fadeInUp preanimate">
			<h4>@lang('project.website',[], $locale)</h4>

			@if ($locale=='es')
			Las mamis podían participar en el casting, subiendo sus fotos y vídeos.
			@else
			Mothers could take part in the casting by uploading their photos and videos.
			@endif

			@include('includes.projects.slider',['numPics'=>5])
			
		</div>
	</section>
	<section class="row">

		<div class="col-md-12 fadeInLeft preanimate">
			@include('includes.projects.project-dash')

			<h4>@lang('project.app',[], $locale)</h4>
			@if ($locale=='es')
			Desarrollamos una app para que pudieran participar en el casting directamente desde su móvil.
			@else
			We developed an app for users to take part in the casting directly from mobile devices.
			@endif
			<br><br><br>

		</div>
		<div class="col-md-12 bg-darker-gray fadeInUp preanimate">
				@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'app.jpg','class'=>'img-width-fix'])
			</div>

	</section>

	<i class="project-dash divider"></i>

	<section class="row bg-gray">

		<div class="col-md-6 fadeInLeft preanimate side-padding">
			<h4>@lang('project.interactive-tab',[], $locale)</h4>
			@if ($locale=='es')
			También podían hacerlo a través de la pestaña de Facebook.
			@else
			They could also do so via the Facebook tab.
			@endif
			<br><br>
			@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'interactive-tab.jpg'])
			
			<div class="full-padding">
                <h4>@lang('project.and-also',[], $locale)</h4>

                    <h5>@lang('project.video-banners',[], $locale)</h5>
                    <h5>@lang('project.community',[], $locale)</h5>
                    <h5>@lang('project.facebook-ads',[], $locale)</h5>
                    <h5>@lang('project.online-press-releases',[], $locale)</h5>

            </div>

		</div>

		<div class="col-md-6 fadeInRight preanimate">
			<h4>@lang('project.newsletters',[], $locale)</h4>
			<br/><br/>
            @include('includes.picture' ,['image' => 'newsletter.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])

        </div>

		<div class="col-md-12 fadeInUp preanimate">
		<h4>@lang('project.facebook-tab',[], $locale)</h4>
            @include('includes.picture' ,['image' => 'facebook.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
		</div>

	</section>

	<section class="row row-results">
	
		@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
		
		<div class="row">
			<div class="col-sm-6 col-lg-4">
				@include('includes.projects.results',['icon'=>'users','title'=>'4.664','es'=>'candidatos','en'=>'candidates'])
			</div>
			<div class="col-sm-6 col-lg-4">
				@include('includes.projects.results',['icon'=>'video','title'=>'136','es'=>'vídeos','en'=>'videos'])
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-lg-4">
				@include('includes.projects.results',['icon'=>'picture','title'=>'9.288','es'=>'fotos','en'=>'photos'])
			</div>
			<div class="col-sm-6 col-lg-4">
				@include('includes.projects.results',['icon'=>'check','title'=>'411.543','es'=>'votos registrados','en'=>'registered votes'])
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-lg-4">
				@include('includes.projects.results',['icon'=>'search','title'=>'109.535','es'=>'visualizaciones del viral','en'=>'views of the viral content'])
			</div>
		</div>
	</section>

@stop
