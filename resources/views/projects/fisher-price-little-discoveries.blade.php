@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Fisher-Price quería lanzar una campaña online a nivel internacional para comunicar 14 nuevos productos a través de 14 spots online con un protagonista diferente para cada uno.
	<h2>El objetivo</h2>
	Comunicar los atributos de los 14 productos haciendo protagonistas a los más pequeños y conectar con los padres a través del humor.
	@stop

	@section('col2')
	<h2>La idea</h2>
	La mejor opinión sobre un producto siempre es la de un experto. ¿Y quiénes son los auténticos expertos en juguetes? Los más pequeños de la casa. La solución: lanzar una campaña donde los pequeños expertos opinaran sobre los nuevos productos de la marca captando las reacciones más inesperadas y añadiéndole un toque de humor.

	@stop

	@section('col3')
        <h2>La estrategia</h2>
        Realizamos un casting para encontrar a los 13 pequeños protagonistas y creamos el master de los 14 spots para UK. La campaña se lanzaría con un vídeo teaser para captar la atención y más tarde se publicarían los 14 spots online en 7 países sucesivamente.
        <br/><br/>
        <a class="red" href="http://www.fisher-price.com/es_ES/promotions/littlediscoveries/index.html" target="_blank" rel="noopener">www.fisher-price.com</a>

    @stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
    Fisher-Price wanted to launch an international online campaign to announce 14 new products via 14 online spots with a different protagonist for each.

	<h2>The goal</h2>
    To communicate the attributes of the 14 products, using young children as the protagonists and employing humour to connect with parents.
	@stop

	@section('col2')
	<h2>The idea</h2>
    The best opinion about a product is always that of an expert. And who are the real experts on toys? The little ones at home. The solution: launch a campaign where the little experts will give their opinions about the brand's new products, capturing their most unexpected reactions and adding a touch of humour.


	@stop

	@section('col3')
        <h2>The strategy</h2>
        We held a casting call to find the 13 young protagonists and we created the master for the 14 spots to be seen in the UK. The campaign would be launched with a video teaser to grab the audience's attention, and later the 14 online spots would be posted successively in 7 countries.
        <br/><br/>
        <a class="red" href="http://www.fisher-price.com/en_GB/promotions/littlediscoveries/index.html" target="_blank" rel="noopener">www.fisher-price.com</a>
    @stop

@endif


@section('mainVideo')


@stop

@section('case')

    <video-modal></video-modal>

	<section class="row">
		<div class="col-md-12">
			@include('includes.projects.h3-row',['title'=>Lang::get('project.spots',[], $locale)])
            <div class="video-list-container">
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125883347" title="Smart Stages Puppy"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125883348" title="Rock-a-Stack"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882514" title="Love to Play Puppy"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882513" title="Lion Walker"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882378" title="Kick & Play Piano Gym"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882375" title="Little People School Bus"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882377" title="Space Saver Jumperoo"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882376" title="Musical Activity Gym"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882219" title="Storybook Rhymes Refresh"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882220" title="Storybook Rhymes"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882218" title="Baby's First Blocks"></video-button>
                <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882516" title="Butterfly Dream 3 in 1 Mobile"></video-button>
            </div>

        </div>
	</section>


	@include('includes.projects.h3-row',['title'=>Lang::get('project.making-of',[], $locale).' '.Lang::get('general.and',[], $locale).' '.Lang::get('project.bloopers',[], $locale)])

	<section class="row">

		<div class="col-md-12">
            <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882515" :title="$t('project.making-of')"></video-button>
            <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="125882221" :title="$t('project.bloopers')"></video-button>
            <video-button ps="{{$projectSlug}}" ip="{{$image_path}}" id="126506003" title="Videoclip"></video-button>
		</div>
	</section>

    @include('includes.projects.h3-row',['title'=>Lang::get('project.work-process',[], $locale)])

    <section class="row bg-gray">
		<div class="col-md-12">
            <vue-picture v-for="n in 3" :key="n" project_slug="{{$projectSlug}}" image_path="{{$image_path}}" :image="'img-'+n+'.jpg'" imageclass="fadeInUp preanimate" alt="'{{$title}} , {{$clientsString}}'"></vue-picture>
		</div>
	</section>

	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

    <div class="row">
        <div class="col-sm-6 col-lg-4">
            @include('includes.projects.results',['icon'=>'check','title'=>'7','es'=>'países han adaptado la campaña','en'=>'countries have adapted the campaign'])
        </div>
        <div class="col-sm-6 col-lg-4">
            @include('includes.projects.results',['icon'=>'video','title'=>'30','es'=>'spots publicados','en'=>'spots posted'])
        </div>
        <div class="col-sm-6 col-lg-4 clearfix">
            @include('includes.projects.results',['icon'=>'eye','title'=>'82.622','es'=>'visualizaciones en solo 2 semanas','en'=>' views of the video in only 2 weeks'])
        </div>
    </div>
	</section>

@stop
