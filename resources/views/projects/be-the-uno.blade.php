@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	UNO nos pidió idear una campaña para rejuvenecer el posicionamiento de la marca e introducirla en el entorno digital interactuando con su público objetivo.
	<h2>La estrategia</h2>
	¿Cómo conseguir impactar a los jóvenes con una campaña que genere playability y que se convierta en viral? Haciéndolos protagonistas del juego.
	@stop

	@section('col2')
	<h2>La idea</h2>
	Cuando juegas a UNO y sostienes la última carta en tus manos, esa carta que te hará ganador, te sientes el rey del mundo, te sientes el número UNO. Por eso, planteamos a UNO que todo el mundo pudiera vivir esa sensación en primera persona.<br><br>
	Para ello, creamos una aplicación interactiva para que los fans de UNO en Facebook pudieran vivirlo. La aplicación recogía el nombre y 3 fotografías del usuario y los incrustaba automáticamente en un vídeo interactivo.<br><br>
	@stop

	@section('col3')
	¿El resultado? Un video en el que el usuario era el protagonista, era el número UNO.

	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	UNO asked us to come up with a campaign to rejuvenate the brand's position and get it into the digital environment by interacting with its target audience.
	<h2>The strategy</h2>
	How could we create an impact on young people with a campaign to generate playability and then get it to go viral? By making them the stars of the game!
	@stop

	@section('col2')
	<h2>The idea</h2>
	When you play UNO and are holding the last card in your hand, that is the winning card and you feel like you rule the world, you feel like the numero UNO. So we suggested to UNO that everyone should be able to enjoy that feeling in person.<br><br>
	We created an interactive app that would give UNO's Facebook fans the chance to enjoy that feeling. The app asked for the user's name and 3 photos and automatically embedded them in an interactive video.<br><br>
	@stop

	@section('col3')
	The result? A video featuring the user, where they are the numero UNO.

	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'123739227'])

@stop

@section('case')


	<section class="row bg-gray">
		<div class="col-md-12">
		<h3>@lang('project.banner-campaign',[], $locale)</h3>
		@include('includes.projects.project-dash')

            @include('includes.picture' ,['image' => 'img-1.jpg', 'alt' => $title . '. ' .$clientsString])


        @for ($i = 2; $i <= 9; $i++)
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-'.$i.'.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		@endfor

		<div class="row relative">
			<div class="col-sm-12 col-md-9">
				<h4>@lang('project.facebook-tab',[], $locale)</h4>
                @include('includes.picture' ,['image' => 'facebook.jpg', 'alt' => $title . '. ' .$clientsString])

            </div>
			<div class="col-sm-12 col-md-3">
				<br><br><br>
				<h3>@lang('project.and-also'):</h3>
					<h4>@lang('project.facebook-ads',[], $locale)</h4>
					<h4>@lang('project.newsletters',[], $locale)</h4>
					<h4>@lang('project.community',[], $locale)</h4>
					<h4>@lang('project.press-releases',[], $locale)</h4>

			</div>
		</div>
	</div>
		


	</section>

	
	
	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-sm-6 col-md-4">
			@include('includes.projects.results',['icon'=>'check','title'=>'15.000','es'=>'fans más en Facebook','en'=>'more Facebook fans'])
		</div>
		<div class="col-sm-6 col-md-4">
			@include('includes.projects.results',['icon'=>'users','title'=>'10.000','es'=>'usuarios participaron en el juego','en'=>'players'])
		</div>

	</div>
	
	<div class="row">
		<div class="col-sm-6 col-md-4">
			@include('includes.projects.results',['icon'=>'eye','title'=>'12.000','es'=>'vídeos creados','en'=>'videos created'])
		</div>
		<div class="col-sm-6 col-md-4">
			@include('includes.projects.results',['icon'=>'movie','title'=>'77.000 ','es'=>'impresiones en Facebook','en'=>'Facebook impressions'])
		</div>
	</div>

	</section>

@stop

