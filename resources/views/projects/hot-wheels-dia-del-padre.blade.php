@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Un estudio demostraba que el concepto de familia se ha transformado con los padres millenials y ahora, los papás desarrollan un rol mucho más importante a la hora de educar a sus hijos que antes. Por este motivo, Hot Wheels quería rendir un homenaje a todos los padres para el día del padre a través de un vídeo. 
	<h2>El objetivo</h2>
	Felicitar a todos los padres y rendirles un homenaje muy especial en su día a través de una campaña internacional para Mattel Europa que se adaptó al inglés, francés, italiano, español, alemán, portugués, holandés y turco entre otros.
	@stop

	@section('col2')
	<h2>La idea</h2>
	Muchos niños creen que sus padres no saben cómo divertirse con ellos y quisimos demostrarles que estaban muy equivocados. Para conseguirlo, hicimos un experimento entre padres e hijos reales de distintos países (Alemania, Francia, Inglaterra y España). Una acción de branding basada en una entrevista previa a los niños y en un test que retaba a los padres a demostrar, delante de sus hijos, que sí saben divertirse con ellos. Una vez superados los retos, les esperaba una gran pista de Hot Wheels para que se divirtieran juntos.
	@stop

	@section('col3')
	<h2>La estrategia</h2>
	Realizamos un minucioso trabajo de casting para seleccionar a 4 niños y padres reales de 4 países distintos: alemanes, franceses, españoles e ingleses. Una de las cosas más importantes era que la espontaneidad y la naturalidad primasen en todo el rodaje. Para ello, padres e hijos no supieron hasta el final qué marca estaba detrás del experimento. 
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	A study showed that the concept of the family has changed for fathers in the new millennium, and they now play a much more important role in the education of their children than they did before. That's why Hot Wheels wanted to pay a video tribute to them all on Fathers Days.
	<h2>The goal</h2>
	To congratulate all fathers and pay a very special tribute to them on their special day via an international campaign for Mattel Europa that was adapted to English, French, Italian, Spanish, German, Portuguese, Dutch, Turkish and other languages.
	@stop

	@section('col2')
	<h2>The idea</h2>
	A lot of kids don't think their fathers know how to enjoy themselves like they do and we wanted to show them how very wrong they were. To do so, we performed an experiment with real fathers and sons from different countries (Germany, France, England and Spain). A branding activity based on a preliminary interview with children and a test that challenged fathers to show their sons that they did know how to have fun together. Once the challenges had been completed, a huge Hot Wheels track awaited, where they could have fun together.
	@stop

	@section('col3')
	<h2>The strategy</h2>
	We undertook a meticulous casting process to select the four real fathers and sons from four different countries: Germany, France, Spain and England. One of the most important things was for the whole recording to be spontaneous and natural, so the fathers and sons didn't know what brand was behind the experiment until the very end.
	@stop
	
@endif

@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-campaign',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'122446331'])

@stop

@section('case')
	
	<hr>

    <section class="row ">
        <div class="col-md-5 fadeInLeft preanimate">

            <h3>@lang('project.making-of',[], $locale)</h3>

            @include('includes.projects.project-dash')

        </div>
        <div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
            @include('includes.projects.video',['id'=>'126501323'])
        </div>
    </section>

	<section class="row bg-gray">
		@for ($i = 1; $i <= 8; $i++)
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-'.$i.'.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		@endfor
	</section>
	
	<section class="row row-results">

		@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
		
		<div class="row">
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'video','title'=>'53.283','es'=>'visualizaciones del vídeo en 7 días','en'=>'views of the video in only 7 days'])
			</div>
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'world','title'=>'12','es'=>'países han adaptado la campaña','en'=>'countries have adapted the campaign'])
			</div>
		</div>

	</section>

@stop

