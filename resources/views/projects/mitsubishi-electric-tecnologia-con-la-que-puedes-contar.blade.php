@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Mitsubishi Electric necesitaba una <strong>campaña de reposicionamiento de marca</strong> para sus aires acondicionados. Su estrategia de marketing se centraba en la <strong>durabilidad</strong> y la <strong>fiabilidad</strong> de sus máquinas. Con dos atributos tan racionales, ¿cómo podíamos conectar con el consumidor?
@stop

@section('col2')
    <h2>El concepto </h2>
    Necesitábamos conectar a través de las historias. Por eso, convertimos lo racional en emocional para hablar con nuestro público de algo más que de datos.
@stop

@section('col3')
    <h2>La idea</h2>
    Transformamos la durabilidad en <strong>experiencia</strong>, la fiabilidad en <strong>historias</strong> y Mitsubishi Electric pasó a ser la <strong>"Tecnología con la que puedes contar."</strong>
    <br/>
    A través de las cifras racionales creamos un <strong>key verbal</strong> que nos permitió contar estas historias. Y, por primera vez, le dimos <strong>voz a nuestros aires acondicionados</strong> para que nos explicaran, en primera persona, todo lo que habían vivido en las casas y negocios de nuestros clientes.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Mitsubishi Electric needed a new <strong>brand positioning campaign</strong> for its air conditioning systems. The marketing strategy was focused on the machines <strong>durability</strong> and their <strong>reliability.</strong> But, with these rational atributes, how we could connect with the consumer?
@stop

@section('col2')
    <h2>The concept</h2>
    We needed to connect throught the stories. For that reason, we transform rationality into emotivity, to talk with our target about something else more than just data.
@stop

@section('col3')
    <h2>The idea</h2>
    Durability was transformed into <strong>experience</strong>, fiability into <strong>stories</strong> and Mitsubishi Electric became the <strong>"Technology you can count on."</strong>
    <br/>
    Through rational numbers, we created a <strong>key verbal</strong> that allowed us to explain stories. And, for the first time, we gave <strong>voice to our air conditioning machines</strong> to explain all that experiences they have lived in our customers' homes and businesses.
@stop

@endif


@section('case')

    <section class="row">
        <div class="col-md-12  fadeInLeft preanimate">
            <h3 class="h3-row-video">Claim</h3>
            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-12" style="margin-bottom: 30px; text-align:center;">
            <webm id="gif" class="fadeInUp" source="{{$image_path}}img-1" fallback="{{$image_path}}img-1.gif"></webm>
        </div>
    </section>

    <section class="row">
        <div class="col-md-4 fadeInLeft preanimate">
            <h3>@lang('project.spot-online',[], $locale)</h3>
            @include('includes.projects.project-dash')
            @include('includes.trans',['es'=>'El video fue el punto de partida de una campaña online de pre rolls y VOD.','en' => 'The video was the trigger of an online campaign of pre roll and VOD.'])
        </div>
        <div class="col-md-8 fadeInRight preanimate" style="margin-top: 30px; margin-bottom: 30px;">
            @include('includes.projects.video-yt',['id'=>'EbquRs0x3rs'])
        </div>
    </section>

    <section class="row bg-gray">
        <div class="col-md-12" style="margin-bottom: 60px">
            @include('includes.projects.project-dash')
            <h3>@include('includes.trans',['es'=>'Campaña gráfica','en' => 'Prints'])</h3>
                @include('includes.trans',['es'=>'Adaptamos el concepto creativo para la campaña gráfica y creamos 2 prints: una dirigida a hogares y otra a pymes.','en' => 'We adapted the creative concept to the graphic campaign and we created two prints: one focused on the house environment and the other one, focused on the business world.'])
        </div>

        <div class="col-sm-4 col-sm-offset-1">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-2.jpg','class'=>''])
        </div>

        <div class="col-sm-4 col-sm-offset-2">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-3.jpg','class'=>''])
        </div>

    </section>
    <section class="row">
        <div class="col-md-12" style="margin-bottom: 40px">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-4.jpg','class'=>''])
        </div>
    </section>
    <section class="row bg-gray">
        <div class="col-md-12" style="margin-bottom: 40px">
            @include('includes.projects.project-dash')
            <h3>@include('includes.trans',['es'=>'Campaña PDV','en' => 'POS Campaign'])</h3>
            @include('includes.trans',['es'=>'Y también realizamos una campaña de marketing directo con adhesivos, stoppers, vinilos y tótems que estuvieron en más de 1.500 locales distribuidores.','en' => 'And we also produced a direct marketing campaign with stickers, stoppers, vinils and totems that were in more than 1.500 retail shops.'])
        </div>
        <div class="col-md-12">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-5.jpg','class'=>''])
        </div>
    </section>



    <section class="row row-results" style="margin-top: 0px; padding-top: 0px">

        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'heart','title_es'=>'Un nuevo','es'=>'posicionamiento más emocional','title_en'=>'A new','en'=>'and emotional brand positioning'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'video','title_es'=>'Más de 11.000','es'=>'visualizaciones del vídeo en Youtube','title_en'=>'More than 11.000','en'=>'views in Youtube'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users','title_es'=>'1.500','es'=>'establecimientos llenos de historias','title_en'=>'1.500', 'en'=>'retails full of stories'])
            </div>
        </div>
    </section>

@stop