@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Adam Foods nos encargó la conceptualización y la creación del evento para los ganadores del concurso de dibujo TostaRica Bizcochitos. El premio consistía en un fin de semana en el que los niños y sus familiares visitarían la fábrica de Nutrexpa.
	@stop

	@section('col2')
		<h2>El evento</h2>
		Creamos un evento que fuera inolvidable para los ganadores del concurso. Para ello, construimos una historia mágica y alucinante alrededor de TostaRica Bizcochitos y de su fábrica.
	@stop

	@section('col3')
		<h2>La idea</h2>
		Dimos vida a tres personajes muy carismáticos y divertidos: el guardián de la fábrica, que necesitó la ayuda de los ganadores para recuperar los ingredientes; el ladrón que los robó la noche de antes, y el cocinero mágico, que fue el encargado de mostrar la fábrica a los ganadores con un toque de magia y con mucho entusiasmo. Los niños se divirtieron y aprendieron todo el proceso de producción de los Bizcochitos a la vez que vieron sus propios dibujos en ellos. Para terminar el día, entregamos premios a todos los ganadores, convirtiendo el fin de semana en una gran experiencia.
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	Adam Foods ordered us the conceptualization and creation of the event for the winners of the drawing contest Tosta Rica Bizcochitos. The prize was a weekend in which children and their families would visit the Nutrexpa factory.
	@stop

	@section('col2')
		<h2>The event</h2>
		We created an event that would be unforgettable to the winners of the contest. To do this, we built a magical and amazing history around Tosta Rica Bizcochitos and its factory.
	@stop

	@section('col3')
		<h2>The idea</h2>
		We gave life to three very charismatic and funny characters: the guardian of the factory, which needed the support of the winners to retrieve ingredients; the thief who stole them the night before, and Magic Chef, who was in charge of show factory winners with a touch of magic and lots of enthusiasm. The children had fun and learned all the process of production of the cookies while they looked at their own drawings on them. To finish the day, we delivered the prizes to the winners, making the weekend in a great experience.
	@stop
	
@endif


@section('case')

	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			<h3 class="h3-row-video">{{Lang::get('project.campaign',[], $locale)}}</h3>
			@include('includes.projects.project-dash')

            <h4>@lang('project.video',[], $locale)</h4>

		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video',['id'=>'153900300'])
		</div>
	</section>

	<section class="row bg-gray">
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-1.jpg','class'=>'fadeInUp preanimate no-shadow', 'alt' => $title . '. ' .$clientsString])
            @include('includes.picture' ,['image' => 'gymkana.png','class'=>'fadeIn preanimate no-shadow','id'=>'gymkana', 'alt' => $title . '. ' .$clientsString])
            @include('includes.picture' ,['image' => 'bizcochito-2.png','class'=>'fadeInUp preanimate no-shadow','id'=>'bizcochito-2', 'alt' => $title . '. ' .$clientsString])
            @include('includes.picture' ,['image' => 'bizcochito-1.png','class'=>'fadeIn preanimate no-shadow','id'=>'bizcochito-1', 'alt' => $title . '. ' .$clientsString])
		</div>
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-2.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-3.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-4.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-5.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12" style="margin-top:6%">
            @include('includes.picture' ,['image' => 'img-6.jpg','class'=>'fadeInUp preanimate no-shadow', 'alt' => $title . '. ' .$clientsString])
            @include('includes.picture' ,['image' => 'bizcochito-3.png','class'=>'fadeIn preanimate no-shadow','id'=>'bizcochito-3', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12" style="margin-top:6%">
            @include('includes.picture' ,['image' => 'img-7.png','class'=>'fadeInUp preanimate no-shadow', 'alt' => $title . '. ' .$clientsString])
            @include('includes.picture' ,['image' => 'bizcochito-4.png','class'=>'fadeIn preanimate no-shadow','id'=>'bizcochito-4', 'alt' => $title . '. ' .$clientsString])
		</div>
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-8.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-9.jpg', 'alt' => $title . '. ' .$clientsString])
        </div>
	</section>

	<section class="row row-results">
	
	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
    <div class="row">
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'users','title'=>'13','es'=>'ganadores alucinaron con la experiencia','en'=>'winners hallucinated with the experience'])
        </div>
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'compass','title'=>'1','es'=>'evento fiel a la filosofía de marca: alimentar la imaginación de los más pequeños','en'=>'event fell to the brand philosophy: feed the imagination of children'])
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'home','title'=>'1','es'=>'fin de semana lleno de imaginación, dibujo y mucha ilusión','en'=>'week-end full of imagination, drawing and illusion'])
        </div>
    </div>

	</section>

@stop
