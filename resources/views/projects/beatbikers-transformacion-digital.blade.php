@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    El reto consistió en la transformación digital de una tienda de accesorios de motos que fuese capaz de vender de manera online. Es decir, dar el gran salto del off al mundo digital. 
@stop

@section('col2')
    <h2>El concepto </h2>
    El desarrollo de la e-commerce fue la piedra angular del proyecto, creamos un nuevo canal de venta donde la facilidad en el proceso de compra y el diseño, totalmente enfocado a un público motero, fueron nuestra prioridad. 
@stop

@section('col3')
    <h2>La idea</h2>
    A través del restyling de marca y de la creación de la e-commerce, construimos un plan de medios digitales ambicioso: Google Adwords & Shopping, campañas display y RTB's consiguieron aumentar las conversiones con un ROI en constante crecimiento. 
    <br><br>
    Las Redes Sociales, además de potenciar el tráfico web, han sido el canal ideal para crear un espacio de interacción en el que conectar con los clientes. 
    <br><br>
    Por ello, los perfiles sociales de Beat Bikers transmiten la pasión por las motos y recojen las impresiones de los seguidores. 
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    The challenge was the digital transformation of accesory store able to sell online. In other words, a change from off to online scenario.
@stop

@section('col2')
    <h2>The concept</h2>
    The development of e-commerce was the cornerstone of the project, we created a new sales channel where the ease in the purchasing process and the design, totally focused on a rider public, were our priority.
@stop

@section('col3')
    <h2>The idea</h2>
    Through brand restyling and the creation of e-commerce, we built an ambitious digital media plan: Google Adwords & Shopping, display campaigns and RTBs were able to increase conversions with a constantly growing ROI. 
    <br><br>
    Social Media, in addition to promoting web traffic, have been the ideal channel to create a space for interaction in which to connect with customers. 
    <br><br>
    Therefore, the social profiles of Beat Bikers convey the passion for motorcycles and collect the impressions of the followers.
@stop

@endif


@section('case')

    <section class="row container-full">   
            <div class="col-md-12" style="padding: 0px 5%;">
                <h3 style="margin-top:0px; padding-top:30px;">{{Lang::get('project.branding',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
            </div>
            <div class="col-md-12" style="padding-left:0px; padding-right:0px; padding-top: 20px;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1-2.jpg','class'=>''])
            </div>
            <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-3.jpg','class'=>''])
            </div>
            <div class="col-md-6" style="padding-left:0px; padding-right:0px;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-4.jpg','class'=>''])
            </div>
            <div class="col-md-12" style="padding: 1% 0px;">
                <webm class="fadeInUp preanimate " source="{{$image_path}}paleta" fallback="{{$image_path}}paleta.gif"></webm>
            </div>
            <div class="col-md-12 gif-logo-bb">
                <webm class="fadeInUp preanimate" source="{{$image_path}}logo" fallback="{{$image_path}}logo.gif"></webm>
            </div>
            <div class="col-md-12 gif-text-bb">
                <webm class="fadeInUp preanimate" source="{{$image_path}}texto" fallback="{{$image_path}}texto.gif"></webm>
            </div>
            <div class="col-md-12" style="padding: 20px 5%; background: #F5F6F8;">
                <h3 style="margin-top:0px; padding-top:30px;">{{Lang::get('project.e-commerce',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
            </div>
            <div class="col-md-12 gif-web-bb">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'web.gif','class'=>''])
            </div>
            <div class="col-md-12 gif-devices-bb">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'ipad.png','class'=>'fadeInUp preanimate ipad'])
                <webm class="fadeInUp preanimate ipad-gif" source="{{$image_path}}ipad" fallback="{{$image_path}}ipad.gif"></webm>
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'iphone.png','class'=>'fadeInUp preanimate iphone'])
                <webm class="fadeInUp preanimate iphone-gif" source="{{$image_path}}iphone" fallback="{{$image_path}}iphone.gif"></webm>
            </div>
            <div class="col-md-12" style="padding: 40% 5% 20px 5%; background: #F5F6F8;">
                <h3 style="margin-top:0px; padding-top:30px;">{{Lang::get('project.social',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
            </div>
            <div class="col-md-12 gif-facebook-bb">
                <webm class="fadeInUp preanimate" source="{{$image_path}}facebook" fallback="{{$image_path}}facebook.gif"></webm>
            </div>
    </section>

    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'like','title_es'=>'50.000','es'=>'nuevos usuarios mensuales en redes sociales','title_en'=>'50.000','en'=>'new monthly users on social networks'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'comments','title_es'=>'100%','es'=>'de comentarios respondidos','title_en'=>'100%','en'=>'responded comment'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'clock','title_es'=>'< 1 HORA','es'=>'tiempo de respuesta','title_en'=>'< 1 HOUR', 'en'=>'response time'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'sessions','title_es'=>'+ 200.000','es'=>'sesiones anuales','title_en'=>'+ 200.000', 'en'=>'annual sessions'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users','title_es'=>'+ 125.000','es'=>'nuevos usuarios anuales','title_en'=>'+ 125.000', 'en'=>'new annual users'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'user','title_es'=>'+ 75%','es'=>'de usuarios retenidos','title_en'=>'+ 75%', 'en'=>'retained users'])
            </div>
        </div>
    </section>

@stop
