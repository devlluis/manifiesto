@extends('layouts.project')


@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Danone nos encargó el desarrollo de la campaña de lanzamiento de su primer yogur enfocado al canal profesional; Danone Pro.

	<h2>El objetivo</h2>
	Generar notoriedad e impacto sobre el nuevo Danone Pro y activar promociones y herramientas específicas para el canal profesional.
	@stop

	@section('col2')
	<h2>La estrategia</h2>
	Para el lanzamiento del nuevo Danone Pro, se optó por crear una nueva categoría de postres para inspirar a hosteleros y chefs a crear recetas dulces y saladas: los Delis de yogur.
	@stop

	@section('col3')
		<h2>La idea</h2>
		Primero, definimos el universo Danone Pro a través de key visuals y key verbals y creamos diferentes herramientas como el spot, la web de marca, material de punto de venta, un recetario de Delis de yogur, etc. Para el lanzamiento realizamos una gran campaña de comunicación que incluía un evento de presentación en el Fòrum Gastronòmic y la activación de una promoción especial en el punto de venta.
	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	Danone asked us the development of the campaign's launch of its first yogurt focused to the professional channel; Danone Pro.

	<h2>The goal</h2>
	Generate visibility and impact on the new Danone Pro and activate promotions and specific tools for the professional channel.
	@stop

	@section('col2')
	<h2>The strategy</h2>
	For the launch of the new Danone Pro, we decided to create a new category of desserts to inspire restaurateurs and chefs to create sweet and salty recipes: Delis of yogurt.
	@stop

	@section('col3')
		<h2>The idea</h2>
		Firstly, we define the Danone Pro universe through verbals key and key visuals and create different tools such as spot, the brand website, point of sale material, a cookbook of Delis of yogurt, etc. For the launch we made a great communications campaign that included a presentation event in the Fòrum Gastronòmic and the activation of a special promotion at the point of sale.
	@stop
	
@endif


@section('case')


	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			<h3 class="h3-row-video">{{Lang::get('project.videocase',[], $locale)}}</h3>
			@include('includes.projects.project-dash')
			
		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video',['id'=>'154283863'])

		</div>
	</section>


	<section class="row bg-gray" style="position:relative">

		<div class="col-md-12">
			<h3>Web <a href="http://danonepro.es" target="_blank" rel="noopener"><span class="site-link"><i class="fa fa-external-link"></i></span></a></h3>
			@include('includes.projects.project-dash')
		</div>
		<div class="col-md-12">
        @include('includes.picture' ,['image' => 'img-1.jpg','class'=>'fadeInUp preanimate no-shadow'])
		<webm id="danone-gif-1" class="fadeInUp preanimate no-shadow" source="{{$image_path}}gif-1" fallback="{{$image_path}}gif-1.gif"></webm>


        </div>
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-2.jpg','class'=>'fadeIn preanimate no-shadow', 'alt' => $title . ', ' .$clientsString])
        </div>

        @include('includes.picture' ,['image' => 'lacasitos.png','class'=>'fadeIn preanimate no-shadow','id'=>'lacasitos', 'alt' => $title . ', ' .$clientsString])

        <div class="col-md-12">
			@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-3.jpg','class'=>''])
		</div>

		<div class="col-md-12">
		    <h3>@include('includes.trans',['es'=>'Prensa / Carta de Delis / Recetario','en' => 'Press / Deli menu / Recipe book'])</h3>
			@include('includes.projects.project-dash')

			@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-4.jpg','class'=>''])
			

            <h3>Stand</h3>
            @include('includes.projects.project-dash')
			@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-5.jpg','class'=>''])
			
		</div>


		<div class="col-md-12">
		
			<h3>@include('includes.trans',['es'=>'Y además:','en'=>'And much more:'])</h3>

			@include('includes.projects.project-dash')
		
			<h4>@include('includes.trans',['es'=>'Materiales para punto de venta','en'=>'Point of sale materials'])</h4>
			<h4>@include('includes.trans',['es'=>'Campaña online','en'=>'Online Campaign'])</h4>
			<h4>@include('includes.trans',['es'=>'Brand Book Danone Pro & Deli','en'=>'Danone Pro & Deli Brand Book'])</h4>
			<h4>@include('includes.trans',['es'=>'Ferias y Congresos Gastronómicos','en'=>'Fairs and Gastronomic Congresses'])</h4>
			<h4>@include('includes.trans',['es'=>'Promociones','en'=>'Promotions'])</h4>
		</div>
	</section>





	<section class="row row-results">
	
	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'eye','title'=>'149.000','es'=>'inserciones en medios','en'=>'inserts in media'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'check','title'=>'29','es'=>'recetas diferentes con Danone Pro','en'=>'different recipes with Danone Pro'])
		</div>
	</div>
	</section>

@stop