@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Scrabble quería lanzar una campaña de marca que estableciera conversaciones con su público y los implicara en una causa social.
    <h2>La búsqueda</h2>
    ¿Cómo conectar a Scrabble con una causa social? Muy fácil: aprovechando algo tan sencillo y poderoso como las palabras para ayudar al desarrollo cognitivo de los niños y niñas en situación de vulnerabilidad a través de la educación. Este era nuestro reto.
@stop

@section('col2')
    <h2>La idea</h2>
    La idea era muy sencilla: demostrar El Poder de las Palabras.  Y con este mensaje, lanzamos una campaña junto a Aldeas Infantiles.
    <br>
    Creamos un site donde explicamos el sueño de Scrabble y trasladamos el juego de mesa a la web para jugar online. Al acabar la partida, el usuario podía donar los puntos conseguidos con sus palabras a Aldeas Infantiles y Scrabble los convertía en una donación real para el proyecto. El reto era conseguir 1.000.000 de puntos donados.
@stop

@section('col3')

@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The Task</h2>
    Scrabble wanted to launch a branded campaign to generate conversations with its audience and get them to support a good cause.
    <h2>The search</h2>
    How could Scrabble connect with a charity? Very easily: by taking advantage of something so simple yet powerful as words to help with the cognitive development of boys and girls in vulnerable situations through education. This was our challenge.
@stop

@section('col2')
    <h2>The idea</h2>
    The idea was very simple: to show The Power of Words. And using that message, we launched a campaign in association with Children's Villages.
    <br>
    We created a site where we introduced Scrabble's dream and transferred the board game to the web for online play. At the end of the game, users could donate the points they had won with their words to Children's Villages and Scrabble turned them into a real donation to the project. The aim was to reach 1,000,000 donated points.
@stop

@section('col3')

@stop

@endif


@section('mainVideo')

    @include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

    @include('includes.projects.mainvideo',['id'=>'108263747'])

@stop

@section('case')

    {{--
    <section class="row">
        <div class="col-md-5">
        @include('includes.projects.h3-row',['title'=>Lang::get('project.video-online',[], $locale)])

        </div>
        <div class="col-md-offset-1 col-md-6">
            @include('includes.projects.video',['id'=>''])

        </div>
    </section>
    --}}

    <hr>

    <section class="row">
        <div class="col-md-12">
            @include('includes.projects.h3-row',['title'=>Lang::get('project.online-campaign',[], $locale)])
        </div>

        <div class="col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10">

            @include('includes.projects.slider',['numPics'=>4])

        </div>
    </section>

    <section class="row">
        @include('includes.projects.project-dash')
    </section>

    <section class="row bg-gray">
        <div class="col-md-12 col-lg-6">
            <h4>@lang('project.newsletters',[], $locale)</h4>
            @include('includes.picture' ,['image' => 'newsletter.jpg', 'alt' => $title . '. ' .$clientsString])

        </div>
        <div class="col-md-12 col-lg-6 side-padding">
            <h4 class="side-padding">@lang('project.banner-campaign',[], $locale)</h4>
            @for ($i = 1; $i <= 6; $i++)
                @include('includes.picture' ,['image' => 'banner-'.$i.'.jpg', 'alt' => $title . '. ' .$clientsString])
            @endfor
        </div>


    </section>


    <section class="row row-results">

        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-4">
                @include('includes.projects.results',['icon'=>'video','title'=>'38.400','es'=>'visitas a la web de España','en'=>'visits to the Spanish website'])
            </div>
            <div class="col-md-4">
                @include('includes.projects.results',['icon'=>'eye','title'=>'32.488','es'=>'visitas a la app del juego','en'=>'visits to the game app'])
            </div>
            <div class="col-md-4">
                @include('includes.projects.results',['icon'=>'letter','title'=>'6.720','es'=>'partidas de Scrabble','en'=>'games of Scrabble'])
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                @include('includes.projects.results',['icon'=>'clock','title'=>'10','es'=>'minutos de media en el juego','en'=>'minutes of playing time on average'])
            </div>
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'check','title'=>'4.757.661 ','es'=>'puntos donados a Aldeas Infantiles','en'=>'points donated to Children\'s Villages.'])
            </div>
        </div>

    </section>

@stop
