@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
Reebok nos encargó una campaña para lanzar su nueva colección Crafted by Fitness, equipación deportiva que se adapta a todo tipo de cuerpos y entrenamientos. 
@stop

@section('col2')
    <h2>El concepto</h2>
    Si no existen dos cuerpos iguales, ¿por qué los maniquíes sí lo son?
    <br><br>
    Lanzamos Reebok Bodies Crafted by Fitness. Una campaña en la que rompemos los moldes establecidos para crear los primeros maniquíes Reebok basados en cuerpos reales.
@stop

@section('col3')
    <h2>La idea</h2>
   Presentamos la campaña con una brand film protagonizada por dos influencers de Reebok: Patry Jordán y Javier Fernández. En ella, explicamos la idea y animamos a la comunidad Reebok a participar para ser los primeros Reebok Bodies.
   <br><br>
    Y después de un scanner 3D y muchas horas de trabajo, lo conseguimos: la colección Crafted by Fitness se presenta en los maniquíes más humanos de Reebok."
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Reebok requested a campaign to launch their new collection Crafted by Fitness, an sports equipment that adapts to all types of bodies and training sessions.
@stop

@section('col2')
    <h2>The concept</h2>
    If there are no two equal bodies, why the mannequins are?
    <br><br>
    We launched Reebok Bodies Crafted by Fitness. A campaign in which we break the established molds to create the first Reebok mannequins based on real bodies.
@stop

@section('col3')
    <h2>The idea</h2>
    We present the campaign with a brand film starring two Reebok influencers: Patry Jordán and Javier Fernández. In it, we explained the idea and encouraged the Reebok community to participate to become the first Reebok Bodies.
    <br><br>
And after a 3D scanner and many hours of work, we did it: the Crafted by Fitness collection was presented on Reebok's most human mannequins.
@stop

@endif


@section('case')



    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'video','title_es'=>'4 millones','es'=>'de VIEWS','title_en'=>'4 million','en'=>'VIEWS'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users-play','title_es'=>'9 millones','es'=>'de usuarios únicos (REACH)','title_en'=>'9 million','en'=>'unique users (REACH)'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'engagement','title_es'=>'12%','es'=>'tasa de engagement','title_en'=>'12%', 'en'=>'engagement rate'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'user','title_es'=>'4 maniquíes','es'=>'con nombre y apellido','title_en'=>'4 mannequins', 'en'=>'with name and surname'])
            </div>
        </div>
    </section>

@stop
