@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Bollycao nos encargó idear la última promoción de la marca: una colección de cromos sobre los clásicos del cine.
	<h2>La búsqueda</h2>
	Bollycao ha marcado una generación de coleccionistas gracias a sus promociones y en Manifiesto teníamos el reto de seguir manteniendo vivo este interés con una promoción única. Para hacerlo, decidimos hacer realidad algo que los seguidores de la marca llevaban pidiendo durante años.
	@stop

	@section('col2')
	<h2>La idea</h2>
	¿Quién podía ser el protagonista de nuestra promoción? Solo un personaje era capaz de representar a todos los clásicos del cine de una manera cómica y divertida, y decidimos revivirlo: ¡TOI volvía a Bollycao!<br><br>
	Así, se crearon 40 cromos diferentes en los que TOI daba vida a una escena mítica de la historia del cine, dimos nombre a la promoción: TOI DE CINE, y creamos una web basada en el mundo del cine donde se podían ganar un montón de premios diarios gracias a los códigos de los cromos.
	@stop

	@section('col3')


	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	Bollycao asked us to come up with the idea for the brand's latest promotion: a sticker collection on the theme of movie classics.
	<h2>The search</h2>
	Bollycao has created a whole generation of collectors thanks to its promotions and at Manifiesto our challenge was to keep this interest alive in the form of a unique promotion. To do so, we decided to bring someone back to life, which was something the brand's followers had been asking for for years.
	@stop

	@section('col2')
	<h2>The idea</h2>
	Who could be the star of our promotion? Only one character was able to represent all the movie classics in a comically fun way, and we decided to bring him back to life: ¡TOI was coming back to Bollycao!<br><br>
	So we created 40 different stickers in which TOI appeared in legendary scenes from cinema history. The name we chose for the promotion was: TOI DE CINE ('Movie Toi'), and we created a website based on the movie world where loads of prizes could be won every day using the codes on the stickers.
	@stop

	@section('col3')

	@stop
	
@endif


@section('mainVideo')

	@include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

	@include('includes.projects.mainvideo',['id'=>'123723799'])

@stop

@section('case')

	
	<section class="row">
		<div class="col-md-5">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.spot-online',[], $locale)])

		@if ($locale=='es')
		Un vídeo donde se presentaba a los nuevos cromos de TOI
		@else
		A video introducing the new TOI stickers
		@endif

		</div>
		<div class="col-md-offset-1 col-md-6">
			@include('includes.projects.video',['id'=>'121340876'])

		</div>
	</section>
	
	<hr>

	<section class="row">
		<div class="col-md-12">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.online-campaign',[], $locale)])
		
		@if ($locale=='es')
		Los usuarios podían registrarse y ganar premios
		@else
		A fun game that involved throwing popcorn into Toi's mouth
		@endif

		</div>

		<div class="col-sm-offset-0 col-sm-12 col-md-offset-1 col-md-10">
		
		@include('includes.projects.slider',['numPics'=>3])
			
		</div>
	</section>
	
	<section class="row">
		<div class="col-md-12">
		@include('includes.projects.project-dash')

		<h2>@lang('project.advergaming',[], $locale)</h2>

		@if ($locale=='es')
		Un divertido juego en el que se tenía que lanzar palomitas a la boca de Toi
		@else
		A fun game that involved throwing popcorn into Toi's mouth
		@endif

		<br><br>

		</div>

		<div class="col-md-12 bg-darker-gray">
            @include('includes.picture' ,['image' => 'mobile.jpg','class'=>'fadeInUp preanimate img-width-fix','alt' => $title . '. ' .$clientsString])
        </div>

	</section>
	<section>
		@include('includes.projects.project-dash')
	</section>

	<section class="row bg-gray">
		<div class="col-md-12 col-lg-7">
			<h4>@lang('project.facebook-tab')</h4>
            @include('includes.picture' ,['image' => 'facebook.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])

        </div>
		<div class="col-md-12 col-lg-5">
			<h4>@lang('project.newsletters')</h4>
            @include('includes.picture' ,['image' => 'newsletter.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
		</div>
		<div class="col-md-12">
		<h4>@lang('project.community')</h4>
            @include('includes.picture' ,['image' => 'community.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])

            <div class="full-padding">
                @lang('project.and-also',[], $locale):
                <ul class="list-also">
                    <li>@lang('project.banner-campaign',[], $locale)</li>
                    <li>@lang('project.community',[], $locale)</li>
                    <li>@lang('project.press-releases',[], $locale)</li>
                </ul>
            </div>
		</div>

	</section>
	
	
	<section class="row row-results">

		@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
		<div class="row">
			<div class="col-md-4">
				@include('includes.projects.results',['icon'=>'users','title'=>'4.370','es'=>'usuarios registrados en la web','en'=>'registered users of the website'])
			</div>
			<div class="col-md-4">
				@include('includes.projects.results',['icon'=>'clock','title'=>'9','es'=>'minutos en el site de media','en'=>'minutes on the site on average'])
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-4">
				@include('includes.projects.results',['icon'=>'gift','title'=>'6.800','es'=>'premios repartidos','en'=>'prizes given out'])
			</div>
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'heart','title'=>'10.000','es'=>'nuevos fans en Facebook','en'=>'new Facebook fans'])
			</div>
		</div>

	</section>

@stop
