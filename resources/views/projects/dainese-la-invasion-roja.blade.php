@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Dainese quería dar a conocer a la gente de Madrid la apertura de su nueva tienda Dainese D-Store Madrid. Para ello, nos pidió la realización de una campaña. 
	<h2>El objetivo</h2>
	Captar la atención de la gente para dirigir tráfico a la tienda el día de la inauguración. Para conseguirlo, creamos una campaña ad hoc.
	@stop

	@section('col2')
	<h2>La idea</h2>
	Aprovechando que el logotipo de Dainese tiene una forma muy característica y puede parecer que es un ser venido de otro planeta, decidimos invadir Madrid de forma online. Creamos un advergame donde, a través de Google Street View, los usuarios debían capturar a todos los invasores que se encontraran por el camino con el objetivo de canjearlos por increíbles premios; cascos, guantes, chaquetas…incluso un traje hecho a medida. ¡Cuantos más invasores capturaran, mayor era el premio que conseguían! Todos aquellos que quisieran su premio, debían acudir a la Dainese D-Store Madrid el día del Opening Day, es decir, el día de la inauguración. 
	@stop

	@section('col3')
	<h2>La estrategia</h2>
	Para lanzar la campaña, apostamos por centralizarlo todo en Facebook. Era una buena manera de conseguir fans y darnos a conocer, así que el advergame solo funcionaba a través de Facebook Connect. Además, también nos pusimos en contacto con influencers del sector motor para que nos ayudaran a comunicarla.<br><br>

	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	Dainese wanted to tell everyone in Madrid that they were opening the new Dainese D-Store Madrid. They asked us to organise a campaign for them to do so.
	<h2>The goal</h2>
	Get people's attention to generate traffic to the store on its opening day. To do this, we created an ad hoc campaign.
	@stop

	@section('col2')
	<h2>The idea</h2>
	Exploiting the fact that Dainese has a highly characteristic logo that could be seen as an extra-terrestrial being, we decided on an online invasion of Madrid. We created an advergame that used Google Street View, in which users had to capture all the invaders in their path and then cash them in for incredible prizes; helmets, gloves, jackets and even a made-to-measure suit. The more invaders they captured, the better the prize! Everyone who wanted a prize had to come to the Dainese D-Store Madrid on the Opening Day.
	@stop

	@section('col3')
	<h2>The strategy</h2>
	To launch the campaign, we decided to centralise everything on Facebook. It was a good way to collect fans and get ourselves noticed, so the advergame only worked via Facebook Connect. We also contacted influencers from the motor sector, who helped us to get our message across.
	
	@stop
	
@endif


@section('mainVideo')
    @include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

    @include('includes.projects.mainvideo',['id'=>'127925383'])
@stop

@section('case')

	

	<section class="row fadeInUp preanimate">
		<div class="col-md-12">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.online-campaign',[], $locale)])

		<h4>@lang('project.microsite',[], $locale)</h4>

            @include('includes.picture' ,['image' => 'microsite.jpg','class'=>'img-width-fix', 'alt' => $title . ', ' .$clientsString])

		</div>

	</section>
	
	<section class="row fadeInUp preanimate">
		<div class="col-md-12">

			<h4>@lang('project.advergaming',[], $locale)</h4>

			<div class="col-sm-offset-0 col-sm-12 col-md-offset-0 col-md-12">
			
			@include('includes.projects.slider',['numPics'=>6])
				
			</div>
		</div>
	</section>

	<section class="row">
		<div class="col-md-12 col-lg-7 fadeInLeft preanimate">
			<h4>@lang('project.and-also',[], $locale)</h4><br><br>
		</div>
		<div class="col-md-6 fadeInLeft preanimate">
			<h5 class="is-size-3">@lang('project.banners',[], $locale)</h5>
				@if ($locale=='es')
				Para activar la campaña, creamos banners especiales para webs del sector motor.
				@else
				To activate the campaign, we created special banners for websites in the motor sector.
				@endif
			<br><br>
			<h5 class="is-size-3">@lang('project.community',[], $locale)</h5>
				@if ($locale=='es')
				Creamos una planificación estratégica de Community Management basada en publicaciones que aportaban información exclusiva para mejorar y fomentar la participación de los usuarios en La Invasión Roja.
				@else
				We developed Community Management strategic planning based on posts that provided exclusive information to improve and encourage user's participation in the Red Invasion.
				@endif
			<br><br>
			<h5 class="is-size-3">@lang('project.facebook-ads',[], $locale)</h5>
				@if ($locale=='es')
				Apostamos por una fuerte campaña segmentada en Facebook ADS para incentivar la participación a La Invasión Roja y la asistencia al Opening Day.
				@else
				We opted for a highly segmented campaign on Facebook ADS to encourage participation in the Red Invasion and attendance of the Opening Day.
				@endif
			<br><br>
		</div>
		<div class="col-md-6 fadeInRight preanimate">
			<h5 class="is-size-3">@lang('project.influencers',[], $locale)</h5>
				@if ($locale=='es')
				Contactamos con algunos de los pilotos de MotoGP más importantes para que se uniesen a la campaña.
				@else
				We contacted some of the most famous Moto GP riders and got them to join the campaign.
				@endif
			<br><br>
			<h5 class="is-size-3">@lang('project.online-press-releases',[], $locale)</h5>
				@if ($locale=='es')
				Realizamos el envío de notas de prensa a los medios más importantes del sector motor y a las principales asociaciones de moteros afincadas en Madrid.
				@else
				We sent press releases to the most important media in the motor sector and the main motor associations based in Madrid.
				@endif
		</div>

	</section>
	
	
	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
		<div class="row">
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'check','title'=>'1.300','es'=>'nuevos fans en Facebook en solo una semana','en'=>'new Facebook fans in just one week'])
			</div>
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'users','title'=>'1000','es'=>'personas visitaron la tienda durante el Opening Day','en'=>'people came to the store on Opening Day'])
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'gift','title'=>'309','es'=>'premios repartidos','en'=>'prizes given out'])
			</div>
			<div class="col-md-6">
				@include('includes.projects.results',['icon'=>'arrows','title'=>'789','es'=>'personas participaron en La Invasión Roja durante los 6 días que duró la campaña','en'=>'people took part in the Red Invasion during the six days of the campaign'])
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				@include('includes.projects.results',['icon'=>'voice','title'=>'Publicity','es'=>'Gran repercusión en los medios más importantes del sector motor que se hicieron eco de la campaña de La Invasión Roja y del Opening Day','en'=>'Major impact in the most important media in the motor sector in the form of coverage of the Red Invasion and the Opening Day'])
			</div>
		</div>
	

	</section>

@stop
