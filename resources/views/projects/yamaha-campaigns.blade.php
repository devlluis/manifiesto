@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>Necesidades de la marca</h2>
	Una marca como Yamaha necesita comunicar conceptos muy específicos en cada una de sus piezas.<br><br>
	En Manifiesto nos hemos adaptado para crear campañas agresivas, potentes y prácticas, con conceptos basados en el respeto por el medio ambiente, pasión por los productos Yamaha o en las aventuras que se viven con esta marca.
	@stop

	@section('col2')
	<h2>&nbsp;</h2>
	Hemos convertido promociones en grandes campañas, hemos comunicado para todo tipo de públicos y hemos presentado a Yamaha como la líder del mercado.
	@stop

	@section('col3')

	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>Brand needs</h2>
	A brand like Yamaha needs to get very specific concepts across regarding each of its pieces.<br><br>
	At Manifiesto we have adapted in order to create aggressive, powerful and practical campaigns, using concepts based on respect for the environment, passion for Yamaha products and the adventures that people have with this brand.
	@stop

	@section('col2')
	<h2>&nbsp;</h2>
	We have turned promotions into large-scale campaigns, we have communicated with all kinds of audiences and we have presented Yamaha as the market leader.

	@stop

	@section('col3')

	@stop
	
@endif


@section('mainVideo')


	@include('includes.projects.mainvideo',['id'=>'110978817'])

@stop

@section('case')

	<hr>
	
	<section class="row bg-gray">
		<div class="col-sm-offset-0 col-sm-12 col-lg-offset-1 col-lg-10">
			@include('includes.projects.slider',['numPics'=>7])
		</div>
	</section>

@stop
