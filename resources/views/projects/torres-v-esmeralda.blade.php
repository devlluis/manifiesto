@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Crear un video único y exclusivo que mostrara el nuevo reposicionamiento de Viña Esmeralda: "The Mediterranean Classic" en el que el glamour, la feminidad, el sabor mediterráneo y el carácter global son los protagonistas.
@stop

@section('col2')
    <h2>El concepto</h2>
    Transmitir el nuevo posicionamiento de marca a través del momento de consumo: el atardecer. ""It's time to shine!"" es el concepto que giraba en torno al video, y nos habla del momento mágico en el que los últimos rayos de sol se reflejan sobre el mar y dan paso a la noche para que todo lo demás brille con luz propia.
@stop

@section('col3')
    <h2>La idea</h2>
    Captar los valores de Viña Esmeralda a través de un evento que se celebró en Sitges. Una ciudad glamurosa, con historia y con mucho carácter mediterráneo.
    <br/>
    El video recoge los mejores instantes del evento en los que se siente de la forma más pura la esencia del Mediterranean Classic.

@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    To create a unique and exclusive video that shows the new repositioning of Viña Esmeralda: "The Mediterranean Classic" in which glamour, femininity, Mediterranean flavor and global character are the main actors.

@stop

@section('col2')
    <h2>The concept</h2>
    To transmit the new brand positioning through the moment of consumption: the sunset. "It's time to shine!" Is the concept that revolved around the video, and shows us the magic moment in which the last sun's rays reflect on the sea and give way to the night so that everything else shines with its own light.
@stop

@section('col3')
    <h2>The idea</h2>
    Capture the values of Viña Esmeralda through an event that took place in Sitges. A glamorous city, with history and a lot of Mediterranean character.
    <br/>
    The video gathers the best moments of the event in which the essence of the Mediterranean Classic is shown in the purest form.
@stop

@endif

@section('mainVideo')


    @include('includes.projects.h3-row',['title'=>Lang::get('project.spot-online',[], $locale)])

    @include('includes.projects.mainvideo-yt',['id'=>'TJRUFYTRMpg'])


@stop


@section('case')

    <section class="row bg-gray">

        @for ($i = 1; $i <= 8; $i++)
            <div class="col-md-6">
                @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
            </div>
        @endfor

    </section>

    <section class="row row-results">

        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-12">
                @include('includes.projects.results',['icon'=>'video','title_es'=>'1 spot online','es'=>'online que se convirtió en la carta de presentación del nuevo reposicionamiento de Viña Esmeralda así como de sus valores','title_en'=>'1 online spot','en'=>'that became the cover letter of the new repositioning of Viña Esmeralda.'])
            </div>
        </div>
    </section>

@stop