@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	Para el lanzamiento de su nueva película, Barbie nos pidió idear y organizar el evento de presentación para las fans de la marca.
	<h2>La organización</h2>
	Para que las niñas viviesen una jornada de película, teníamos que encontrar el lugar ideal para que la experiencia fuese única. Y lo encontramos. ¿Dónde se une la magia del mar con la diversión para toda la familia? En el Aquarium de Barcelona. El entorno ideal para presentar una película protagonizada por una sirena. Después, solo teníamos que dejar que nuestra magia hiciera todo lo demás.
	@stop

	@section('col2')
	<h2>El concurso</h2>
	Para que el evento fuera aún más especial, teníamos que convertirlo en un acontecimiento exclusivo. Para ello, organizamos un concurso en el que las niñas tenían que responder a una sencilla pregunta para participar. Las ganadoras conseguían entradas para el evento.
	@stop

	@section('col3')


	@stop

@endif

@if ($locale=='en')

	@section('col1')
	<h2>The Task</h2>
	To launch their new movie, Barbie asked us to conceive and organise an event to present it to the brand's fans.
	<h2>Organisation</h2>
	To offer a wonderful day for girls to enjoy, we had to find the ideal place to make the experience unique. And we found it. Where does the magic of the sea combine with fun for all the family? At Barcelona Aquarium. The ideal setting to present a movie starring a mermaid. All we had to do after that was get our magic to do the rest.
	@stop

	@section('col2')
	<h2>The competition</h2>
	To make the event even more special, we had to make it exclusive. To do that, we organised a competition in which girls had to answer a simple question in order to enter. The winners got tickets for the event.
	@stop

	@section('col3')

	@stop
	
@endif


@section('mainVideo')

	{{--@include('includes.projects.mainvideo',['id'=>'92631642'])--}}
	@include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1.jpg','class'=>'mainImg'])



@stop

@section('case')

	<section class="row">
		<div class="col-md-5">
			@include('includes.projects.h3-row',['title'=>Lang::get('project.event',[], $locale)])

			@if ($locale=='es')
				El día del evento todo tenía que representar la magia de la película. Decoramos una de las salas con vistas al fondo del Aquarium para que las niñas se sintiesen en el fondo del mar, repartimos welcome packs con camisetas, una pulsera mágica y productos de la peli. Y peinamos a las niñas con mechas de colores.
				<br><br>
				Pero lo mejor de todo fue la sorpresa: ¡las niñas conocieron a Barbie de verdad!
			@else
				On the day of the event, everything had to reflect the movie's magic. We decorated one of the halls with views of the Aquarium in the background so that girls would feel like they were under the sea, we handed out welcome packs with shirts, a magic bracelet and products related to the film. And we combed coloured highlights into the girls' hair.
				<br><br>
				But the biggest surprise of all was that the girls got to meet Barbie herself!
			@endif

		</div>
		<div class="col-md-offset-1 col-md-6">
			@include('includes.projects.video',['id'=>'92631642'])
		</div>
	</section>

	<hr>

	<section class="row bg-gray">
		<div class="col-md-7">
            @include('includes.picture' ,['image' => 'img-2.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-5">
            @include('includes.picture' ,['image' => 'img-3.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-4.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-5">
            @include('includes.picture' ,['image' => 'img-5.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-7">
            @include('includes.picture' ,['image' => 'img-6.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
		<div class="col-md-12">
			@lang('project.and-also'):
			<ul class="list-also">
				<li>@lang('project.barbie.contest',[], $locale)</li>
				<li>@lang('project.banner-campaign',[], $locale)</li>
				<li>@lang('project.newsletters',[], $locale)</li>
				<li>@lang('project.community',[], $locale)</li>
				<li>@lang('project.press-releases',[], $locale)</li>
			</ul>
		</div>
	</section>

	<section class="row row-results">
	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'users','title'=>'1.800','es'=>'niñas participaron en el concurso','en'=>'girls entered the competition'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'heart','title'=>'350','es'=>'niñas acudieron al evento exclusivo y conocieron a Barbie en persona','en'=>'girls came to the exclusive event and got to meet Barbie in person'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'video','title'=>'Boing','es'=>'El vídeo del evento se emitió en la tele','en'=>'The video of the event was shown on TV'])
		</div>
	</div>

	</section>
	
@stop

