@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Por primera vez, el Servei Català de Trànsit iba a estar presente en el Salón Internacional del Cómic de Barcelona. Y quiso hacerlo de una forma innovadora y rompedora.
    <br>
    Nos pidió diseñar un espacio que no dejara indiferente a nadie y que a la vez fuera una plataforma para concienciar a los asistentes de la importancia de una conducción responsable.
@stop

@section('col2')
    <h2>El concepto </h2>
    Crear una historia que uniera el mundo del cómic y la ciencia ficción con el Servei Català de Trànsit, y que tuviera un carácter didáctico que conectara con el público más joven.
    <h2>La idea</h2>
    Dimos vida a nuestros propios personajes de cómic: “Els Guardians del Trànsit”. Dos agentes de tráfico provenientes de un futuro sin accidentes, que aterrizaron en Barcelona para mostrarnos los peligros al volante.
@stop

@section('col3')
    <h2>El espacio</h2>
    Diseñamos un espacio futurista con dos atracciones interactivas. <br/>
    El Viatge en el temps: Un photocall interactivo en forma de nave que llevó a los tripulantes a diferentes momentos de la historia, buscando el día en el que todo empezó a cambiar.
    <br/>
    Y el simulador de realidad virtual con el que pudieron sentir en primera persona las consecuencias de beber, tomar drogas y de las distracciones al volante, gracias a las Oculus Rift.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    For the first time, the Catalan Traffic Service was present in the Barcelona International Comics Convention. And they wanted to do it in an innovative and groundbreaking way.
    <br/>
    They asked us to design a space that doesn't leave anyone indifferent! The main objetive of this space was to raise awareness among participants of the importance of responsible driving.

@stop

@section('col2')
    <h2>The concept</h2>
    Create a history able to link the comic universe and the science fiction with the Catalan Traffic Service. That story had to be didactic and powerful enough to connect with the younger audience.
    <h2>The idea</h2>
    We gave life to our own comic characters: The Guardians of the Traffic. Two traffic officers from a future without accidents, which landed in Barcelona to show us the dangers behind the steering wheel.
@stop

@section('col3')
    <h2>The Space</h2>
    We designed a futuristic space with two interactive attractions. <br/>
    The travel in time: An interactive photocall that led the crew "travel" into different moments in history, looking for the day when everything started to change. The interactive photocall was a spacecraft.
    <br/>
    And the virtual reality simulator with which the assistants could feel in their own body the consequences of drinking, taking drugs and having distractions while they were driving, thanks to the Oculus Rift.
@stop

@endif


@section('mainVideo')

    @include('includes.projects.h3-row',['title'=>Lang::get('project.video-case',[], $locale)])

    @include('includes.projects.mainvideo',['id'=>'187632487'])

@stop

@section('case')


    @include('includes.projects.h3-row',['title'=>Lang::get('project.space',[], $locale)])


    <section class="row bg-gray">

    <div class="col-md-12">@include('includes.picture' ,['image' => 'img-1.jpg', 'alt' => $title . ', ' .$clientsString, 'alt' => $title . ', ' .$clientsString])</div>
    <h4 class="col-md-12">@include('includes.trans',['es'=>'Diseño del Traje','en' => 'Suit design'])</h4>

    <div class="col-sm-6">@include('includes.picture' ,['image' => 'suit-1.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-xs-6 col-sm-3">@include('includes.picture' ,['image' => 'suit-2.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-xs-6 col-sm-3">@include('includes.picture' ,['image' => 'suit-3.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-12">@include('includes.picture' ,['image' => 'suit-4.jpg', 'alt' => $title . ', ' .$clientsString])</div>

    <h4 class="col-md-12">@include('includes.trans',['es'=>'Diseño de la nave','en' => 'Spaceship design'])</h4>

    <div class="col-md-12">@include('includes.picture' ,['image' => 'spaceship.jpg', 'alt' => $title . ', ' .$clientsString])</div>

    <h4 class="col-md-12">@include('includes.trans',['es'=>'Diseño del espacio','en' => 'Stand design'])</h4>

    <div class="col-md-8">@include('includes.picture' ,['image' => 'stand-1.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-4">@include('includes.picture' ,['image' => 'stand-2.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-12">@include('includes.picture' ,['image' => 'stand-3.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-6">@include('includes.picture' ,['image' => 'stand-4.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-6">@include('includes.picture' ,['image' => 'stand-5.jpg', 'alt' => $title . ', ' .$clientsString])</div>

    <h4 class="col-md-12 clearfix">@include('includes.trans',['es'=>'Photocall Interactivo: El viaje en el tiempo','en' => 'Interactive Photocall: Travel in Time'])</h4>

    <div class="col-md-6">@include('includes.picture' ,['image' => 'photocall-1.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-6">@include('includes.picture' ,['image' => 'photocall-2.jpg', 'alt' => $title . ', ' .$clientsString])</div>

    <h4 class="col-md-12">@include('includes.trans',['es'=>'Simulador de realidad virtual: Oculus Rift','en' => 'Virtual Reality Simulator: Oculus Rift'])</h4>

    <div class="col-md-12">@include('includes.picture' ,['image' => 'oculus-1.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-6">@include('includes.picture' ,['image' => 'oculus-2.jpg', 'alt' => $title . ', ' .$clientsString])</div>
    <div class="col-md-6">@include('includes.picture' ,['image' => 'oculus-3.jpg', 'alt' => $title . ', ' .$clientsString])</div>


</section>


<section class="row row-results">

    @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

    <div class="row">
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'users','title_es'=>'Más de 2000','es'=>'personas viajaron en el tiempo en nuestro photocall interactivo','title_en'=>'More than 2000','en'=>'people traveled through time in our interactive photocall'])
        </div>
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'oculus','title'=>'850','es'=>'sintieron los riesgos al volante con la experiencia Oculus Rift','en'=>'felt the risks behind the steering wheel with the Oculus Rift experience'])
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            @include('includes.projects.results',['icon'=>'heart','title_es'=>'1 espacio','es'=>'que no dejó indiferente a ninguno de los asistentes que pasaron por delante de nuestro stand','title_en'=>'1 space','en'=>'that did not left indifferent any of the assistants who walked past our stand'])
        </div>
    </div>

</section>

@stop
