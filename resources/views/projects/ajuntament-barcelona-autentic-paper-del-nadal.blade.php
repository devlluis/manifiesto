@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    El Ajuntament de Barcelona quería una campaña de concienciación ciudadana para fomentar la compra de proximidad durante la Navidad. El objetivo era dejar claro al ciudadano que en este tema, su papel es muy importante.
@stop

@section('col2')
    <h2>El concepto </h2>
    Necesitábamos que el ciudadano se reencontrara con el significado más auténtico de la Navidad, el que te empuja a ayudar a los demás y a mantener el barrio vivo. Pero, ¿cómo íbamos a transmitir el auténtico papel de la Navidad sin perder la ilusión por comprar? Utilizando el papel de regalo como medio de comunicación.
@stop

@section('col3')
    <h2>La idea</h2>
    <strong>"L'autèntic paper del Nadal"</strong>
    Creamos el primer papel de regalo del Ajuntament de Barcelona. Un papel ecológico y con un diseño muy especial para envolver todos los regalos comprados en las tiendas de barrio.  
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Barcelona's Council wanted a new public awareness campaign to promote proximity purchase during the Christmas season. The purpose was to communicate citiziens that, in this subject, its role is very important. 
@stop

@section('col2')
    <h2>The concept</h2>
    We needed the citizien to rediscover the real meaning of Christmas, to help other people and to keep the district alive. But how could we convey the authentic role of Christmas without losing the wish of buying? Using gift wrap as media.
@stop

@section('col3')
    <h2>The idea</h2>
    <strong>"The real Christmas' paper"</strong>
    We made Barcelona's Council first gift wrap. Made of recicled paper and with a very special design, to wrap all gifts bought at the neighborhood shops.
@stop

@endif


@section('case')

    <section class="row">   
            <div class="col-md-12">
                <h3>{{Lang::get('project.video-case',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
            </div>
            <div class="col-md-12 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.projects.video',['id'=>'223749665'])
            </div>
    </section>

    <section class="row bg-gray">   
            <div class="col-md-12">
                <h3 style="margin-top:0px; padding-top:30px;">{{Lang::get('project.campaign',[], $locale)}}</h3>
                @include('includes.projects.project-dash')
                 <h4>@include('includes.trans',['es'=>'Casitas','en' => 'Stands'])</h4>
                 @include('includes.trans',['es'=>'Organizamos el montaje y vinilamos las casitas donde se envolvían los regalos comprados en las tiendas de los barrios. ','en' => 'We made Barcelona\'s Council first gift wrap. Made of recicled paper and with a very special design, to wrap all gifts bought at the neighborhood shops.'])
                 <br><br>
            </div>
            <div class="col-md-12 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1.jpg','class'=>''])
            </div>
            <div class="col-md-12 fadeInLeft preanimate" style="margin-bottom: 50px;">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-2.jpg','class'=>''])
            </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h4>@include('includes.trans',['es'=>'Poster','en' => 'Poster'])</h4>
            @include('includes.projects.project-dash')
        </div>
    </section>
    <section class="row bg-gray">
        <div class="col-md-6" style="padding: 0px 15px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-3.jpg','class'=>''])
        </div>
        <div class="col-md-6" style="padding: 0px 15px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-4.jpg','class'=>''])
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h4>Flyer</h4>
            @include('includes.projects.project-dash')
        </div>
    </section>
    <section class="row bg-gray">
        <div class="col-md-7" style="padding: 0px 15px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-5.jpg','class'=>''])
        </div>
        <div class="col-md-5" style="padding: 0px 15px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-6.gif','class'=>''])
        </div>
        <div class="col-md-12" style="padding: 0px 15px; margin-top: 40px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-7.jpg','class'=>''])
        </div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h4>@include('includes.trans',['es'=>'Redes sociales','en' => 'Social Media'])</h4>
            @include('includes.projects.project-dash')
        </div>
    </section>
    <section class="row bg-gray">
        <div class="col-md-1" style="padding: 0px 15px;"></div>
        <div class="col-md-10" style="padding: 0px 15px;">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'imac.png','class'=>''])
            <webm class="fadeInUp preanimate social-paper-nadal" source="{{$image_path}}social" fallback="{{$image_path}}social.gif"></webm>
        </div>
        <div class="col-md-1" style="padding: 0px 15px;"></div>
    </section>
    <section class="row">
        <div class="col-md-12">
            <h3>@include('includes.trans',['es'=>'Vídeo de lanzamiento de campaña','en' => 'Campaign Launch Video'])</h3>
            @include('includes.projects.project-dash')
        </div>
    </section>
    <section class="row bg-gray">
        <div class="col-md-12" style="padding: 0px 15px;">
            @include('includes.projects.video',['id'=>'224175691'])
        </div>
    </section>

    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'shop','title_es'=>'Acción en 10','es'=>'barrios de Barcelona','title_en'=>'Action in 10','en'=>'Barcelona\'s neighborhoods'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'gift','title_es'=>'+ 12.000','es'=>'regalos envueltos','title_en'=>'+ 12.000','en'=>'gifts wraped'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'newspaper','title_es'=>'+ 150','es'=>'apariciones en medios','title_en'=>'+ 150', 'en'=>'media mentions'])
            </div>
            <div class="col-md-6" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'sessions','title_es'=>'Demostramos','es'=>'que el comercio de barrio está vivo','title_en'=>'We show', 'en'=>'that the neighborhood trade is alive'])
            </div>
        </div>
    </section>

@stop
