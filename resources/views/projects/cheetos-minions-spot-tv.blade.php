@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
	<h2>El encargo</h2>
	El reto que nos lanzó Cheetos fue crear el spot de TV de 20” y 10” para la nueva licencia de tazos Minions sin perder el punto de “diversión traviesa”.
	<h2>El concepto</h2>
	Convertir al niño protagonista en el líder de los Minions.
	@stop

	@section('col2')
	<h2>La idea</h2>
	El spot cuenta una divertida historia en la que los tazos triple volador de Minions encuentran a un niño con el que hacer una travesura. La gamberrada consiste en fastidiar a su hermana mayor mientras se hace una selfie. Gracias a los tazos triple volador de Minions su misión es todo un éxito.
	@stop



@endif

@if ($locale=='en')

	@section('col1')
	<h2>The assignment</h2>
	The challenge that launched us Cheetos was to create the TV spot of 20 "and 10" to the new license of tazos Minions without losing the "naughty fun" point.
	<h2>The concept</h2>
	The child actor become the leader of the Minions.
	@stop

	@section('col2')
	<h2>The idea</h2>
	The spot has a funny story in which the flying tazos' Minions are a child to do a mischief. Prank is to annoy his older sister while making a selfie. Thanks to the tazos flying Minions of their mission is a success.
	@stop


	
@endif


@section('case')

	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			<h3 class="h3-row-video">{{Lang::get('project.spot',[], $locale)}}</h3>
			@include('includes.projects.project-dash')

		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video-yt',['id'=>'7gu6FF-3hCs'])

		</div>
	</section>

    <section class="row ">
        <div class="col-md-5 fadeInLeft preanimate">

            <h3>@lang('project.campaign')</h3>

            @include('includes.projects.project-dash')

            @include('includes.trans',['es'=>'Spot para TV. Versiones de 20" y 10"','en' => 'TV Spot, 20" and 10" versions'])

        </div>
    </section>

	<section class="row bg-gray">
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-1.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
        </div>
		@for ($i = 2; $i <= 5; $i++)
		<div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
        </div>
		@endfor
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-6.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
        </div>
		@for ($i = 7; $i <= 8; $i++)
			<div class="col-md-6">
                @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
            </div>
		@endfor
		<div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-9.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
        </div>
	</section>

@stop

