@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    La Generalitat de Cataluña nos pidió una campaña 360 para comunicar la ampliación de recursos que garantizan el derecho a la vivienda. Además, querían transmitir que son una administración cercana que pone todo su esfuerzo en encontrar soluciones a los ciudadanos con problemas para acceder a la vivienda o mantenerla. 
@stop

@section('col2')
    <h2>El concepto</h2>
    Cuando nos encontramos frente a una situación complicada con la vivienda, solo encontramos barreras. La Generalitat de Cataluña trabaja para eliminar esas barreras. De ahí nació el concepto: "Una puerta abierta a la vivienda".
@stop

@section('col3')
    <h2>La idea</h2>
    La puerta se convirtió en nuestro elemento comunicador. Esta puerta abierta se convirtió en un símbolo de esperanza para los ciudadanos en riesgo en cuanto a la vivienda. 
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Generalitat de Catalunya asked us for a 360 campaign to communicate the resources' expansion which guarantees the right to housing. Moreover, they wanted to inform that they are a close administration which puts all its effort to find solutions for the citizens with problems on accesing housing or keeping it.
@stop

@section('col2')
    <h2>The concept</h2>
    When we find ourselves in a complicated situation with housing, we only find barriers. Generalitat de Catalunya works to remove those barriers. From there, the concept: ""An open door to housing"" was born.
@stop

@section('col3')
    <h2>The idea</h2>
    The door became our communicating element. This open door became a symbol of hope for citizens at risk in terms of housing.
@stop

@endif

@section('case')
    @include('includes.projects.h3-row',['title'=>Lang::get('project.campaign',[], $locale)])

    <h4>@include('includes.trans',['es'=>'Spot TV','en' => 'TV Spot '])</h4>



<section class="row bg-gray">

    {{--  SPOTS  --}}
    <div class="col-md-6">
        <h5>@include('includes.trans',['es'=>'Spot TV 30``','en' => '30`` TV Spot '])</h5>
        @include('includes.projects.video',['id'=>'239594356'])
    </div>
    <div class="col-md-6">
        <h5>@include('includes.trans',['es'=>'Spot TV 20``','en' => '20`` TV Spot '])</h5>
        @include('includes.projects.video',['id'=>'239594341'])
    </div>    

   {{--  FOTOS RODAJE  --}}
    <div class="col-md-12">
        <h4>Making of</h4>
    </div>
   
   <div class="col-sm-8">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'spot-1.jpg','class'=>''])
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'spot-2.jpg','class'=>''])
    </div>
    <div class="col-sm-4">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'spot-3.jpg','class'=>''])
    </div>
    <div class="col-sm-6">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'spot-4.jpg','class'=>''])
    </div>
    <div class="col-sm-6">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'spot-5.jpg','class'=>''])
    </div>


</section>
<section class="row">
    <div class="col-md-12">
        <h3>@include('includes.trans',['es'=>'Key visual de campaña','en' => 'Key visual'])</h3>
        @include('includes.projects.project-dash')
    </div>
</section>

<section class="row bg-gray">
    <div class="col-md-12">
        <webm source="{{$image_path}}keyvisual-anim" fallback="{{$image_path}}keyvisual-anim.gif"></webm>
    </div>
    <div class="col-md-12">
        <br>
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'keyvisual.jpg','class'=>''])
    </div>
</section>

<section class="row">
    <div class="col-md-12">
        <h3>@include('includes.trans',['es'=>'Prensa','en' => 'Press'])</h3>
        @include('includes.projects.project-dash')
    </div>
</section>

<section class="row bg-gray">
    <div class="col-sm-4">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'prensa-1.jpg','class'=>''])
    </div>
    <div class="col-sm-4">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'prensa-2.jpg','class'=>''])
    </div>
    <div class="col-sm-4">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'prensa-3.jpg','class'=>''])
    </div>
</section>

<section class="row">
    <div class="col-md-12">
        <h3>@include('includes.trans',['es'=>'Exterior','en' => 'Outdoor'])</h3>
        @include('includes.projects.project-dash')
    </div>
</section>

<section class="row bg-gray">
    <div class="col-md-12">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'exterior-1.jpg','class'=>''])
    </div>
    <div class="col-md-12">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'exterior-2.jpg','class'=>''])
    </div>
    <div class="col-sm-4">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'exterior-3.jpg','class'=>''])
    </div>
    <div class="col-sm-8">
        @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'exterior-4.jpg','class'=>''])
    </div>
</section>

<section class="row">
    <div class="col-md-12">
        <h3>@include('includes.trans',['es'=>'Cuñas de radio','en' => 'Radio slots'])</h3>
        @include('includes.projects.project-dash')
    </div>
</section>

<section class="row bg-gray">
    <project-audio class="col-sm-3" source="generica" slug="{{$projectSlug}}"></project-audio>
    <project-audio class="col-sm-3" source="desnonaments" slug="{{$projectSlug}}"></project-audio>
    <project-audio class="col-sm-3" source="lloguer" slug="{{$projectSlug}}"></project-audio>
    <project-audio class="col-sm-3" source="hipoteca" slug="{{$projectSlug}}"></project-audio>
</section>

<section class="row">
    <div class="col-md-12">
        <h3>@lang('project.campaign-online')</h3>
        @include('includes.projects.project-dash')
    </div>
</section>

<section class="row bg-gray">
    <div class="col-md-12">
        @include('includes.projects.slider',['numPics'=>3])
    </div>
</section>

@stop