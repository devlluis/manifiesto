@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Barbie quería lanzar una campaña que involucrara al máximo a las niñas y diera a conocer la nueva canción "Todo es posible con Barbie".

    <h2>El objetivo</h2>
    Amplificar el lanzamiento de la nueva canción y crear engagement con la niñas con una campaña muy notoria.
@stop

@section('col2')
    <h2>El concepto</h2>
    La letra de la canción "Todo es posible con Barbie" habla de la posibilidad de cumplir los sueños y hacerlos realidad. Así, trasladamos este concepto y lo convertimos en una promoción que hiciera que las niñas cumplieran uno de sus mayores sueños: ser las protagonistas del nuevo videoclip de Barbie.

    <h2>La idea</h2>
    Teníamos una canción, queríamos hacer un videoclip... nos faltaba lo más importante: ¡la coreografía! Y nos dimos cuenta que las más indicadas para crearla eran las propias niñas. Así nació el concurso BAILA CON BARBIE.
@stop

@section('col3')
    <h2>El concurso</h2>
    Con el objetivo de buscar a las protagonistas del nuevo videoclip, lanzamos un site donde las niñas podían descargarse la música y la letra de la canción para crear sus coreografías y enviar sus vídeos con sus creaciones. De entre las 20 coreografías más votadas, un jurado eligió a las 5 niñas ganadoras que se convirtieron en las protagonistas del nuevo videoclip de Barbie, bailando una coreografía creada con sus propios pasos de baile.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Barbie wanted to launch a campaign that involved the most girls and to share the new song "Everything is possible with Barbie". "

    <h2>The goal</h2>
    Amplify the launch of the new song and create engagement with the girls with a very noticeable campaign.
@stop

@section('col2')
    <h2>The concept</h2>
    The lyrics of the song "Everything is possible with Barbie" speaks of the possibility of fulfilling dreams and make them reality. " Thus, we move this concept and turn it into a promotion that made girls to fulfil one of his greatest dreams: being the protagonists of the new video of Barbie.

    <h2>The idea</h2>
    We had a song, we wanted to make a videoclip... most importantly we needed: the choreography! And we realized that those more indicated to create it were own girls. Thus was born the contest DANCE WITH BARBIE.
@stop

@section('col3')
    <h2>The contest</h2>
    In order to search for the protagonists of the new video, we launched a site where girls could download the music and the lyrics of the song to create his choreography and send their videos with their creations. Among the 20 most voted choreographies, a jury chose the 5 winning girls who became the protagonists of the new video of Barbie, dancing choreography created with their own dance steps.
@stop

@endif


@section('mainVideo')

    <h3>@lang('project.video-case',[], $locale)</h3>

    @include('includes.projects.project-dash')

    @include('includes.projects.mainvideo',['id'=>'154709364'])

@stop

@section('case')


    <section class="row">
        <div class="col-md-5 fadeInLeft preanimate">

            <h3 class="h3-row-video">{{Lang::get('project.campaign',[], $locale)}}</h3>
            @include('includes.projects.project-dash')

            <h4>@lang('project.video',[], $locale)</h4>


        </div>
        <div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
            @include('includes.projects.video',['id'=>'146879316'])

        </div>
    </section>


    <section class="row">
        <div class="col-md-12">

            <h4>@lang('project.website',[], $locale)</h4>

            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-12">
            @include('includes.projects.slider',['numPics'=>3])
        </div>

        <div class="col-md-12" style="margin-top: 5%;">

            <h4>@include('includes.trans',['es'=>'Y además:','en' => 'And much more:'])</h4>

            @include('includes.projects.project-dash')

            <h5 class="is-size-3">@include('includes.trans',['es'=>'Fuerte campaña de Community Management','en' => 'Strong Community Management campaign'])</h5>
            <h5 class="is-size-3">@include('includes.trans',['es'=>'Cola promocional en Boing','en' => 'Promotional tail in Boing'])</h5>
            <h5 class="is-size-3">Prints</h5>
            <h5 class="is-size-3">Banners</h5>
            <h5 class="is-size-3">Newsletters</h5>

        </div>
    </section>


    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
        
        <div class="row">
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'eye','title'=>'39.270','es'=>'visitas a la web de la campaña','en'=>'visits to the campaign website'])
            </div>
            <div id="dance" class="col-md-6">
                @include('includes.projects.results',['icon'=>'dance','title'=>'5.907','es'=>'de niñas bailaron la canción de Barbie','en'=>"of girls danced the Barbie's song"])
            </div>
        </div>

    </section>

@stop
