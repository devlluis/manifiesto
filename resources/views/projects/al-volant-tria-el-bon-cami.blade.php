@extends('layouts.project')

@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    El Servei Català de Trànsit necesitaba una nueva campaña de concienciación ciudadana para reducir los accidentes en carretera, en la época estival. El mensaje debía ser claro, directo e impactante.
@stop

@section('col2')
    <h2>El concepto </h2>
    Necesitábamos un concepto creativo que dejara bien claro que los accidentes de tráfico se pueden evitar. Solo se trata de tomar las decisiones correctas.
    <br>
    "AL VOLANT, TRIA EL BON CAMÍ" cumple perfectamente con este objetivo.
@stop

@section('col3')
    <h2>La idea</h2>
    A través de la narrativa hipertextual, trabajamos dos historias, basadas en hechos reales, que se cruzaron en un trágico final.
    <br/>
    Y, a partir de aquí, desarollamos todas las piezas de la campaña.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    The Servei Català de Trànsit needed a new public awareness campaign to reduce the number of road accidents during the summer season. The message had be clear, direct and impressive.
@stop

@section('col2')
    <h2>The concept</h2>
    We needed a creative concept that made it clear that road accidents could be avoided. It's just making the right choices.
    <br>
    The claim "BEING AT THE WHEEL, CHOOSE THE RIGHT TRACK" perfectly fulfills this objective.
@stop

@section('col3')
    <h2>The idea</h2>
    Through the hypertextual narrative, we worked two stories, based on real facts, that were crossed between them in a tragic ending.
    <br/>
    And from this idea , we developed all the advertising campaign.
@stop

@endif


@section('case')

    <section class="row">
        <div class="col-md-12 fadeInLeft preanimate">
            <h3>{{Lang::get('project.video-case',[], $locale)}}</h3>
            @include('includes.projects.project-dash')
            @include('includes.projects.mainvideo',['id'=>'200802761'])
        </div>
    </section>


    <section class="row">
        <div class="col-md-12 fadeInLeft preanimate" style="margin-bottom: 30px;">
            <h3>@lang('project.campaign',[], $locale)</h3>
            @include('includes.projects.project-dash')

            <h4>Spots</h4>
        </div>
        <div class="col-md-4 fadeInLeft preanimate" style="margin-bottom: 50px;">
            @include('includes.projects.video-yt',['id'=>'tXXpywnPt_o'])
        </div>
        <div class="col-md-4 fadeInLeft preanimate" style="margin-bottom: 50px;">
            @include('includes.projects.video-yt',['id'=>'bp1hNxILCSs'])
        </div>
        <div class="col-md-4 fadeInLeft preanimate" style="margin-bottom: 50px;">
            @include('includes.projects.video-yt',['id'=>'8y_iwCA-DK4'])
        </div>

        @for ($i = 1; $i <= 6; $i++)
            <div class="col-md-6" style="margin-bottom:30px">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-'.$i.'.jpg','class'=>''])
            </div>
        @endfor
    </section>

    <section class="row bg-gray">
        <div class="col-md-12">
            <h4>@include('includes.trans',['es'=>'Cuñas de radio','en' => 'Radio slots'])</h4>
            @include('includes.projects.project-dash')
                <project-audio class="col-sm-3" source="familia" slug="{{$projectSlug}}"></project-audio>
                <project-audio class="col-sm-3" source="pau" slug="{{$projectSlug}}"></project-audio>
        </div>

        <div class="col-md-12">
            <h3>@include('includes.trans',['es'=>'Prensa','en' => 'Press'])</h3>
            @include('includes.projects.project-dash')

            <div class="col-md-6">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-7.jpg','class'=>''])
            </div>
            <div class="col-md-6">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-8.jpg','class'=>''])
            </div>
        </div>
        <div class="col-md-12">
            <h3>@include('includes.trans',['es'=>'Campaña online','en' => 'Online Campaign'])</h3>
            @include('includes.projects.project-dash')
            @include('includes.projects.slider',['numPics'=>5])
        </div>
    </section>


    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'heart','title_es'=>'26%','es'=>'menos víctimas mortales','title_en'=>'26%','en'=>'less mortal victims'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'video','title_es'=>'Fuerte','es'=>'impacto en medios','title_en'=>'Strong','en'=>'impact on media'])
            </div>
            <div class="col-md-4" style="margin-top:0px">
                @include('includes.projects.results',['icon'=>'users','title_es'=>'Conciencia','es'=>'ciudadana','title_en'=>'Public', 'en'=>'awareness'])
            </div>
        </div>
    </section>

@stop
