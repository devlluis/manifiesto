@extends('layouts.project')


@if ($locale=='es')

@section('col1')
    <h2>El encargo</h2>
    Babicka es un vodka premium checo que quería introducirse en el mercado español.

    <h2>El objetivo</h2>
    Crear un nuevo posicionamiento e imagen de marca diferencial y que transmitiera su valor premium.
@stop

@section('col2')
    <h2>El concepto</h2>
    Nos basamos en uno de los ingredientes característicos del vodka Babicka, la absenta, para marcar un territorio creativo. Analizando la historia de la absenta, construimos el territorio estratégico de la marca y su mundo clandestino, misterioso y secreto.
@stop

@section('col3')
    <h2>La idea</h2>
    Creamos un storytelling con key verbals y key visuals muy concretos para explicar que, cuando la rutina te atrapa, Babicka te pasa al otro lado. Un lado misterioso, secreto y VIP donde puedes vivir experiencias especiales.
@stop

@endif

@if ($locale=='en')

@section('col1')
    <h2>The assignment</h2>
    Babicka is a premium Czech vodka that wanted to enter the Spanish market.

    <h2>The goal</h2>
    Create a new positioning and differential brand image that transmits its premium value.
@stop

@section('col2')
    <h2>The concept</h2>
    We rely on one of the characteristic ingredients of Babicka vodka, the absinthe, to mark a creative territory. Analyzing the history of the absinthe, we build the strategic territory of the brand and its clandestine, secret and mysterious world.
@stop

@section('col3')
    <h2>The idea</h2>
    We created a storytelling with verbals key and very specific visuals key to explain that, when routine catches you, Babicka passes you on the other side. A mysterious, secret and VIP side where you can enjoy special experiences.
@stop

@endif


@section('case')


    <section class="row">
        <div class="col-md-5 fadeInLeft preanimate">

            <h3>@lang('project.spot-online',[], $locale)</h3>
            @include('includes.projects.project-dash')

        </div>
        <div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
            @include('includes.projects.video',['id'=>'149634004'])

        </div>
    </section>


        <h3>@lang('project.website',[], $locale)
            <a href="http://babickaiberica.es" target="_blank" rel="noopener"><span class="site-link"><i class="fa fa-external-link"></i></span></a>
        </h3>

    @include('includes.projects.project-dash')

    <section class="row">
        <div class="col-md-12">

            @include('includes.picture' ,['image' => 'img-1.jpg'])
            
            <webm id="babicka-gif-1" class="fadeIn preanimate" source="{{$image_path}}gif-1" fallback="{{$image_path}}gif-1.gif"></webm>
            <webm id="babicka-gif-2" class="fadeIn preanimate" source="{{$image_path}}gif-2" fallback="{{$image_path}}gif-2.gif"></webm>

        </div>
    </section>

    <section class="row bg-gray">
        <div class="col-md-12">
        <h3  class="is-size-2"> @include('includes.trans',['es'=>'Creación especial de cócteles para Babicka','en' => 'Special creation of cocktails for Babicka'])</h3>
            @include('includes.projects.project-dash')
            @include('includes.projects.slider',['numPics'=>6])
        </div>
        @for ($i = 2; $i <= 3; $i++)
            <div class="col-md-6">
                @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
            </div>
        @endfor

        <div class="col-md-12">

            <h3 class="is-size-2"> @include('includes.trans',['es'=>'Diseño de vaso especial Babicka','en' => 'Special Babicka glass design'])</h3>
            @include('includes.projects.project-dash')
        </div>
        <div class="col-md-8">
            @include('includes.picture' ,['image' => 'img-4.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
        </div>

        <div class="col-md-12">

            <h3 class="is-size-2">@include('includes.trans',['es'=>'Material PLV en locales, hoteles y restaurantes premium','en' => 'POS material in bars, hotels and restaurants premium'])</h3>
            @include('includes.projects.project-dash')

        </div>
        @for ($i = 5; $i <= 6; $i++)
            <div class="col-md-6">
                @include('includes.picture' ,['image' => 'img-'.$i.'.jpg','class'=>'', 'alt' => $title . ', ' .$clientsString])
            </div>
        @endfor

        <div class="col-md-12">

            <h3>@include('includes.trans',['es'=>'Y además:','en' => 'And much more:'])</h2>
            @include('includes.projects.project-dash')

                <h4>@include('includes.trans',['es'=>'Comunidad en Facebook e Instagram','en'=>'Facebook and Instagram Communities'])</h4>
                <h4>@include('includes.trans',['es'=>'Acción promocional de lanzamiento','en'=>'Launch Promotional Action'])</h4>
                <h4>@include('includes.trans',['es'=>'Experiencia de marca \'The other side tour\'','en'=>'Brand Experience \'The other side tour\''])</h4>
        </div>
    </section>





    <section class="row row-results">
        @include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

        <div class="row">
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'plus','title_es'=>'Cientos','es'=>'de locales se han unido a ‘The other side’ de Babicka','title_en'=>'Hundreds','en'=>'of clubs have joined  ’The other side’ of Babicka'])
            </div>
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'image','title_es'=>'Imagen','es'=>'nueva e impactante','title_en'=>'Image','en'=>'New and impressive'])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'position','title_es'=>'Nuevo','es'=>'posicionamiento','title_en'=>'New','en'=>'positioning'])
            </div>
            <div class="col-md-6">
                @include('includes.projects.results',['icon'=>'cup','title_es'=>'Marca','es'=>'sólida','title_en'=>'Solid','en'=>'brand'])
            </div>
        </div>
    </section>

@stop
