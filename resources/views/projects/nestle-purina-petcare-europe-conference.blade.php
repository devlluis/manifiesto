@extends('layouts.project')

@if ($locale==='es')

	@section('col1')
	<h2>El encargo</h2>
    Cada año, Nestlé Purina realiza una conferencia con los 150 top managers de toda Europa. Una conferencia a la que acuden representantes de más de 30 países y donde se analizan los resultados y tendencias del sector y se marcan las pautas estratégicas a seguir de cara al futuro. En esta ocasión, la conferencia era en Barcelona y nos encargamos de la realización de la organización del evento.
	@stop

	@section('col2')
	<h2>La organización</h2>
    Queríamos que los asistentes al evento lo recordaran para siempre, así que buscamos un lugar único y que representara simbólicamente el ADN de la ciudad y lo encontramos. El Hotel W reunía todas estas características así que ese era uno de los espacios para las conferencias, además de las propias oficinas que Nestlé tiene en Barcelona.</p>

    <p>Decoramos la sala principal del Hotel W con una trasera textil envolvente con 3 pantallas integradas con proyección frontal, donde se transmitirían todas las presentaciones y se realizarían los speech de cada ponente. Además, utilizamos cabezas móviles con efectos visuales para conseguir una iluminación que enfatizara a cada uno de los ponentes y creara dinamismo entre una presentación y otra.</p>



    @stop

	@section('col3')
    <h2>&nbsp;</h2>
    <p>Con motivo del cambio de tendencia en el mercado del cuidado de animales, “Winning the Purina way in the new reality” era el eje central de las conferencias, así que decidimos crear el premio “The Purina Way Awards” que premiaba las mejores prácticas en cada uno de los países. Una forma de destacar el esfuerzo de los trabajadores y, además, se convertía en un bonito recuerdo del evento.</p>
	@stop

@endif

@if ($locale==='en')

	@section('col1')
	<h2>The assignment</h2>
    Each year, Nestlé Purina holds a conference attended by 150 top managers from all over Europe. At this conference, representatives from more than 30 countries analyse the sector’s results and trends, and set the strategic guidelines for the future. This latest edition took place in Barcelona and we were responsible for organising the event.
	@stop

	@section('col2')
	<h2>The organisation</h2>
    <p>We wanted to make this a truly memorable event for all those attending, so we set about looking for a unique venue that would symbolise the city’s DNA. And we found it. The Hotel W offers all this and more, making it the perfect space to host the talks, together with the Nestlé offices in Barcelona.</p>

    <p>We decorated the main meeting room at the Hotel W with a large fabric surround featuring 3 integrated screens with frontal projection in order to transmit all the presentations and speakers’ talks. We also used moving heads with visual effects to create special lighting that would spotlight each of the speakers and create a sense of movement and differentiation between each presentation.</p>

	@stop

	@section('col3')
    <h2>&nbsp;</h2>
    <p>To mark the changing market trend in animal care, ‘Winning the Purina way in the new reality’ formed the axis on which all the talks hinged, and we therefore decided to create ‘The Purina Way Awards’, in acknowledgement of the best practices in each country. A great way of highlighting the employees’ efforts and dedication and also a fabulous memento of the event.</p>
	@stop

@endif


@section('mainVideo')
    <hr/>
@stop

@section('case')

	<section class="row fadeInUp preanimate">
		<div class="col-md-12">
		@include('includes.projects.h3-row',['title'=>Lang::get('project.event',[], $locale)])
		</div>
        <div class="col-sm-12 col-lg-8">

        @if ($locale==='es')
        <p>La Nestlé Purina PetCare Europe Conference fueron 2 días de conferencias y estudios de casos de éxito en un entorno único. Además, se creó una aplicación que permitía a todos los asistentes seguir toda la agenda del evento a través de sus dispositivos móviles. También les servía para las distintas sesiones de trabajo que se plantearon (respondiendo a preguntas sobre cada sesión, votando sus preferencias o realizando encuestas).</p>
        <p>El evento se cerró con una cena de gala que dejó a todos los asistentes boquiabiertos y con muchas ganas de repetir de ciudad.</p>
        @else
        <p>The Nestlé Purina PetCare Europe Conference comprised 2 days of talks and successful case studies in a unique setting. An app was also created enabling all delegates to follow the event programme on their mobile devices. It also came in handy for the various work sessions (answering the questions that were raised during each session, voting for their preferred options or completing surveys).</p>
        <p>The event closed with a gala dinner that left all those present truly amazed and eager to return to Barcelona.</p>
        @endif
        <br/><br/>
        </div>

        <div class="col-md-12">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1.jpg','class'=>'fadeInUp preanimate img-width-fix'])
        </div>

	</section>

    <section class="row bg-gray">
        <div class="col-md-8">
            @include('includes.picture' ,['image' => 'img-2.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
        <div class="col-md-4">
            @include('includes.picture' ,['image' => 'img-3.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
        <div class="col-md-12">
            @include('includes.picture' ,['image' => 'img-4.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
        <div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-5.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
        <div class="col-md-6">
            @include('includes.picture' ,['image' => 'img-6.jpg','class'=>'fadeInUp preanimate ', 'alt' => $title . '. ' .$clientsString])
        </div>
    </section>


	<section class="row row-results">

	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])

	<div class="row">
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'users','title'=>'150','es'=>'top managers reunidos','en'=>'participating top managers'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'world','title'=>'30','es'=>'países representados','en'=>'countries represented'])
		</div>
		<div class="col-md-4">
			@include('includes.projects.results',['icon'=>'video','title'=>'2','es'=>'días de exitosas conferencias','en'=>'days of successful talks'])
		</div>
	</div>

	</section>

@stop
