@extends('layouts.project')

@if ($locale=='es')

	@section('col1')
		<h2>El encargo</h2>
		Felicitar la Navidad transmitiendo la importancia de la familia e incluyendo el producto de una forma natural y orgánica.
	@stop

	@section('col2')
		<h2>El concepto</h2>
		Creamos el concepto “la receta de la vida” para demostrar que una receta es como la vida misma y que, como en todas las recetas, siempre hay un ingrediente imprescindible.
	@stop

	@section('col3')
		<h2>La idea</h2>
		Demostrar que ninguna receta tiene sentido si no se comparte en familia. Para conseguirlo, fuimos a casa de Carmen, una abuela muy entrañable para que nos contara su mejor receta de Navidad: pavo al horno. Pero lo que no sabía Carmen es que, en realidad, estábamos allí para darle una gran sorpresa. Una sorpresa que no olvidaría jamás.
	@stop

@endif

@if ($locale=='en')

	@section('col1')
		<h2>The assignment</h2>
		Celebrate Christmas conveying the importance of the family and including the product in a natural and organic way.
	@stop

	@section('col2')
		<h2>The concept</h2>
		We created the concept "the recipe for life" to show that a recipe is like life itself and that, as in all recipes, always there is an essential ingredient.
	@stop

	@section('col3')
		<h2>The idea</h2>
		Demonstrate that no recipe makes sense if it is not shared in family. To achieve this, we went to Carmen home, a grandmother very endearing so tell us your best recipe for Christmas: Turkey in the oven. But what Carmen did not know is that, in reality, we were there to give you a big surprise. No surprise that not ever forget.
	@stop
	
@endif


{{--@section('mainVideo')

	@include('includes.projects.mainvideo',['id'=>'108263641'])

@stop--}}

@section('case')


	<section class="row">
		<div class="col-md-5 fadeInLeft preanimate">

			<h3 class="h3-row-video">{{Lang::get('project.campaign',[], $locale)}}</h3>
			@include('includes.projects.project-dash')

			<h4>@include('includes.trans',['es'=>'Spot para internet','en'=>'Internet Spot'])</h4>
			
		</div>
		<div class="col-md-offset-1 col-md-6 fadeInRight preanimate">
			@include('includes.projects.video-yt',['id'=>'H0hu05O1YtI'])
		</div>
	</section>

	<section class="row bg-gray">
		<div class="col-md-12">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-1.jpg'])
		</div>
		@for ($i = 2; $i <= 5; $i++)
			<div class="col-md-6">
                @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-'.$i.'.jpg'])
            </div>
		@endfor
	</section>

	<h4 class="is-size-3">@include('includes.trans',['es'=>'Creatividad de posts de difusión en Facebook y Twitter','en'=>'Creativity for broadcasting on Facebook and Twitter posts'])</h4>
	@include('includes.projects.project-dash')

	<section class="row bg-gray">
		<div class="col-md-12">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-6.jpg'])
        </div>
		<div class="col-md-12">
            @include('includes.picture' ,['alt' => $title . ', ' .$clientsString, 'image' => 'img-7.jpg'])
        </div>
	</section>

	<section class="row row-results">
	
	@include('includes.projects.h2-row',['title'=>Lang::get('project.results',[], $locale)])
	
	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'play','title'=>'771.535','es'=>'reproducciones del vídeo en 2 semanas','en'=>'video plays in 2 weeks'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'promo','title'=>'1.500.000','es'=>'impresiones del tweet promocionado','en'=>'impressions in Twitter'])
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'heart','title'=>'5.907','es'=>'likes en Facebook','en'=>'likes in Facebook'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'users','title'=>'3283','es'=>'compartidos en Facebook','en'=>'shared on Facebook'])
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'click','title'=>'6.496','es'=>'clicks en Twitter','en'=>'clicks in Twitter'])
		</div>
		<div class="col-md-6">
			@include('includes.projects.results',['icon'=>'video','title'=>'261.212','es'=>'reproducciones en Youtube','en'=>'plays in Youtube'])
		</div>
	</div>
	</section>

@stop
