<url>
    <loc>{{ config('app.url')}}/{{$lang}}/{{ $url or '' }}</loc>
    @if(isset($date))
        <lastmod>{{ gmdate(DateTime::W3C, strtotime($date)) }}</lastmod>
    @else
        <lastmod>{{ gmdate(DateTime::W3C, strtotime($defaultDate)) }}</lastmod>
    @endif
    <changefreq>monthly</changefreq>
    <priority>{{ $priority or '0.5' }}</priority>
</url>