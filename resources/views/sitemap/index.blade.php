<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($locales as $localeCode => $properties)
        <sitemap>
            <loc>{{ config('app.url')}}/sitemap_{{$localeCode}}.xml</loc>
            <lastmod>{{gmdate(DateTime::W3C, strtotime($defaultDate))}}</lastmod>
        </sitemap>
    @endforeach
</sitemapindex>