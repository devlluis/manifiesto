<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @include('sitemap.url',['url'=>''])

    @foreach($projects as $project)
        @include('sitemap.url',['url'=>'works/' .$project->slug, 'date'=>$project->updated_at,'priority'=>'0.9'])
    @endforeach

    @include('sitemap.url',['url'=>'about','priority'=>'0.5'])
    @include('sitemap.url',['url'=>'services','priority'=>'0.75'])
    @include('sitemap.url',['url'=>'contact','priority'=>'0.75'])
    @include('sitemap.url',['url'=>'legal','priority'=>'0.25'])
</urlset>