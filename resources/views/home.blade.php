@extends('layouts.default')

@section('title'){{ trans('metas.home.title') }} @if(trans('metas.home.title')!=$title) · {{$title}}@endif @stop

@section('metas')
    <meta name="title" content="{{ trans('metas.home.title') }}"/>
    <meta name="description" content="{{ trans('metas.home.description') }}"/>
    <meta name="keywords" content="{{ trans('metas.home.keywords') }}"/>
@stop

@section('styles')
@stop

@section('js')


<script src="/assets/js/app.js"></script>

@stop
@section('content')
{{--  <transition name="fade" appear><router-view id="main-container" name="main-container" class="router-view"></router-view> </transition>  --}}
<router-view id="main-container" name="main-container" class="router-view"></router-view>


@stop