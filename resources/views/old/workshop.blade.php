@extends('layouts.default')

@section('title'){{ trans('metas.contact.title') }}@stop

@section('metas')
    <meta name="title" content="{{ trans('metas.contact.title') }}"/>
    <meta name="description" content="{{ trans('metas.contact.description') }}"/>
    <meta name="keywords" content="{{ trans('metas.contact.keywords') }}"/>
@stop

@section('styles')
    <link rel="stylesheet" href="/assets/css/workshop.css"/>
@stop

@section('js')
    <script src="/assets/js/workshop.js"></script>
@stop

@section('content')
    <div id="workshopContainer">
       <div id="workshopInner">
         <div id="title-workshop" class="row fadeInUp preanimate">
            <div class="col-xs-offset-1 col-md-6 col-xs-10">
                <h2>@lang('workshop.title') <br>
                @lang('workshop.title2')</h2>
                @lang('workshop.subtitle')
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-offset-1 col-md-6 col-xs-10">
                <form method="POST" action="/workshop-un-touch-point-estrategico">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">@lang('workshop.form.name')</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}" placeholder="@lang('workshop.form.name')">
                        @if ($errors->first('name')) <span class="msg-errors">{{$errors->first('name')}}</span> @endif
                    </div>
                    <div class="form-group">
                        <label for="position">@lang('workshop.form.position')</label>
                        <input type="text" name="position" class="form-control" id="position" value="{{old('position')}}" placeholder="@lang('workshop.form.position')">
                        @if ($errors->first('position')) <span class="msg-errors">{{$errors->first('position')}}</span> @endif
                    </div>
                    <div class="form-group">
                        <label for="company">@lang('workshop.form.company')</label>
                        <input type="text" name="company" class="form-control" id="company" value="{{old('company')}}" placeholder="@lang('workshop.form.company')">
                        @if ($errors->first('company')) <span class="msg-errors">{{$errors->first('company')}}</span> @endif
                    </div>
                    <div class="form-group">
                        <label for="email">@lang('workshop.form.email')</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}" placeholder="@lang('workshop.form.email')">
                        @if ($errors->first('email')) <span class="msg-errors">{{$errors->first('email')}}</span> @endif
                    </div>
                    <small>
                    En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, te informamos que tus datos personales quedarán incorporados y serán tratados en un fichero propiedad de Comunicación Creativa Manifiesto, S.L. con la finalidad de gestionar tu relación con la agencia.
                    </small>
                    <small>
                    Te informamos sobre la posibilidad que tienes de ejercer, en cualquier momento, tus derechos de acceso, rectificación, cancelación y oposición de tus datos de carácter personal mediante correo electrónico o bien mediante escrito dirigido a Comunicación Creativa Manifiesto, S.L. / Protección de datos, Trafalgar  nº 38, planta  2ª, 08010 Barcelona.
                    </small>
                    <br>
                    <br>

                    <button data-loading-text="Enviando..." id="btn-submit" type="submit" class="btn btn-default">@lang('workshop.form.submit')</button>
                </form>
            </div>
        </div>
       </div>
    </div>
    <br>
    <br>
    <br>
@stop