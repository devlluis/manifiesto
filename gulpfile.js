const elixir = require('laravel-elixir');
var argv = require('yargs').argv;
var shell = require('gulp-shell');
require('laravel-elixir-vue-2');
elixir(function(mix) {

    mix.task('js-langs', ['resources/lang/**/*.php']);

    //VENDOR CSS
    var vendorcss = [
        '../../../node_modules/flexslider/flexslider.css',
        '../../../node_modules/animate.css/animate.css',
    ];

    var vendorjs = [

    ];

    if (argv.production !== undefined) {
        vendorjs.push('lib/production.js');
    }
    mix.webpack('app.js', 'public/assets/js/app.js')
    mix.styles(vendorcss, 'public/assets/css/vendor.css');
    mix.sass(['../../../public/assets/css/vendor.css', 'resources/assets/sass/app.scss'], 'public/assets/css');

    mix.webpack(vendorjs, 'public/assets/js/vendor.js');

    if (!argv.production && !argv.devel) {
        mix.browserSync({
            proxy: 'testing.loc',
            open: false,
            reloadDebounce: 1000,
            files: [
                //'app/**/*.php',
                'public/assets/css/**/*.css',
                'public/assets/js/**/*.js',
                'public/assets/js/**/*.vue',
                'resources/views/**/*.php'
            ]
        })
    }
})

//gulp.task('js-langs', shell.task([
  //  "php artisan vue-i18n:generate",
//]));
var exec = require('child_process').exec;

gulp.task('js-langs', function (cb) {
 exec('php artisan vue-i18n:generate', function (err, stdout, stderr) {
   cb(err);
 });
});