//ejecutar con gulp --gulpfile gulpfile.cms.js
const elixir = require('laravel-elixir');
var argv = require('yargs').argv;
var shell = require('gulp-shell');
require('laravel-elixir-vue-2');


elixir(function(mix) {

    mix.webpack('resources/assets/js/cms.js', 'public/assets/js/cms.js')

    mix.sass('resources/assets/sass/cms.scss', 'public/assets/css/cms.css');
  

    if (!argv.production && !argv.devel) {
        mix.browserSync({
            proxy: 'manifiesto.loc',
            open: false,
            reloadDebounce: 1000,
            files: [
                'app/**/*.php',
                'public/assets/css/**/*.css',
                'public/assets/js/**/*.js',
                'public/assets/js/**/*.vue',
                'resources/views/**/*.php'
            ]
        })
    }
})