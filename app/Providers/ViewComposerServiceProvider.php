<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use View;
use LaravelLocalization;
use Lang;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['errors::505','errors::404'], function($view)
        {
            $view->with([
                'locale' => LaravelLocalization::getCurrentLocale(),
                'view_name' => 'error' 
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
