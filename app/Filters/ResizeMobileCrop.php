<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ResizeMobileCrop implements FilterInterface
{
    /**
     * Default size of filter effects
     */
    const DEFAULT_SIZE = 480;


    /**
     * Creates new instance of filter
     *
     * @param integer $size
     */
    public function __construct($size = null)
    {
        $this->size = is_numeric($size) ? intval($size) : self::DEFAULT_SIZE;
    }

    /**
     * Applies filter effects to given image
     *
     * @param  Intervention\Image\Image $image
     * @return Intervention\Image\Image
     */
    public function applyFilter(Image $img)
    {
        $img->resize($this->size*1.2,null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $img->resizeCanvas($this->size,null);

        return $img;
    }
}