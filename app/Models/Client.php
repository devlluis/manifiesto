<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project;


class Client extends Model  {

	protected $fillable = ['name','priority', 'slug'];


	/*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
	*/

	public function projects()
    {
        return $this->belongsToMany(Project::class,'client_project')->withTimestamps();
	}
	/*
	|--------------------------------------------------------------------------
	| Query Scopes
	|--------------------------------------------------------------------------
	*/

	

}
