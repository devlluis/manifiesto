<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model {
	public $timestamps = false;
	protected $fillable = ['title','caption'];

	public function project()
	{
		return $this->belongsTo('App\Models\Project');
		
	}
}

