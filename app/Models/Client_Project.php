<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client_Project extends Model
{
protected $fillable = ['client_id','project_id'];

public $table = 'client_project';


	public function clients()
	{
		return $this->hasMany('App\Models\Project');
	}
	public function projects()
	{
		return $this->hasOne('App\Models\Client');
	}	

}
