<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class Project extends Model  {

	use \Dimsav\Translatable\Translatable;

	public $translatedAttributes = ['title','caption'];

	protected $fillable = ['order','public','slug'];

	/*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
	*/

	public function clients()
    {
        return $this->belongsToMany(Client::class,'client_project')->withTimestamps();
        //return $this->belongsToMany('Client')->withTimestamps()->orderBy('priority', 'desc');
	}
	public function translates()
	{
		return $this->hasMany(ProjectTranslation::class);
	}
	



	/*
	|--------------------------------------------------------------------------
	| Query Scopes
	|--------------------------------------------------------------------------
	*/

	public function scopeLatest($query) {
		return $query->wherePublic(1)->orderBy('order', 'desc');
	}

	public function scopeSlice($query, $offset, $limit) {
		return $query->wherePublic(1)->orderBy('order', 'desc')->offset($offset)->limit($limit);
	}

}
