<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Project;


class Worker extends Model  {

	protected $fillable = ['name','order','position', 'hashtag', 'slug'];


	/*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| Query Scopes
	|--------------------------------------------------------------------------
	*/

	public function scopeAll($query) {
		return $query->wherePublic(1)->orderBy('order', 'asc');
	}

	public function scopeSlice($query, $offset, $limit) {
		return $query->wherePublic(1)->orderBy('order', 'asc')->offset($offset)->limit($limit);
	}

}
