<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkshopVisitor extends Model
{
    protected $fillable = [
        'name'
        ,'company'
        ,'position'
        ,'email'
    ];
}
