<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use File;
use Cache;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Client;
use App\Models\Client_Project;
use App\Models\ProjectTranslation;
use App\Models\Worker;
use App\Admin;
use Session;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getIndex() //Mostrar pagina de inicio del backend
    {
        return view('cms.public.views.index');
        //return view('backend.public.pages.index');
    }  

    //Menus
    public function menuProjects()
    {
        $projects = DB::table('projects')->orderBy('order','asc')->get();
        $clients = DB::table('clients')->orderBy('name','asc')->get();
        return view('cms.public.views.projects.menu', ['projects' => $projects, 'clients' => $clients]); 
    }
    public function menuEmployeesTest()
    {
        $employees = DB::table('workers')->orderBy('order','asc')->get();
        return view('cms.public.views.employees.menutest', ['employees' => $employees]);
    }
    public function menuClients()
    {
        $clients = DB::table('clients')->get();
        return view('cms.public.views.clients.menu', ['clients' => $clients]);
    }
    public function menuEmployees()
    {
        $employees = DB::table('workers')->orderBy('order','asc')->get();
        return view('cms.public.views.employees.menu', ['employees' => $employees]);
    }
    public function menuAdmins()
    {   
        $admins = DB::table('admins')->get();
        return view('cms.public.views.admins.menu', ['admins' => $admins]);
    }

    //Employees 

    public function showEmployee($id) //Mostrar la informacion de un trabajador en especifico
    {
        $employee = Worker::find($id);
        return view('cms.public.views.employees.showemployee')->withEmployee($employee); 
    }
    public function editEmployee($id)
    {
        $employee = Worker::find($id);
        return view('cms.public.views.employees.editemployee')->withEmployee($employee);
    }
    public function storeEmployee(Request $request)
    {

        if ($request->file('pathimage')){

            $imagen = getimagesize($request->file('pathimage'));
            $ancho = $imagen[0];
            $altura = $imagen[1];
            $maxancho = 310;
            $minancho = 290;
            $maxaltura = 287;
            $minaltura = 267;

        if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
            $employee = new Worker();
            $employee->name = $request->input("nameEmployee");
            $employee->hashtag = $request->input("hashtagEmployee");
            $employee->slug = $request->input("slugEmployee");
            $employee->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $employee->slug);
            $employee->position = $request->input("positionEmployee");
            $employee->order = DB::table('workers')
                          ->where('order', DB::raw("(select max(`order`) from workers)"))
                          ->first()
                          ->order + 1; 
            $employee->public = 1;
            \Storage::disk('employees')->put($employee->slug.'.jpg',\File::get($request->file('pathimage')));
            $employee->save();
            $employees = DB::table('workers')->orderBy('order','asc')->get();
            $data = array('status' => 'success','employees' => $employees);
            Session::flash('success','El trabajador se ha creado con éxito.');
            return json_encode($data);
        }

        else{
            return json_encode(['status' => 'trabajadortamañoincorrecto']);
        }
     }
     else{
        return json_encode(['status' => 'nohayimagen']);
    }

    }
    public function updateEmployee(Request $request, $id) //Actualizar la informacion de un trabajador
    {

        $employee = Worker::find($id);

        $oldSlug = $employee->slug; 

        $employee->hashtag = $request->input('hashtag');
        $employee->name = $request->input('name');
        $employee->position = $request->input('position');
        $employee->order = $request->input('order');
        $employee->public = $request->input('public');
        
        if (strcmp($oldSlug, $request->input('slug')) !== 0) { //Si el slug cambia
            if ($request->hasfile('pathimage')){
                $imagen = getimagesize($request->file('pathimage'));
                $ancho = $imagen[0];
                $altura = $imagen[1];
                $maxancho = 310;
                $minancho = 290;
                $maxaltura = 287;
                $minaltura = 267;
                if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                    \Storage::disk('employees')->delete($oldSlug.'.jpg');
                    $employee->slug=$request->input('slug');
                    $employee->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $employee->slug);
                    \Storage::disk('employees')->put($employee->slug.'.jpg',\File::get($request->file('pathimage')));
                    Session::flash('success','Imagen actualizada.');
                }
                else{
                    Session::flash('warning','Las medidas de la imagen no son correctas');
                    cache::flush();
                    return redirect()->route('admin.employees.edit', $employee->id);
                }
        }
        else{
            $employee->slug=$request->input('slug');
            $employee->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $employee->slug);
            Storage::move('public/employees/'.$oldSlug.'.jpg','public/employees/'.$employee->slug.'.jpg');
            Session::flash('success','El trabajador se ha actualizado con éxito.');
        }
    }
        
        if ($request->hasFile('pathimage')){
            $imagen = getimagesize($request->file('pathimage'));
            $ancho = $imagen[0];
            $altura = $imagen[1];
            $maxancho = 310;
            $minancho = 290;
            $maxaltura = 287;
            $minaltura = 267;
            if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                $employee->slug=$request->input('slug');
                $employee->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $employee->slug);
                \Storage::disk('employees')->put($employee->slug.'.jpg',\File::get($request->file('pathimage')));
                Cache::flush();
                $employee->save();
                Session::flash('success','El trabajador se ha actualizado con éxito.');
            }
            else{
                Cache::flush();
                Session::flash('warning','No se ha podido actualizar la información, el trabajador tiene un tamaño incorrecto.');
                return redirect()->route('admin.employees.edit', $employee->id);
            }
           
        }
        Cache::flush();
        $employee->save();
        Session::flash('success','El trabajador se ha actualizado con éxito.');
        return redirect()->route('admin.employees.edit', $employee->id);
    }

    public function destroyEmployee($id) //Destruir el trabajador de la BBDD.
    {
        $employee = Worker::find($id);
        $slug = Worker::find($id)->slug;
        $employee->delete();
        DB::table('workers')->where('id',$id)->delete();
        \Storage::disk('employees')->delete($slug.'.jpg');
        Session::flash('success','El trabajador se ha borrado con éxito');
        return redirect()->route('admin.employees');

    
    }





    //Proyectos 
    public function listAll(){
        $projects = DB::table('projects')->get();
        return view('cms.public.views.projects.listall', ['projects' => $projects]); 
    }
/*
    public function updateOrder(Request $request){
        print_r($request->all());
        $orders = explode (',',$request->orders);
        foreach ( $orders as $order => $id) { 
            DB::table('projects') ->where('id', $id) ->update(['order' => $order]); 
        }
        var_dump($orders);
    }
*/
/*
    public function updateOrder(Request $request)
    {
        $ids = $request->ids;
        // en el array ids, los "keys" serían el orden, los cuales puedes modificar según lo que requieras
        $caseQuery = 'CASE ';
        foreach ($ids as $order => $id) {
            $caseQuery .= "WHEN id = $id THEN $order ";
        }
        $caseQuery .= 'ELSE null';
        $caseQuery .= ' END';
            DB::table('projects')
                ->whereIn('id', $ids)
                ->update(['order' => $caseQuery]);            
    }
    */
    public function updateOrder(Request $request)
   {
    
       $queryParams = [];
        
       $ids = $request->ids;
       //el query será definido en su totalidad de forma manual
       $query = 'UPDATE projects SET `order` = CASE id ';
       //agregamos cada parámetro de orden y de id al array para respetar las convenciones de PDO
       foreach ($ids as $order => $id) {
           $query .= 'WHEN ? THEN ? ';
           $queryParams[] = (int) $id;
           $queryParams[] = (int) $order+1;
       }


       //por último agregamos los ids implicados en el update
       $queryParams = array_merge($queryParams, $ids);

       //generamos los ? necesarios para el array de ids en el query PDO
       $whereInArray = array_fill(0, count($ids), '?');
       $whereInString = implode(", ", $whereInArray);
       
       //agregamos dicho string generado al query final
       $query .= "END WHERE id IN ($whereInString)";
       
       //realizamos el update
       DB::update($query, $queryParams);
   }
   public function updateOrderEmployee(Request $request)
   {

    $queryParams = [];
    
     $ids = $request->ids;
    //el query será definido en su totalidad de forma manual
     $query = 'UPDATE workers SET `order` = CASE id ';
     
      //agregamos cada parámetro de orden y de id al array para respetar las convenciones de PDO
      foreach ($ids as $order => $id) {
        
       $query .= 'WHEN ? THEN ? ';
       $queryParams[] = (int) $id;
       $queryParams[] = (int) $order+1;
    }

    //por último agregamos los ids implicados en el update
    $queryParams = array_merge($queryParams, $ids);

    //generamos los ? necesarios para el array de ids en el query PDO
    $whereInArray = array_fill(0, count($ids), '?');
    $whereInString = implode(", ", $whereInArray);
   
    //agregamos dicho string generado al query final
    $query .= "END WHERE id IN ($whereInString)";
   
    //realizamos el update
    DB::update($query, $queryParams);
   }     
    public function orderProjects($order,$field) 
    { 
        $clients = DB::table('clients')->get();
        $projects = DB::table('projects')->orderBy($order,$field)->get();

        return view('cms.public.views.projects.menu', ['projects' => $projects, 'clients' => $clients]);
    } 
    public function orderClients($order,$field)
    {
        $clients = DB::table('clients')->orderBy($order,$field)->get();
        return view('cms.public.views.clients.menu', ['clients' => $clients]);
    }
    public function orderEmployees($order,$field)
    {
        $employees = DB::table('workers')->orderBy($order,$field)->get();
        return view('cms.public.views.employees.menu', ['employees' => $employees]);
    }
    public function orderAdmins($order,$field)
   {
       $admins = DB::table('admins')->orderBy($order,$field)->get();
       return view('cms.public.views.admins.menu',['admins' => $admins]);
   } 
   
    public function showProjects() //Mostrar todos los proyectos
    {
        $projects = DB::table('projects')->get();
        return view('cms.public.views.projects.orderby', ['projects' => $projects]); 
    } 

    public function showProject($id) //Mostrar la informacion de un proyecto en especifico
    {
        $project = Project::find($id);
        $translation = ProjectTranslation::where('project_id',$id)->get();
        return view('cms.public.views.projects.showproject',['project' => $project, 'translation' => $translation]); 
    }
    public function editProject($id)
    {
        $project = Project::find($id);
        $translation = ProjectTranslation::where('project_id',$id)->get();
        $projectclient = Client_Project::where('project_id',$id)->first()->client_id;
        $client = DB::table('clients')->where('id',$projectclient)->first();
        return view('cms.public.views.projects.editproject', ['project' => $project, 'translation' => $translation, 'client' => $client]);
    }
   
   public function updateProject(Request $request, $id) //Actualizar la informacion de un trabajador 
    { 
       $project = Project::find($id); 

            $oldSlug = $project->slug; 
            $project->order = $request->input('order'); 
            $project->public = $request->input('public'); 
            $locale_id = $request->locale_id;
            $locale = $request->locale ;
            $caption =  $request->caption;
            $title=  $request->title;
 
          
            $maxancho = 1930;
            $minancho = 1910;
            $maxaltura = 822;
            $minaltura = 802;
            $maxancho2 = 778;
            $minancho2 = 568;
            $maxaltura2 = 355;
            $minaltura2 = 335; 
            

            if (strcmp($oldSlug, $request->input('slug')) !== 0) { //Si el slug cambia
                
                if ($request->hasfile('pathheader') && $request->hasfile('pathhome')){
                    $imagen = getimagesize($request->file('pathheader'));
                    $imagen2 = getimagesize($request->file('pathhome'));
                    $ancho = $imagen[0];
                    $altura = $imagen[1];
                    $ancho2 = $imagen2[0];
                    $altura2 = $imagen2[1];
                    if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura &&
                    $ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                        foreach($locale_id as $key => $value){
                            DB::table('project_translations')->where('id', $value )->update([
                            'locale' => $locale[$key],
                            'title' => $title[$key],
                            'caption' => $caption[$key],
                            ]);
                        }
                        $project->slug = $request->input('slug'); //se pilla el valor del slug
                        $project->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $project->slug);

                       // Storage::disk('projects')->move($oldSlug, $project->slug); //Se renombra la carpeta project

                       Storage::move('public/projects/'.$oldSlug,'public/projects/'.$project->slug);
                       File::move(resource_path('views/projects/').$oldSlug.'.blade.php',resource_path('views/projects/').$project->slug.'.blade.php');
                        Cache::flush();
                        $project->save();
                        
                    }
                    else{
                        Session::flash('warning','Las medidas de las imagenes no son correctas.');
                        return redirect()->route('admin.project.edit', $project->id);
                    }
                }

                if ($request->hasFile('pathheader')){
                    $imagen = getimagesize($request->file('pathheader'));
                    $ancho = $imagen[0];
                    $altura = $imagen[1];
                    if($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                         \Storage::disk('projects')->putFileAs($project->slug,$request->file('pathheader'),'header.jpg');
                         $project->save();
                    }
                    else{
                        Session::flash('warning','La información no se ha podido actualizar ya que las medidas del header no son las correctas.');
                        return redirect()->route('admin.project.edit', $project->id);
                    }
                }


                if ($request->hasFile('pathhome')){
                    $imagen2 = getimagesize($request->file('pathhome'));
                    $ancho2 = $imagen2[0];
                    $altura2 = $imagen2[1];
                    if($ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                        \Storage::disk('projects')->putFileAs($project->slug,$request->file('pathhome'),'home.jpg');
                        $project->save();
                    }
                    else{
                        Cache::flush();
                        Session::flash('warning','No se ha podido actualizar la información, el home tiene un tamaño incorrecto.');
                        return redirect()->route('admin.project.edit',$project->id);
                    }
    
                }

                else{
                    $project->slug = $request->input('slug'); //se pilla el valor del slug
                    $project->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $project->slug);
                    Storage::move('public/projects/'.$oldSlug,'public/projects/'.$project->slug);
                    File::move(resource_path('views/projects/').$oldSlug.'.blade.php',resource_path('views/projects/').$project->slug.'.blade.php');
                    Cache::flush();
                    $project->save();
                    Session::flash('success','La información se ha actualizado con éxito.');

                }
               




            }
            if ($request->hasfile('pathheader') && $request->hasfile('pathhome')){
                $imagen = getimagesize($request->file('pathheader'));
                $ancho = $imagen[0];
                $altura = $imagen[1];
                $imagen2 = getimagesize($request->file('pathhome'));
                $ancho2 = $imagen2[0];
                $altura2 = $imagen2[1];
                if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura &&
                $ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                    foreach($locale_id as $key => $value){
                        DB::table('project_translations')->where('id', $value )->update([
                        'locale' => $locale[$key],
                        'title' => $title[$key],
                        'caption' => $caption[$key],
                        ]);
                    }
                    Storage::move('public/projects/'.$project->slug.'/home.jpg','public/projects/'.$project->slug.'/'.$request->file('pathhome'));
                    Storage::move('public/projects/'.$project->slug.'/header.jpg','public/projects/'.$project->slug.'/'.$request->file('pathheader'));
                    Cache::flush();
                    $project->save();
                    Session::flash('success','La información se ha actualizado con éxito.');
                    
                }
                else{
                    Session::flash('warning','No se ha podido actualizar la información, revisar el tamaño de las imagenes.');
                    return redirect()->route('admin.project.edit',$project->id);
                }


            }

            if ($request->hasFile('pathheader')){
                               $imagen = getimagesize($request->file('pathheader'));
                    $ancho = $imagen[0];
                    $altura = $imagen[1];
                if($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                     \Storage::disk('projects')->putFileAs($project->slug,$request->file('pathheader'),'header.jpg');
                     Session::flash('success','Información actualizada.');
                }

                else{
                    Session::flash('warning','No se ha podido actualizar la información, el header tiene un tamaño incorrecto.');
                    return redirect()->route('admin.project.edit', $project->id);
                }
            }

            if ($request->hasFile('pathhome')){
                $imagen2 = getimagesize($request->file('pathhome'));
                $ancho2 = $imagen2[0];
                $altura2 = $imagen2[1];
                if($ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                    \Storage::disk('projects')->putFileAs($project->slug,$request->file('pathhome'),'home.jpg');
                    Session::flash('success','Información actualizada con éxito.');
                }
                else{
                    Session::flash('warning','No se ha podido actualizar la información, el home tiene un tamaño incorrecto.');
                    return redirect()->route('admin.project.edit',$project->id);
                }

            }

            foreach($locale_id as $key => $value){
                DB::table('project_translations')->where('id', $value )->update([
                'locale' => $locale[$key],
                'title' => $title[$key],
                'caption' => $caption[$key],
                ]);
            }     
            Cache::flush();
            $project->save();
            Session::flash('success','Información actualizada con éxito.');
            return redirect()->route('admin.project.edit', $project->id);

    }
    
    public function destroyProject($id) //Destruir el proyecto de la BBDD. (Tambien sus traducciones y su relacion con el cliente)
    {
        $project = Project::find($id);
        $slug = Project::find($id)->slug;
        $project->delete();
        DB::table('project_translations')->where('project_id',$id)->delete();
        DB::table('client_project')->where('project_id',$id)->delete();
        \Storage::disk('projects')->deleteDirectory($slug);
        Session::flash('success','El proyecto ha sido borrado con éxito');
        File::delete(resource_path('views/projects/'.$project->slug.'.blade.php'));

        return redirect()->route('admin.projects');
    }

    
    public function storeProject(Request $request)
        {
        if ($request->hasfile('pathheader') && $request->hasfile('pathhome')){

            $imagen = getimagesize($request->file('pathheader'));
            $ancho = $imagen[0];
            $altura = $imagen[1];
            $maxancho = 1930;
            $minancho = 1910;
            $maxaltura = 822;
            $minaltura = 802;
            $imagen2 = getimagesize($request->file('pathhome'));
            $ancho2 = $imagen2[0];
            $altura2 = $imagen2[1];
            $maxancho2 = 778;
            $minancho2 = 358;
            $maxaltura2 = 355;
            $minaltura2 = 335;

            if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                if ($ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                    $clients = DB::table('clients')->orderBy('name','asc')->get();
                    $projects = DB::table('projects')->get();
                    $project = new Project();
                    $project->slug = $request->input("slug");
                    $project->slug = preg_replace('/[^a-zA-Z0-9]/', "_", $project->slug);
                    $project->order = 1;
                    DB::table('projects')->increment('order');
                    $project->public = 0;
                    \Storage::disk('projects')->makeDirectory($project->slug);
                    \Storage::disk('projects')->putFileAs($project->slug, $request->file('pathheader'),'header.jpg');
                    \Storage::disk('projects')->putFileAs($project->slug, $request->file('pathhome'),'home.jpg');
                    $project->save();
                    File::put(resource_path('views/projects/').$project->slug.'.blade.php','');
                    return json_encode(['status'=>'ok']);
            }
                else{
                    return json_encode(['status' => 'hometamañoincorrecto']);
                }
            }
            if ($ancho2<$maxancho2 && $ancho2>$minancho2 && $altura2<$maxaltura2 && $altura2>$minaltura2){
                if($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                    $clients = DB::table('clients')->orderBy('name','asc')->get();
                    $projects = DB::table('projects')->get();
                    $project = new Project();
                    $project->slug = $request->input("slug");
                    $project->slug = preg_replace('/[^a-zA-Z0-9]/', "_", $project->slug);
                    $project->order = 1;
                    DB::table('projects')->increment('order');
                    $project->public = 0;
                    \Storage::disk('projects')->makeDirectory($project->slug);
                    \Storage::disk('projects')->putFileAs($project->slug, $request->file('pathheader'),'header.jpg');
                    \Storage::disk('projects')->putFileAs($project->slug, $request->file('pathhome'),'home.jpg');
                    $project->save();
                    File::put(resource_path('views/projects/').$project->slug.'.blade.php','');
                    return json_encode(['status'=>'ok']);
                }
                else{
                    return json_encode(['status' => 'headertamañoincorrecto']);
                }
            }
            else{
                return json_encode(['status' => 'headerhometamañoincorrecto']);
            }
            

            
            //$imagen = getimagesize($request->file('pathheader'));
            //$ancho = $imagen[0];
            //$altura = $imagen[1];
            //$maxancho = 1930;
            //$minancho = 1910;
            //$maxaltura = 822;
            //$minaltura = 802;
            //$imagen2 = getimagesize($request->file('pathhome'));
            //$ancho2 = $imagen2[0];
            //$altura2 = $imagen2[1];
            //$maxancho2 = 778;
            //$minancho2 = 358;
            //$maxaltura2 = 355;
            //$minaltura2 = 335;

         
            
                
        }
        if ($request->hasFile('pathheader')){
            
        }
        else{
            if ($request->hasFile('pathhome')){
                return json_encode(['status' => 'faltaheader']);
            }
            else{
                return json_encode(['status' => 'faltanlosdos']);
            }
        }
        if ($request->hasFile('pathhome')){

        }
        else{
            if ($request->hasFile('pathheader')){
                return json_encode(['status' => 'faltahome']);
            }
            else{
                return json_encode(['status' => 'faltanlosdos']);
            }
        }
       // else{
          //  $data = array('status' => 'error',
            //              'warning' => 'Hay que poner una imagen como header y otra como home.');
               //           return json_encode($data);

        //}
        }

        public function storeProjectsClients(Request $request){
            $client = new Client();
            $client->name = $request->input("nameClient");
            $client->slug = $request->input("slugClient");
            $client->save();

            $client_project = new Client_Project();
            $client_project->client_id = DB::table('clients')->where('id', DB::raw("(select max(id) from clients)"))->first()->id;
            $client_project->project_id = DB::table('projects')->where('id', DB::raw("(select max(`id`) from projects)"))->first()->id;
            $client_project->save();
            Session::flash('success','Proyecto, Traducciones, Cliente y relación Proyecto-Cliente creado con éxito.');
            $projects = DB::table('projects')->get();
            $clients = DB::table('clients')->orderBy('name','asc')->get();
            Cache::flush();
            return view('cms.public.views.projects.menu', ['projects' => $projects, 'clients' => $clients]);
        }

        public function storeProjectsClientsIfExist(Request $request){
            $client_project = new Client_Project();
            $client_project->client_id = $request->client;
            $client_project->project_id = DB::table('projects')->where('id', DB::raw("(select max(`id`) from projects)"))->first()->id;
            $client_project->save();
            Session::flash('success','Proyecto, Traduccion y relación Proyecto-Cliente creado con éxito.');
            $projects = DB::table('projects')->get();
            $clients = DB::table('clients')->orderBy('name','asc')->get();
            Cache::flush();
            return view('cms.public.views.projects.menu', ['projects' => $projects, 'clients' => $clients]);
        }


/*   //Funcion para pillar el nombre original 
    public function storeProject(Request $request)
    {
        $project = Project::create($request->only('slug', 'position', 'public', 'pathheader', 'pathhome'));

        \Storage::disk('projects')->makeDirectory($project->slug);

        foreach ($request->allFiles() as $file) {
            \Storage::disk('projects')->putFileAs($project->slug, $file, $file->getClientOriginalName());
        }
    }
*/
    //Clientes
    public function showClients() //Mostrar todos los clientes
    {
        $clients = DB::table('clients')->get();
        return view('admin.clientes', ['clients' => $clients]);

    }
    
    public function showClient($id) //Mostrar la informacion de un cliente
    {
       $client = Client::find($id);
       return view('cms.public.views.clients.showclient')->withClient($client);
    }

//    public function editClient($id)
  //  {
    //    $client = Client::find($id);
      //  $client_project = DB::table('client_project')->where('client_id',$id)->get();
      //  return $client_project[0]->project_id;
       // return view('cms.public.views.clients.editclient')->withClient($client);
   // }

    public function editClient($id)
    {
        $client = Client::find($id);
       // $client_project = DB::table('client_project')->where('client_id',$id)->first()->project_id;
        //$project = DB::table('projects')->where('id',$client_project)->first();
        $client_projects = Client::find($id)->projects;
        return view('cms.public.views.clients.editclient', ['client' => $client, 'client_projects' => $client_projects]);
        
    }



    public function storeClient(Request $request)
    {
        if ($request->hasfile('pathimage')){

        $imagen = getimagesize($request->file('pathimage'));
        $ancho = $imagen[0];
        $altura = $imagen[1];
        $maxancho = 210;
        $minancho = 190;
        $maxaltura = 210;
        $minaltura = 190;
        if($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
            $client = new Client();
            $client->name = $request->input("nameClient");
            $client->slug = $request->input("slugClient");
            $client->slug = preg_replace('/[^a-zA-Z0-9]/', "_", $client->slug);
            \Storage::disk('clients')->put($client->slug.'.png',\File::get($request->file('pathimage')));
            $client->save();
            $clients = DB::table('clients')->get();
            $data = array('status' => 'success',
                         'clients' => $clients);
            Session::flash('success','El cliente se ha creado con éxito.');
            return json_encode($data);
        }
        else {
            return json_encode(['status' => 'tamañoincorrecto']);
        }
        
        
    }
    else{
        return json_encode(['status' => 'faltaimagen']);
    }
}

    public function updateClient(Request $request, $id) //Actualizar la informacion de un cliente
    {
   
        $cliente = Client::find($id);
        $oldSlug = $cliente->slug;

        $cliente->name = $request->input('name');

        if (strcmp($oldSlug, $request->input('slug')) !== 0)
        {
            if ($request->hasfile('pathimage')){
                $imagen = getimagesize($request->file('pathimage'));
                $ancho = $imagen[0];
                $altura = $imagen[1];
                $maxancho = 210;
                $minancho = 190;
                $maxaltura = 210;
                $minaltura = 190;
                if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                    \Storage::disk('clients')->delete($oldSlug.'.png');
                    $cliente->slug = $request->input('slug');
                    $cliente->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $cliente->slug);
                    \Storage::disk('clients')->put($cliente->slug.'.png',\File::get($request->file('pathimage')));
                    $cliente->save();
                    Session::flash('success','La información se ha actualizado con éxito.');
                }
                else{
                    Session::flash('warning','La información no se ha podido actualizar ya que las medidas de la imagen no son las correctas.');
                    return redirect()->route('admin.clients.edit', $cliente->id);
                }
            }
        else{
            $cliente->slug=$request->input('slug');
            $cliente->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $cliente->slug);
            Storage::move('public/clients/'.$oldSlug.'.png','public/clients/'.$cliente->slug.'.png');
            $cliente->save();   
            Session::flash('success','La información se ha actualizado con éxito.');
        }
        }

        if ($request->hasFile('pathimage')){
            $imagen = getimagesize($request->file('pathimage'));
            $ancho = $imagen[0];
            $altura = $imagen[1];
            $maxancho = 210;
            $minancho = 190;
            $maxaltura = 210;
            $minaltura = 190;
            if ($ancho<$maxancho && $ancho>$minancho && $altura<$maxaltura && $altura>$minaltura){
                $cliente->slug = $request->input('slug');
                $cliente->slug = preg_replace('/[^a-zA-Z0-9]/', "-", $cliente->slug);
                \Storage::disk('clients')->put($cliente->slug.'.png',\File::get($request->file('pathimage')));
                $cliente->save();
                Cache::flush();
                Session::flash('success','La información se ha actualizado con éxito.');
                return redirect()->route('admin.clients.edit', $cliente->id);
                
        }
        else{
            Cache::flush();
            Session::flash('warning','Tamaño incorrecto de la imagen del cliente, no se ha podido actualizar la información.');
            return redirect()->route('admin.clients.edit', $cliente->id);
            
        }

    }   
        $cliente->save();
        Cache::flush();
        Session::flash('success','La información se ha actualizado con éxito.');
        return redirect()->route('admin.clients.edit', $cliente->id);

    } 

    public function destroyClient($id) //Destruir cliente y todo lo relacionado de la bbdd
    {
        /* Primera manera
        $cliente = Client::find($id); 
        $cliente_project = DB::table('client_project')->where('client_id',$id)->first();
        $project_id = $cliente_project->project_id;
        $cliente->delete();
        DB::table('client_project')->where('client_id',$id)->delete();
        DB::table('projects')->where('id',$project_id)->delete();
        DB::table('project_translations')->where('project_id',$project_id)->delete();
        return redirect()->route('admin.clients');
        */
        /*Segunda manera*/
        $cliente = Client::find($id); 
        $cliente->delete(); //delete the client
        Session::flash('success','El cliente se ha borrado con éxito');
        // listado de proyectos de cliente
        $projects = DB::table('client_project')->where('client_id',$id)->get();
        DB::table('client_project')->where('client_id',$id)->delete(); 
        // borrar proyecto y su traducción
        foreach($projects as $project)
        {
            DB::table('projects')->where('id',$project->id)->delete();
            DB::table('project_translations')->where('project_id',$project->id)->delete(); 
        }    
        return redirect()->route('admin.clients');
    }



    public function createAdmin(){

    }
    //Admins
    public function showAdmins() //Mostrar todos los admins
    {
        $admins = DB::table('admins')->get();
        return view('admin.admins', ['admins' => $admins]);
    }

    public function showAdmin($id) //Mostrar la informacion de un admin
    {
        $admin = Admin::find($id);
        return view('cms.public.views.admins.showadmin')->withAdmin($admin);
    } 
    public function editAdmin($id)
    {
        $admin = Admin::find($id);
        return view('cms.public.views.admins.editadmin')->withAdmin($admin);
    }

    public function storeAdmin(Request $request)
    {
        $admin = new Admin();
        $admin->name = $request->input("name");
        $admin->email = $request->input("email");

        if (strlen($request->input('password')) > 6){
            $admin->password = bcrypt($request->input("password"));
            $admin->save(); 
            Session::flash('success','El administrador se ha creado con éxito.');
      }
      else {
         Session::flash('warning','El campo password tiene que tener como mínimo 6 caracteres.');
      }
    }

    public function updateAdmin(Request $request, $id) //Actualizar la informacion de un admin
    {
        $this->validate($request, array(
            'name'=> 'required',
            'email'=> 'required',
            'password'=> 'required'
        ));
        $admin = Admin::find($id);
        $admin->name = $request->input('name');
        $admin->email = $request->input('email');
        $admin->password = $request->input('password');
        $admin->save();
        Session::flash('success','Este usuario se ha actualizado con éxito.');
        return redirect()->route('admin.showadmin', $user->id);
    }

    public function destroyAdmin($id) //Eliminar la informacion de un admin
    {
        $admin = Admin::find($id);
        $admin->delete();
        Session::flash('success','El admin ha sido eliminado con éxito.');
        return redirect()->route('admin.admins');
    }
    /*
    public function cambiarVisible(Request $request, $id)
    {
        $employee = Worker::find($id);
        if ($employee->public == 0){
            DB::table('workers')->where('id',$id)
            ->first()
            ->public = 1;
        dd($employee->public);
        }
        else{
            $employee->public = 0;
        }
    } */
    public function cambiarPublicEmployee(Request $request, $id)
    {
        $employee = Worker::findOrFail($id);
        $employee->public = ($employee->public == 1) ? 0 : 1;
        $employee->save();
    }
    public function cambiarPublicProject(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $project->public = ($project->public == 1) ? 0 : 1;
        $project->save();
    }
    
}
