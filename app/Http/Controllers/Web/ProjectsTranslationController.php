<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjectTranslation;
use App\Models\Client;
use DB;
use Session;
class ProjectsTranslationController extends Controller
{
        public function storeTranslation(Request $request){
        $projecttranslation0 = new ProjectTranslation();
        $projecttranslation0->locale = 'en';
        $projecttranslation0->project_id = DB::table('projects')
                ->where('id', DB::raw("(select max(`id`) from projects)"))->first()->id;
        $projecttranslation0->title = $request->input("title-0");
        $projecttranslation0->caption = $request->input("caption-0");
        $projecttranslation0->save();

        $projecttranslation1 = new ProjectTranslation();
        $projecttranslation1->locale = 'es';
        $projecttranslation1->project_id = DB::table('projects')
                ->where('id', DB::raw("(select max(`id`) from projects)"))->first()->id;
        $projecttranslation1->title = $request->input("title-1");
        $projecttranslation1->caption = $request->input("caption-1");
        $projecttranslation1->save();   
        }
        }
