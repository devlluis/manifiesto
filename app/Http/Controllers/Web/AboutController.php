<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Worker;
use Lang;

class AboutController extends Controller
{
    public function getIndex()
    {
        $data = [
            'workerslug' => '',
            'title' => Lang::get('metas.about.title'),
            'view_name' => 'about',
            //'locale' => Lang::getLocale()
        ];

        return view('about', $data);
    }

    public function getWorkerList($offset, $limit)
    {
        $workers = Worker::slice($offset, $limit)->get();
        $total = Worker::all()->count();
        
        return response()->json(['success' => true, 'workers' => $workers, 'total'=>$total ]);
    }
}
