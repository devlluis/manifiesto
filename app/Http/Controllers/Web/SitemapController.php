<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Carbon\Carbon;
use Lang;
use Jenssegers\Agent\Agent;
use View;
use Response;
use LaravelLocalization;


class SitemapController extends Controller
{


    public function getIndex()
    {

        $defaultDate = Carbon::now();
        $locales = LaravelLocalization::getSupportedLocales();

        $content = View::make('sitemap.index', ['defaultDate'=>$defaultDate, 'locales'=>$locales]);
        return Response::make($content)->header('Content-Type', 'text/xml');

    }

    public function getLang($lang)
    {

        $projects = Project::latest()->get();
        $defaultDate = Carbon::today();


        $content = View::make('sitemap.lang', ['lang'=>$lang,'projects' => $projects, 'defaultDate'=>$defaultDate]);
        return Response::make($content)->header('Content-Type', 'text/xml');

    }

}