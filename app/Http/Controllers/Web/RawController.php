<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Lang;

class RawController extends Controller
{
    public function getIndex($view)
    {
        $data = [
            'title' => Lang::get('metas.'.$view.'.title'),
            'view_name' => $view,
        ];

        return view($view, $data);
    }

    public function getJson($view)
    {
        $data = [
            'title' => Lang::get('metas.'.$view.'.title'),
            'view_name' => $view,
        ];

        $html = view($view)->render();
        
        $data['success'] = true;
        $data['html'] = $html;
        
        return response()->json($data);


    }

}
