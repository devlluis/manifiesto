<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Requests;
use Lang;
use LaravelLocalization;


class ProjectsController extends Controller
{

    public function getIndex()
    {
        $data = [
            'projectSlug' => '',
            'projects' => Project::latest()->get(),
            'title' => Lang::get('metas.home.title'),
            'view_name' => 'home'
        ];

        return view('home', $data);
    }

    public function getProjectList($offset, $limit)
    {
        $locale = LaravelLocalization::getCurrentLocale();
        $projects = Project::orderBy('order','asc')->slice($offset, $limit)->get();
        $total = Project::latest()->get()->count();
        
        foreach ($projects as $project) {
            $project['client'] = $project->clients->first()->slug;
            $project['caption'] = strip_tags($project->caption);
        };
        return response()->json(['success' => true, 'projects' => $projects, 'total'=>$total , 'locale'=>$locale ]);
    }

    public function getProject($slug, Request $request)
    {
        
        $project = Project::whereSlug($slug)->first();
        $projects = Project::latest()->get();

        //position table starts on 1 instead of 0
        $ppos = ($project->order);
        $nextPosition = Project::where('order', '<', $project->order)->wherePublic(1)->max('order');
        $prevPosition = Project::where('order', '>', $project->order)->wherePublic(1)->min('order');
        $nextProject = Project::whereOrder($nextPosition)->get();
        $prevProject = Project::whereOrder($prevPosition)->get();

        $nextSlug = ($nextPosition) ? $nextProject[0]['slug'] : '';
        $prevSlug = ($prevPosition) ? $prevProject[0]['slug'] : '';

        $nextTitle = ($nextPosition) ? $nextProject[0]['title'] : '';
        $prevTitle = ($prevPosition) ? $prevProject[0]['title'] : '';

        $clients = $project->clients;
        $clientsString = '';

        //TODO: simplificarlo, ya solo usamos un cliente por proyecto
        foreach ($clients as $key => $client) {
            $clientsString .= $client->name;
            $clientsString .= count($clients)-1<=$key ? '' : ', ';
        };

        $caption = $project->caption;
        $projectSlug = $slug;
        $image_path = '/storage/projects/'.$projectSlug.'/';
        $title = $project->title;
        $og_description = $project->title . '. ' . html_entity_decode(strip_tags($project->caption), ENT_QUOTES, 'utf-8');
        $og_image = config('app.url') . '/storage/projects/' . $project->slug . '/home.jpg';
        $view_name = 'project';
        $video_button_extras = 'ps='.$projectSlug." ip=".$image_path;

        $data = compact(
            'project',
            'clients',
            'clientsString',
            'caption',
            'nextSlug',
            'prevSlug',
            'nextTitle',
            'prevTitle',
            'projectSlug',
            'image_path',
            'title',
            'og_description',
            'og_image',
            'view_name',
            'video_button_extras'
        );

        $html = view('projects.'.$slug,$data)->render();

        $data['success'] = true;
        $data['html'] = $html;

        return response()->json($data);
        

    }


}
