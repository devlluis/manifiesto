<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Lang;

use App\Models\WorkshopVisitor;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('workshop')->with([
            'title' => Lang::get('footer.legal-link'),
            'view_name'=>'workshop',
            'locale' => Lang::getLocale(),   
        ]);
    }

    protected function validateVisitor($data)
    {
        $rules = [
            'name'          => 'required'
            ,'company'      => 'required'
            ,'position'     => 'required'
            ,'email'        => 'required|email|unique:workshop_visitors,email'
        ];

        $messages = [
            'name.required'         => trans('validation.required', ['attribute' => trans('workshop.form.name')])
            ,'company.required'     => trans('validation.required', ['attribute' => trans('workshop.form.company')])
            ,'position.required'    => trans('validation.required', ['attribute' => trans('workshop.form.position')])
            ,'email.required'       => trans('validation.required', ['attribute' => trans('workshop.form.email')])
            ,'email.email'          => trans('validation.email', ['attribute' => trans('workshop.form.email')])
            ,'email.unique'          => trans('validation.unique', ['attribute' => trans('workshop.form.email')])
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = $this->validateVisitor($request->all());
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->messages()]);
        }


        $visitor = WorkshopVisitor::firstOrCreate([
            'email' => $request->input('email')
            ,'name'  => $request->input('name')
            ,'company'  => $request->input('company')
            ,'position' => $request->input('position')
        ]);

        return response()->json(['success' => true, 'message' => trans('workshop.form.success')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
