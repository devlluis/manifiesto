<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use View;
use LaravelLocalization;
use Jenssegers\Agent\Agent;
use Lang;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->locale = LaravelLocalization::getCurrentLocale();

        $browser = new Agent();

        View::share('locale', $this->locale);
        View::share('browser', $browser);

    }
}
