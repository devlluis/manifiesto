var path = require('path');

module.exports = {
    module: {
        loaders: [
            { test: /\.js$/, loader: 'imports-loader?define=>false' },
        ]
    },
    resolve: {
        alias: {
            'ScrollMagicGSAP': 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap',
            'ScrollMagicIndicators': 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators',
            'JqueryGsap': 'gsap/jquery.gsap.js',
            'ScrollTo': 'gsap/src/uncompressed/plugins/ScrollToPlugin.js',
            'TweenMax': 'gsap/src/uncompressed/TweenMax.js',
            'TimelineMax': 'gsap/src/uncompressed/TimelineMax.js'
        }
    }
}